#!/bin/bash

sbt assembly
cd src/main/frontend
npm run build:web
cd ../../..
rm -f ~/Deutsche\ Telekom\ AG/BDSP\ -\ Dokumente/Technische\ Dokumentation\ \(intern\)/Demos/the_incident_room.zip
~/Programme/7-ZipPortable/App/7-Zip64/7z a ~/Deutsche\ Telekom\ AG/BDSP\ -\ Dokumente/Technische\ Dokumentation\ \(intern\)/Demos/the_incident_room.zip \
run.sh \
run.cmd \
README.md \
resources/orc_demo/tensorType=GENERAL \
resources/orc_demo/tensorType=CHANNEL \
resources/orc_demo/tensorType=SCALAR \
resources/hadoop-2.7.1 \
target/scala-2.12/com.tsi.bdsp.the-incident-room-jar-with-dependencies.jar \
src/main/resources/web \

rm -f ~/Deutsche\ Telekom\ AG/BDSP\ -\ Dokumente/Technische\ Dokumentation\ \(intern\)/Demos/the_incident_room_rgb.zip
~/Programme/7-ZipPortable/App/7-Zip64/7z a ~/Deutsche\ Telekom\ AG/BDSP\ -\ Dokumente/Technische\ Dokumentation\ \(intern\)/Demos/the_incident_room_video_rgb.zip \
resources/orc_demo/tensorType=VIDEO/key=6fe7c6fa-ce7d-363b-b57e-537e1c59f4f0/side=LEFT/coloured=true \
resources/orc_demo/tensorType=VIDEO/key=6fe7c6fa-ce7d-363b-b57e-537e1c59f4f0/side=RIGHT/coloured=true \
resources/orc_demo/tensorType=VIDEO/key=cbe98932-8a5f-3a35-9960-096b9dbf5031/side=LEFT/coloured=true \
resources/orc_demo/tensorType=VIDEO/key=cbe98932-8a5f-3a35-9960-096b9dbf5031/side=RIGHT/coloured=true 

rm -f ~/Deutsche\ Telekom\ AG/BDSP\ -\ Dokumente/Technische\ Dokumentation\ \(intern\)/Demos/the_incident_room_gsc.zip
~/Programme/7-ZipPortable/App/7-Zip64/7z a ~/Deutsche\ Telekom\ AG/BDSP\ -\ Dokumente/Technische\ Dokumentation\ \(intern\)/Demos/the_incident_room_video_gsc.zip \
resources/orc_demo/tensorType=VIDEO/key=6fe7c6fa-ce7d-363b-b57e-537e1c59f4f0/side=LEFT/coloured=false \
resources/orc_demo/tensorType=VIDEO/key=6fe7c6fa-ce7d-363b-b57e-537e1c59f4f0/side=RIGHT/coloured=false \
resources/orc_demo/tensorType=VIDEO/key=cbe98932-8a5f-3a35-9960-096b9dbf5031/side=LEFT/coloured=false \
resources/orc_demo/tensorType=VIDEO/key=cbe98932-8a5f-3a35-9960-096b9dbf5031/side=RIGHT/coloured=false 

rm -f ~/Deutsche\ Telekom\ AG/BDSP\ -\ Dokumente/Technische\ Dokumentation\ \(intern\)/Demos/the_incident_room_lidar1.zip
~/Programme/7-ZipPortable/App/7-Zip64/7z a ~/Deutsche\ Telekom\ AG/BDSP\ -\ Dokumente/Technische\ Dokumentation\ \(intern\)/Demos/the_incident_room_lidar1.zip \
resources/orc_demo/tensorType=POINTCLOUD/key=6fe7c6fa-ce7d-363b-b57e-537e1c59f4f0 

rm -f ~/Deutsche\ Telekom\ AG/BDSP\ -\ Dokumente/Technische\ Dokumentation\ \(intern\)/Demos/the_incident_room_lidar2.zip
~/Programme/7-ZipPortable/App/7-Zip64/7z a ~/Deutsche\ Telekom\ AG/BDSP\ -\ Dokumente/Technische\ Dokumentation\ \(intern\)/Demos/the_incident_room_lidar2.zip \
resources/orc_demo/tensorType=POINTCLOUD/key=cbe98932-8a5f-3a35-9960-096b9dbf5031 
