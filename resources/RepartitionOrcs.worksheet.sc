val df =spark.read.orc("Projekte\\the_indicent_room\\resources\\orc\\tensorType=GENERAL")

df.groupBy("key")
  .pivot("field")
  .agg(min("value"))
  .withColumn("frames",when(col("key")===lit("6fe7c6fa-ce7d-363b-b57e-537e1c59f4f0"),lit(108)).otherwise(lit(100)))
  .withColumn("startTime",when(col("key")===lit("6fe7c6fa-ce7d-363b-b57e-537e1c59f4f0"),lit("2011-09-26 13:02:25.964389")).otherwise(lit("2011-09-26 14:58:03.244023")))
  .withColumn("endTime",when(col("key")===lit("6fe7c6fa-ce7d-363b-b57e-537e1c59f4f0"),lit("2011-09-26 13:02:37.004854")).otherwise(lit("2011-09-26 14:58:13.484456")))  
  .withColumn("vehicle",when(col("key")===lit("6fe7c6fa-ce7d-363b-b57e-537e1c59f4f0"),lit("MB C205")).otherwise(lit("VW P3C")))  
  .coalesce(1)
  .write
  .mode(org.apache.spark.sql.SaveMode.Overwrite)
  .orc(s"Projekte\\the_indicent_room\\resources\\orc_demo\\tensorType=GENERAL")

case class Channel(
    val key:String,
    val targetType:String,
    val device:String,
    val option1:String,
    val option2:String,
    val option3:String,
    val frequency:String
)

val channels=Seq(
    Channel("6fe7c6fa-ce7d-363b-b57e-537e1c59f4f0","VIDEO","FL2-14S3C-C","BF 130°, LEFT, RGB","LEFT", "true", "1242 x 375, 10 Hz, PNG"),
    Channel("6fe7c6fa-ce7d-363b-b57e-537e1c59f4f0","VIDEO","FL2-14S3C-C","BF 130°, RIGHT, RGB","RIGHT", "true", "1242 x 375, 10 Hz, PNG"),
    //Channel("6fe7c6fa-ce7d-363b-b57e-537e1c59f4f0","VIDEO","FL2-14S3C-C","BF 130°, LEFT, GSC","LEFT", "false", "1242 x 375, 10 Hz, PNG"),
    //Channel("6fe7c6fa-ce7d-363b-b57e-537e1c59f4f0","VIDEO","FL2-14S3C-C","BF 130°, RIGHT, GSC","RIGHT", "false", "1242 x 375, 10 Hz, PNG"),
    Channel("cbe98932-8a5f-3a35-9960-096b9dbf5031","VIDEO","FL2-14S3C-C","BF 130°, LEFT, RGB","LEFT", "true", "1242 x 375, 10 Hz, PNG"),
    Channel("cbe98932-8a5f-3a35-9960-096b9dbf5031","VIDEO","FL2-14S3C-C","BF 130°, RIGHT, RGB","RIGHT", "true", "1242 x 375, 10 Hz, PNG"),
    //Channel("cbe98932-8a5f-3a35-9960-096b9dbf5031","VIDEO","FL2-14S3C-C","BF 130°, LEFT, GSC","LEFT", "false", "1242 x 375, 10 Hz, PNG"),
    //Channel("cbe98932-8a5f-3a35-9960-096b9dbf5031","VIDEO","FL2-14S3C-C","BF 130°, RIGHT, GSC","RIGHT", "false", "1242 x 375, 10 Hz, PNG"),
    Channel("6fe7c6fa-ce7d-363b-b57e-537e1c59f4f0","POINTCLOUD","VD HDL-64E","360x20°","", "", "0.4°, 10 Hz, CB"),
    Channel("cbe98932-8a5f-3a35-9960-096b9dbf5031","POINTCLOUD","VD HDL-64E","360x20°","", "", "0.4°, 10 Hz, CB"),
    Channel("6fe7c6fa-ce7d-363b-b57e-537e1c59f4f0","SCALAR","OXTS RT 3003","[T_w_imu,packet]","", "", "[[3x3],[31x1]] 10 Hz, IEEE64"),
    Channel("cbe98932-8a5f-3a35-9960-096b9dbf5031","SCALAR","OXTS RT 3003","[T_w_imu,packet]","", "", "[[3x3],[31x1]] 10 Hz, IEEE64")
)

val channelsDf=spark.createDataFrame(channels)

val sessions=spark.read.orc("Projekte\\the_indicent_room\\resources\\orc_demo\\tensorType=GENERAL").select("key","startTime","endTime","frames","vehicle","driveId","dataDate")

sessions.join(channelsDf,"key")
  .withColumnRenamed("frames","oldFrames")
  .withColumn("frames",when(col("targetType")===lit("SCALAR"),lit(46)).otherwise(col("oldFrames")))
  .drop("oldFrames")
  .coalesce(1)
  .write
  .mode(org.apache.spark.sql.SaveMode.Overwrite)
  .partitionBy("key")
  .orc(s"Projekte\\the_indicent_room\\resources\\orc_demo\\tensorType=CHANNEL")
  
val vdf=spark.read.orc("Projekte\\the_indicent_room\\resources\\orc\\tensorType=VIDEO")

val allFrames=vdf.groupBy("key","startTime")
   .agg(max("endTime").as("endTime"),
        collect_list(when(col("field")===lit("side"),col("value")).otherwise(lit(null))).as("sides"),
        collect_list(when(col("field")===lit("index"),col("value")).otherwise(lit(null))).as("indices"),
        collect_list(when(col("field")===lit("coloured"),col("value")).otherwise(lit(null))).as("colours"),
        collect_list(when(col("field")===lit("imgType"),col("value")).otherwise(lit(null))).as("imgTypes")
        , collect_list(when(col("field")===lit("imgData"),col("value")).otherwise(lit(null))).as("imgDatas")
       )

def extractFrame(pos:Int) = {
    allFrames
    .withColumn("index",regexp_extract(col("indices").apply(pos),"\\[(.*)\\]",1).cast(org.apache.spark.sql.types.DataTypes.IntegerType))
    .withColumn("side",regexp_extract(col("sides").apply(pos),"\\[(.*)\\]",1))
    .withColumn("coloured",regexp_extract(col("colours").apply(pos),"\\[(.*)\\]",1))
    .withColumn("imgType",regexp_extract(col("imgTypes").apply(pos),"\\[(.*)\\]",1))
    .withColumn("imgData",col("imgDatas").apply(pos))
}

val framesSingle=(0 until 3).map(extractFrame(_))

framesSingle.foreach(  frames => {
 frames
  .write
  .mode(org.apache.spark.sql.SaveMode.Append)
  .partitionBy("key","side","coloured","imgType")
  .orc(s"Projekte\\the_indicent_room\\resources\\orc_demo\\tensorType=VIDEO")
})

val pdf=spark.read.orc("Projekte\\the_indicent_room\\resources\\orc\\tensorType=POINTCLOUD")

val allClouds=pdf.groupBy("key","startTime")
   .agg(max("endTime").as("endTime"),
        regexp_extract(max(when(col("field")===lit("index"),col("value")).otherwise(lit(null))),"\\[(.*)\\]",1).cast(org.apache.spark.sql.types.DataTypes.IntegerType).as("index")
        , first(when(col("field")===lit("pointCloudData"),col("value")).otherwise(lit(null))).as("pointCloudData")
       )

allClouds
  .write
  .mode(org.apache.spark.sql.SaveMode.Overwrite)
  .partitionBy("key")
  .orc(s"Projekte\\the_indicent_room\\resources\\orc_demo\\tensorType=POINTCLOUD")

import scala.util.parsing.combinator.RegexParsers

sealed trait DataTuple
case class DATAITEMS(items: List[DataTuple]) extends DataTuple
case class DATAENTRY(key: String, value: Double) extends DataTuple
case class DATAKEY(key: String) extends DataTuple
case class DATAVALUE(value: Double) extends DataTuple

// Scala parser combinators for multiple array items
class OxtsDataParser extends RegexParsers  {

  def value: Parser[DataTuple] = (arr | pair)
  def arr: Parser[DATAITEMS] = "[" ~> repsep(value, ",") <~ "]" ^^ DATAITEMS
  def pair: Parser[DATAENTRY] = keyLiteral ~ "," ~ doubleValue ^^ { case DATAKEY(k) ~ _ ~ DATAVALUE(v) =>  DATAENTRY(k, v) }
  def keyLiteral: Parser[DATAKEY] = "(\\w+)((\\.\\[\\d+\\])|(\\.\\w+))*".r ^^ DATAKEY
  def doubleValue: Parser[DATAVALUE] = "^(-)?(\\d+)(\\.\\d+)?".r ^^ (d => DATAVALUE(d.toDouble))

  def apply(raw: String): ParseResult[DataTuple] = parseAll(value, raw)

  def toPairs(items: DataTuple): Array[(String,Double)] = {
      items match {
        case dataItems: DATAITEMS => dataItems.items.toArray.flatMap(toPairs)
        case dataEntry: DATAENTRY => Array((dataEntry.key -> dataEntry.value))
        case _ => Array()
      }
  }
  
  def getPairs(values:String): Array[(String,Double)] = {
    val items: DATAITEMS = apply(values).getOrElse(DATAITEMS(List())).asInstanceOf[DATAITEMS]
    toPairs(items)
  }
}
val parseOxtsUdf=org.apache.spark.sql.functions.udf(
      (values:String) => { 
          new OxtsDataParser().getPairs(values)
      })

val scalars=spark.read.orc("Projekte\\the_indicent_room\\resources\\orc\\tensorType=OXTSDATA")

scalars.groupBy("key","startTime")
       .agg(max("endTime").as("endTime"),
        regexp_extract(max(when(col("field")===lit("index"),col("value")).otherwise(lit(null))),"\\[(.*)\\]",1).cast(org.apache.spark.sql.types.DataTypes.IntegerType).as("index")
        , first(when(col("field")===lit("oxts"),col("value")).otherwise(lit(null))).as("oxtsData"))
       .withColumn("parsedOxts",parseOxtsUdf(col("oxtsData")))
  .select(col("key"),col("startTime"),col("endTime"),explode(col("parsedOxts")).as("signal"))
  .select(col("key"),col("startTime"),col("endTime"),col("signal").apply("_1").as("channel"),col("signal").apply("_2").as("value"))
  .coalesce(8)
  .write
  .mode(org.apache.spark.sql.SaveMode.Overwrite)
  .partitionBy("key")
  .orc(s"Projekte\\the_indicent_room\\resources\\orc_demo\\tensorType=SCALAR")

