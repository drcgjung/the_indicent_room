package com.tsi.bdsp.api.service

import com.tsi.bdsp.api.support.{DATAITEMS, DataTuple, OxtsDataParser, ValueParser, ArrayDataParser}
import org.junit.Assert.{assertEquals, assertFalse, assertNotNull, assertTrue}
import org.junit.Test

class ValueParserTest {

  @Test
  def testValueParsering(): Unit = {
    val str1 = "[2011_09_26]"
    val str2 = "[0001]"
    val str3 = "[[Test]]"
    val str4 = "[[[Test1][Test2][Test3]]]"
    val str5 = "[[Test1[0.1,0.2,0.3]][Test2[0.8,0.9,1.0]]]"
    val str6 = "[[Test1[0.1|0.2|0.3]][Test2[0.8|0.9|1.0]]]"

    val vp1: List[String] = ValueParser(str1).get
    val vp2: List[String] = ValueParser(str2).get
    val vp3: List[String] = ValueParser(str3).get
    val vp4: List[String] = ValueParser(str4).get
    val vp5: List[String] = ValueParser(str5).get
    val vp6: List[String] = ValueParser(str6).get

    assertEquals("2011_09_26", vp1.head)
    assertEquals("0001", vp2.head)
    assertEquals("Test", vp3.head)
    assertEquals("Test1,Test2,Test3", vp4.head)
    assertEquals("Test1,0.1,0.2,0.3,Test2,0.8,0.9,1.0", vp5.head)
    assertEquals("Test1,0.1|0.2|0.3,Test2,0.8|0.9|1.0", vp6.head)
  }

  @Test
  def testValueParsingOxtsData(): Unit = {
    val str1 = "[[[T_w_imu.[1].[3],-36.12642597872764],[packet.alt,116.47912597656],[packet.wy,0.016598482294063],[packet.ve,-7.940007201176],[packet.al,-0.064506048449015],[packet.velmode,6],[T_w_imu.[2].[1],0.02463801443024033],[packet.wu,0.0024801103865712],[packet.ay,0.17915637040572],[T_w_imu.[1].[2],0.01861638955135811],[packet.au,9.6049723615228],[T_w_imu.[3].[0],0.0],[packet.wf,-0.013209105541174],[packet.vl,-0.030758150117988],[packet.navstat,4],[packet.lat,49.014679293008],[packet.wx,-0.01323384792128],[packet.numsats,8],[packet.pos_accuracy,0.026907248094147],[packet.ax,-0.89545749579404],[T_w_imu.[0].[2],-0.019533174953904695],[packet.lon,8.4332719945888],[T_w_imu.[0].[0],-0.925034420484886],[T_w_imu.[3].[1],0.0],[packet.vn,-3.2912244492457],[packet.posmode,6],[packet.vel_accuracy,0.012041594578792],[packet.wz,0.0019217284844557],[packet.af,-0.79892688581476],[packet.roll,0.024642],[T_w_imu.[2].[2],0.999635876265099],[packet.az,9.5949085265805],[T_w_imu.[3].[3],1.0],[packet.yaw,-2.7520699803847],[packet.vf,8.5950522068704],[T_w_imu.[0].[1],0.3793807796850167],[T_w_imu.[2].[0],-0.011003777925923186],[T_w_imu.[1].[0],-0.3797238967849509],[packet.wl,0.01654505037523],[T_w_imu.[0].[3],-74.84312113327906],[packet.pitch,0.011004],[T_w_imu.[1].[1],-0.924912532216168],[T_w_imu.[3].[2],0.0],[packet.vu,0.040010965581069],[packet.orimode,6],[T_w_imu.[2].[3],0.04879760741999917]]]"
    val vp1 = OxtsDataParser(str1).get
    assertNotNull(vp1)
    val dataItems = vp1.asInstanceOf[DATAITEMS]
    assertTrue(dataItems.items.nonEmpty)

    val oxtsData: Seq[(String, Double)] = OxtsDataParser.getPairs(str1).sortBy(_._1)
    assertTrue(oxtsData.nonEmpty)
  }

  @Test
  def testValueParsingVideoData(): Unit = {
    val str1 = "[LEFT]"
    val str2 = "[[RIGHT],[LEFT]]"

    val left = ArrayDataParser.getSingleValue(str1)
    assertEquals("LEFT", left)

    val array = ArrayDataParser.getArrayValue(str2)
    assertTrue(array.nonEmpty)
    assertEquals("RIGHT", array.head)
    assertEquals("LEFT", array.tail.head)
  }

}
