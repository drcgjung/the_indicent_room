package com.tsi.bdsp.data

import scala.reflect.{ClassTag, classTag}
import scala.reflect.runtime.universe._
class MyType[T]

object SimpleDemo {

  def arrayMake[T: ClassTag](first: T, second: T) = {
    val r = new Array[T](2)
    r(0) = first
    r(1) = second
    r
  }
  //The above is equivalent to the following. The following is a more native form of writing. The following wording is not recommended
  def arrayMake2[T](first: T, second: T)(implicit m:  ClassTag[T]) = {
    println(m.typeArguments)
    //Actual types of print generics
    println(implicitly[ClassTag[T]].runtimeClass)

    val r = new Array[T](2)
    r(0) = first
    r(1) = second
    r
  }

  //implicit m: ClassTag[T] to implicit m: Manifest[T] is also possible
  def manif[T](x: List[T])(implicit m: Manifest[T]) = {
    if (m <:< manifest[String])
      println("List strings")
    else
      println("Some other type")
  }

  def manif2[T](x: List[T])(implicit ct: ClassTag[T]) = {
    //classManifest has a weaker ability to get information than manifest.
    if (ct <:< classManifest[String])
      println("List strings")
    else
      println("Some other type")
  }

  //implicit m: ClassTag[T] to implicit m: TypeTag[T] is also possible
  def manif3[T](x: List[T])(implicit m: TypeTag[T]) = {
    println(x)
  }

  //ClassTag is our most commonly used. It mainly specifies at runtime information of categories that cannot be determined at compile time.
  // The underscore in my Array[T](elems: *) is a placeholder, representing many elements.
  def mkArray[T: ClassTag](elems: T*) = Array[T](elems: _*)


  def main(args: Array[String]) {
    arrayMake(1, 2).foreach(println)
    arrayMake2(1, 2).foreach(println)
    manif(List("Spark", "Hadoop"))
    manif(List(1, 2))
    manif(List("Scala", 3))
    manif2(List("Spark", "Hadoop"))
    manif2(List(1, 2))
    manif2(List("Scala", 3))

    manif3(List("Spark", "Hadoop"))
    manif3(List(1, 2))
    manif3(List("Scala", 3))

    val m = manifest[MyType[String]]
    println(m) //myscala.scalaexercises.classtag.MyType[java.lang.String]
    val cm = classManifest[MyType[String]]
    println(cm) //myscala.scalaexercises.classtag.MyType[java.lang.String]
    val ct = classTag[MyType[String]]
    println(ct) // com.tsi.bdsp.data.MyType
    val tt = typeTag[MyType[String]]
    println(tt) // TypeTag[com.tsi.bdsp.data.MyType[String]]
    val to = typeOf[MyType[String]]
    println(to) // com.tsi.bdsp.data.MyType[String]
    val ct2 = classTag[MyType[MyType[String]]]
    println(ct2) // com.tsi.bdsp.data.MyType
    val tt2 = typeTag[MyType[MyType[String]]]
    println(tt2.tpe) // com.tsi.bdsp.data.MyType[com.tsi.bdsp.data.MyType[String]]
    val to2 = typeOf[MyType[MyType[String]]]
    println(to2) // com.tsi.bdsp.data.MyType[com.tsi.bdsp.data.MyType[String]]

    def getAnyTags[T](any: T)(implicit m: Manifest[T], ct: ClassTag[T], tt: TypeTag[T]): Unit = {
      println(m)
      println(ct)
      println(tt)
    }

    getAnyTags(Array(1, "2", 3L))

    /*
    In fact, manifest is problematic. manifest misjudges actual types (such as dependency paths), so it later introduced ClassTag and TypeTag.
    Replace manifest with TypeTag and classManifest with ClassTag
    */
    mkArray(1, 2, 3, 4, 5).foreach(println)
  }
}
