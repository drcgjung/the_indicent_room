package com.tsi.bdsp.data

import com.tsi.bdsp.messages.{MsgType, SparkTest}
import org.apache.spark.sql.{Dataset, Row}
import org.junit.Assert._
import org.junit.Test

class TensorTest extends SparkTest {

  @Test
  def testTensorReflect(): Unit = {
    val tensor1 = new Tensor(Seq("Test"))
    assertEquals(1, tensor1.shape.x)
    assertEquals("scala.collection.Iterable", tensor1.meta.fieldTypes(0)._1)
    assertEquals("java.lang.String", tensor1.meta.fieldTypes(0)._2)

    val tensor2 = new Tensor(Seq(Seq(1, 2, 3)))
    assertEquals(1, tensor2.shape.x)
    assertEquals(3, tensor2.shape.y)
    assertEquals("scala.collection.Iterable", tensor2.meta.fieldTypes(0)._1)
    assertEquals("scala.Int", tensor2.meta.fieldTypes(0)._2)

    val tensor3 = new Tensor(Some(Shape(x=1, y=2, z=3)))
    assertEquals(1, tensor3.data.length)
    assertEquals(2, tensor3.data(0).asInstanceOf[Array[Any]].length)
    assertEquals(3, tensor3.data(0).asInstanceOf[Array[Any]](0).asInstanceOf[Array[Any]].length)
  }

  @Test
  def testGeneralToTensorData(): Unit = {
    val dsGM: Dataset[Row] = getMessageData(MsgType.GENERAL)
    dsGM.printSchema()
    import spark.implicits._
    val dsT: Dataset[TensorTuple] = dsGM.flatMap(row => {
      new Tensor("valueType")
        .fromRow(row)
        .flatMapped()
    })
    val dsTR: Dataset[Row] = dsT.toDF()
    assertFalse(dsTR.isEmpty)
    dsTR.printSchema()
    dsTR.show(false)
  }

  @Test
  def testPointCloudToTensorData(): Unit = {
    val dsPCM: Dataset[Row] = getMessageData(MsgType.POINTCLOUD)
    dsPCM.printSchema()
    import spark.implicits._
    val dsT: Dataset[IntervalTensorTuple] = dsPCM.flatMap(row => {
      new IntervalTensor("valueType")
        .withStartTime("startTime")
        .withEndTime("endTime")
        .fromRow(row)
        .flatMapped()
    })
    val dsTR: Dataset[Row] = dsT.toDF()
    assertFalse(dsTR.isEmpty)
    dsTR.printSchema()
    dsTR.filter(TensorColumns.colField !== "pointCloudData").show(false)
  }

  @Test
  def testVideoToTensorData(): Unit = {
    val dsVM: Dataset[Row] = getMessageData(MsgType.VIDEO)
    dsVM.printSchema()
    import spark.implicits._
    val dsT: Dataset[IntervalTensorTuple] = dsVM.flatMap(row => {
      new IntervalTensor("valueType")
        .withStartTime("startTime")
        .withEndTime("endTime")
        .fromRow(row)
        .flatMapped()
    })
    val dsTR: Dataset[Row] = dsT.toDF()
    assertFalse(dsTR.isEmpty)
    dsTR.printSchema()
    dsTR.filter(TensorColumns.colField !== "imgData").show(false)
  }

  @Test
  def testOxtsDataToTensorData(): Unit = {
    val dsOM: Dataset[Row] = getMessageData(MsgType.OXTSDATA)
    dsOM.printSchema()
    import spark.implicits._
    val dsT: Dataset[IntervalTensorTuple] = dsOM.flatMap(row => {
      new IntervalTensor("valueType")
        .withStartTime("startTime")
        .withEndTime("endTime")
        .fromRow(row)
        .flatMapped()
    })
    val dsTR: Dataset[Row] = dsT.toDF()
    assertFalse(dsTR.isEmpty)
    dsTR.printSchema()
    dsTR.show(false)
  }

}
