package com.tsi.bdsp.messages

import org.apache.spark.sql.{Dataset, Row, SparkSession}
import org.junit.{BeforeClass, Test}

import java.io.File

object SparkTest {

  var spark: SparkSession = _
  val rootPath: String = new File(System.getProperty("user.dir")).getAbsolutePath

  @BeforeClass
  def setupOnce(): Unit = {
    spark = SparkSession.builder()
      .appName("StreamingTest")
      .master("local[4]")
      .getOrCreate()
  }
}

class SparkTest {

  protected val spark: SparkSession = SparkTest.spark
  protected var messageOrcResources = s"${SparkTest.rootPath}/src/test/resources/orc"
  protected var tensorOrcResources = s"${SparkTest.rootPath}/resources/orc"

  def getMessageData(valueType: String): Dataset[Row] = {
    spark.read.option("basePath", messageOrcResources).orc(s"$messageOrcResources/valueType=$valueType")
  }

  def getTensorData(tensorType: String): Dataset[Row] = {
    spark.read.option("basePath", tensorOrcResources).orc(s"${tensorOrcResources}/tensorType=${tensorType}")
  }
}
