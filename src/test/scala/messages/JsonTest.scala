package com.tsi.bdsp.messages

import com.tsi.bdsp.messages.JsonTest.resourcesPath
import org.json4s.jackson.JsonMethods
import org.json4s.{JObject, JValue}
import org.junit.Assert.{assertEquals, assertNotNull, assertTrue}
import org.junit.{BeforeClass, Test}

import java.io.File
import java.sql.Timestamp
import scala.io.{BufferedSource, Source}

object JsonTest {

  var resourcesPath: String = _


  @BeforeClass
  def beforeOnce(): Unit = {
    val root: File = new File(System.getProperty("user.dir"))
    val rootPath: String = root.getAbsolutePath
    resourcesPath = s"${rootPath}/resources"
  }
}

class JsonTest {

  protected def getJsonData(jsonFile: File): JValue = {
    var source: BufferedSource = null
    try {
      source = Source.fromFile(jsonFile)
      JsonMethods.parse(source.reader())
    } finally {
      source.close()
    }
  }

  private val timestamp = Timestamp.valueOf("2011-09-26 13:02:25.964389")

  @Test
  def testGeneralMessageJsonParsing(): Unit = {
    val generalMessageJsonFile = new File(s"${resourcesPath}/kitti_general_message.json")
    val jVal: JValue = getJsonData(generalMessageJsonFile)
    assertNotNull(jVal)
    val generalMessage = GeneralMessage().fromJson(jVal.asInstanceOf[JObject])
    assertNotNull(generalMessage)
    assertEquals("2011_09_26", generalMessage.dataDate)
    assertEquals("0001", generalMessage.driveId)
    assertNotNull(generalMessage.key)
    assertEquals(MsgType.GENERAL, generalMessage.valueType)
    assertNotNull(generalMessage.camToCamCalibTime)
    assertNotNull(generalMessage.imuToVeloCalibTime)
    assertNotNull(generalMessage.veloToCamCalibTime)
    assertTrue(generalMessage.calibData.nonEmpty)
  }

  @Test
  def testOxtsDataMessageJsonParsing(): Unit = {
    val oxtsDataMessageJsonFile = new File(s"${resourcesPath}/kitti_oxtsdata_message.json")
    val jVal: JValue = getJsonData(oxtsDataMessageJsonFile)
    assertNotNull(jVal)
    val oxtsDataMessage = OxtsDataMessage().fromJson(jVal.asInstanceOf[JObject])
    assertNotNull(oxtsDataMessage)
    assertEquals(0, oxtsDataMessage.index)
    assertNotNull(oxtsDataMessage.key)
    assertEquals(timestamp, oxtsDataMessage.startTime)
    assertTrue(oxtsDataMessage.oxts.nonEmpty)
  }

  @Test
  def testPointCloudMessageJsonParsing(): Unit = {
    val generalMessageJsonFile = new File(s"${resourcesPath}/kitti_pointcloud_message.json")
    val jVal: JValue = getJsonData(generalMessageJsonFile)
    assertNotNull(jVal)
    val pointCloudMessage = PointCloudMessage().fromJson(jVal.asInstanceOf[JObject])
    assertNotNull(pointCloudMessage)
    assertEquals(0, pointCloudMessage.index)
    assertEquals(timestamp, pointCloudMessage.startTime)
    assertNotNull(pointCloudMessage.key)
    assertEquals(MsgType.POINTCLOUD, pointCloudMessage.valueType)
    assertTrue(pointCloudMessage.pointCloudData.nonEmpty)
  }

  @Test
  def testVideoMessageJsonParsing(): Unit = {
    val videoMessageJsonFile = new File(s"${resourcesPath}/kitti_video_message.json")
    val jVal: JValue = getJsonData(videoMessageJsonFile)
    assertNotNull(jVal)
    val videoMessage = VideoMessage().fromJson(jVal.asInstanceOf[JObject])
    assertNotNull(videoMessage)
    assertEquals(0, videoMessage.index)
    assertEquals(timestamp, videoMessage.startTime)
    assertNotNull(videoMessage.key)
    assertEquals(MsgType.VIDEO, videoMessage.valueType)
    assertEquals("png", videoMessage.imgType)
    assertEquals(false, videoMessage.coloured)
    assertEquals("LEFT", videoMessage.side)
    assertTrue(videoMessage.imgData.nonEmpty)
  }
}
