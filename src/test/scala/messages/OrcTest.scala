package com.tsi.bdsp.messages

import com.tsi.bdsp.data.TensorColumns
import org.apache.spark.sql.functions.first
import org.apache.spark.sql.{Dataset, Row}
import org.junit.Assert.{assertFalse, assertTrue}
import org.junit.Test

class OrcTest extends SparkTest {

  @Test
  def testGeneralMessage(): Unit = {
    val dsGM: Dataset[Row] = getMessageData(MsgType.GENERAL)
    assertFalse(dsGM.isEmpty)
    dsGM.printSchema()
    dsGM.show(false)
    val gmCount = dsGM.count()
    assertTrue(gmCount == 1)
  }

  @Test
  def testGeneralTensor(): Unit = {
    val dsGT: Dataset[Row] = getTensorData(MsgType.GENERAL)
    assertFalse(dsGT.isEmpty)
    dsGT.printSchema()
    dsGT.show(false)
    val gtCount = dsGT.filter(TensorColumns.colField === "dataDate").count()
    assertTrue(gtCount == 1)
  }

  @Test
  def testOxtsDataMessages(): Unit = {
    val dsOM: Dataset[Row] = getMessageData(MsgType.OXTSDATA)
    assertFalse(dsOM.isEmpty)
    dsOM.printSchema()
    dsOM.show(false)
    val omCount = dsOM.count()
    assertTrue(omCount == 108)
  }

  @Test
  def testOxtsDataTensors(): Unit = {
    val dsOT: Dataset[Row] = getTensorData(MsgType.OXTSDATA)
    assertFalse(dsOT.isEmpty)
    dsOT.printSchema()
    dsOT.show(false)
    val otCount = dsOT.filter(TensorColumns.colField === "index").count()
    assertTrue(otCount == 108)
  }

  @Test
  def testPointCloudMessages(): Unit = {
    var dsPCM: Dataset[Row] = getMessageData(MsgType.POINTCLOUD)
    assertFalse(dsPCM.isEmpty)
    dsPCM.printSchema()
    dsPCM = dsPCM.select("index", "startTime", "endTime", "valueType").orderBy("index")
    dsPCM.show(false)
    val pcmCount = dsPCM.count()
    assertTrue(pcmCount == 108) // it must be 108 messages
  }

  @Test
  def testPointCloudTensors(): Unit = {
    val dsPCT: Dataset[Row] = getTensorData(MsgType.POINTCLOUD)
    assertFalse(dsPCT.isEmpty)
    dsPCT.printSchema()
    dsPCT.filter(TensorColumns.colField !== "pointCloudData").show(false)
    val pctCount = dsPCT.filter(TensorColumns.colField === "index").count()
    assertTrue(pctCount == 108)
  }

  @Test
  def testVideoMessages(): Unit = {
    var dsVM: Dataset[Row] = getMessageData(MsgType.VIDEO)
    assertFalse(dsVM.isEmpty)
    dsVM.printSchema()
    dsVM = dsVM.select("index", "startTime", "endTime", "valueType", "coloured", "side", "imgType").orderBy("index")
    dsVM.show(false)
    val vmCount = dsVM.count()
    assertTrue(vmCount == 4 * 108) // for each message 4 picture must exist
  }

  @Test
  def testVideoTensors(): Unit = {
    val dsVT: Dataset[Row] = getTensorData(MsgType.VIDEO)
    assertFalse(dsVT.isEmpty)
    dsVT.printSchema()
    dsVT.filter(TensorColumns.colField !== "imgData").show(false)
    val vtCount = dsVT.filter(TensorColumns.colField === "index").count()
    assertTrue(vtCount == 4 * 108)
  }
}
