import logging
import os
import unittest
from pathlib import Path
from unittest.mock import patch

from kitti.dataparser import DataParser
from kitti.messenger import Messenger
from kitti.model import KittiData, GeneralMessage, VideoMessage, MsgType, ImgSide, PointCloudMessage, OxtsDataMessage


class KittiTest(unittest.TestCase):

    @classmethod
    def setUpClass(self) -> None:
        logging.basicConfig(
            level=logging.INFO,
            format='[%(asctime)s] %(levelname)-8s %(name)-12s %(message)s'
        )
        self.log = logging.getLogger(__name__)
        self.log.addHandler(logging.StreamHandler())

    def setUp(self):
        path_cwd = Path(os.getcwd())
        self.root = str(path_cwd.parent.parent.parent)
        self.res_dir_path = Path(f'{self.root}/main/python/resources'.replace('\\', '/'))
        self.data_dir: str = f'{str(self.res_dir_path)}/2011_09_26'
        self.drive_id = '0001'

    def test_data_parser(self):
        self.assertTrue(self.res_dir_path.exists() and self.res_dir_path.is_dir(),
                        'Resource directory must exists in project root!')
        data_parser = DataParser(self.data_dir, self.drive_id)
        self.assertIsNotNone(data_parser)

        kitti_data: KittiData = data_parser.parse()
        self.assertEqual('2011_09_26', kitti_data.date)
        self.assertEqual('0001', kitti_data.driveId)
        self.assertIsNotNone(kitti_data.key)
        self.assertIsNotNone(kitti_data.genCam0)
        self.assertIsNotNone(kitti_data.idxCam0)
        self.assertIsNotNone(kitti_data.genCam1)
        self.assertIsNotNone(kitti_data.idxCam1)
        self.assertIsNotNone(kitti_data.genCam2)
        self.assertIsNotNone(kitti_data.idxCam2)
        self.assertIsNotNone(kitti_data.genCam3)
        self.assertIsNotNone(kitti_data.idxCam3)
        self.assertIsNotNone(kitti_data.genVelo)
        self.assertIsNotNone(kitti_data.idxVelo)
        self.assertIsNotNone(kitti_data.iterGray)
        self.assertIsNotNone(kitti_data.iterRGB)
        self.assertIsNotNone(kitti_data.timestamps)
        self.assertIsNotNone(kitti_data.oxts)
        self.assertIsNotNone(kitti_data.calibData)
        self.assertIsNotNone(kitti_data.datasetType)
        self.assertIsNotNone(kitti_data.imageType)

    def test_messenger(self):
        data_parser = DataParser(self.data_dir, self.drive_id)
        self.assertIsNotNone(data_parser)
        kitti_data = data_parser.parse()
        first_timestamp = kitti_data.timestamps[0]
        last_timestamp = kitti_data.timestamps[-1]

        messenger = Messenger(kitti_data)
        self.assertIsNotNone(messenger)

        general_message: GeneralMessage = next(iter(messenger.yield_general_message()))
        self.assertIsNotNone(general_message)
        self.assertEqual('2011_09_26', general_message.dataDate)
        self.assertEqual('0001', general_message.driveId)
        self.assertEqual(kitti_data.key, general_message.key)
        self.assertIsNotNone(general_message.camToCamCalibTime)
        self.assertIsNotNone(general_message.imuToVeloCalibTime)
        self.assertIsNotNone(general_message.veloToCamCalibTime)
        self.assertIsNotNone(general_message.calibData)
        self.assertEqual(MsgType.GENERAL, general_message.valueType)

        pointcloud_message: PointCloudMessage = next(iter(messenger.yield_pointcloud_message()))
        self.assertIsNotNone(pointcloud_message)
        self.assertEqual(kitti_data.key, pointcloud_message.key)
        self.assertEqual(0, pointcloud_message.index)
        self.assertTrue(first_timestamp == pointcloud_message.timestamp < last_timestamp)
        self.assertEqual(MsgType.POINTCLOUD, pointcloud_message.valueType)
        self.assertIsNotNone(pointcloud_message.pointCloudData)

        oxtsdata_message: OxtsDataMessage = next(iter(messenger.yield_oxtsdata_message()))
        self.assertIsNotNone(oxtsdata_message)
        self.assertEqual(kitti_data.key, oxtsdata_message.key)
        self.assertEqual(0, oxtsdata_message.index)
        self.assertTrue(first_timestamp == oxtsdata_message.timestamp < last_timestamp)
        self.assertIsNotNone(oxtsdata_message.oxt)
        self.assertEqual(MsgType.SCALAR, oxtsdata_message.valueType)

        video_message: VideoMessage = next(iter(messenger.yield_video_message()))
        self.assertIsNotNone(video_message)
        self.assertEqual(kitti_data.key, video_message.key)
        self.assertEqual(0, video_message.index)
        self.assertTrue(first_timestamp == video_message.timestamp < last_timestamp)
        self.assertFalse(video_message.coloured)
        self.assertEqual(ImgSide.LEFT, video_message.side)
        self.assertEqual('png', video_message.imgType.lower())
        self.assertEqual(MsgType.VIDEO, video_message.valueType)
        self.assertIsNotNone(video_message.imgData)

    def test_messaging(self):
        data_parser = DataParser(self.data_dir, self.drive_id)
        self.assertIsNotNone(data_parser)
        kitti_data = data_parser.parse()
        messenger = Messenger(kitti_data)
        self.assertIsNotNone(messenger)

        generators = [
            (MsgType.GENERAL, messenger.yield_general_message()),
            (MsgType.SCALAR, messenger.yield_oxtsdata_message()),
            (MsgType.POINTCLOUD, messenger.yield_pointcloud_message()),
            (MsgType.VIDEO, messenger.yield_video_message()),
        ]

        for msg_type, gen in generators:
            for idx, json_msg in enumerate(messenger.yield_json_message(gen)):
                self.assertIsNotNone(json_msg)
                msg_len = len(json_msg)
                self.log.info(f'created {msg_type.name} {idx} json message with byte size {msg_len}')

    def test_message_sending(self):
        data_parser = DataParser(self.data_dir, self.drive_id)
        self.assertIsNotNone(data_parser)
        kitti_data = data_parser.parse()
        messenger = Messenger(kitti_data)
        self.cnt = 0

        def check_msg(*args, **kwargs):
            data = args[0]
            self.assertIsNotNone(data)
            msg_byte_size = len(data)
            self.log.info(f'Socket message #{self.cnt} send with size {msg_byte_size}')
            self.cnt += 1
            return None

        with patch('socket.socket') as mock_socket:
            mock_socket.return_value.recv.return_value = 'some data'.encode()
            mock_socket._closed = False
            mock_socket.sendall.side_effect = check_msg

            self.assertTrue(
                messenger.send_all_data_messages(
                    sock=mock_socket,
                    source='Python',
                    channel='TEST',
                    break_condition=(lambda: messenger.message_count == 1000)
                )
            )
