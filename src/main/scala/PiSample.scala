package com.tsi.bdsp

import com.tsi.bdsp.application.BaseApp
import org.apache.spark.SparkContext
import scala.math.random

/**
  * Sample spark application which computes/approximates PI
  */
object PiSample extends BaseApp {

  val sc=SparkContext.getOrCreate(conf)

  val slices = if (args.length > 0) args(0).toInt else java.lang.Runtime.getRuntime.availableProcessors
  val n = math.min(100000L * slices, Int.MaxValue).toInt // avoid overflow

  val count = sc.parallelize(1 until n, slices).map { i =>
      val x = random * 2 - 1
      val y = random * 2 - 1
      if (x*x + y*y <= 1) 1 else 0
    }.reduce(_ + _)
  
  println(s"Pi is roughly ${4.0 * count / (n - 1)}")

}