package com.tsi.bdsp.streaming

import org.apache.spark._
import org.apache.spark.storage._
import org.apache.spark.streaming._
import org.apache.spark.streaming.receiver._
import org.apache.spark.internal._
import java.net._
import java.io._
import org.apache.log4j.Logger

/**
  * Spark streaming receiver which exposes a socket and accepts clients
  * in a given protocol message type 
  * @param port
  * @tparam ProtocolElement
  */
abstract class ServerSocketReceiver[ProtocolElement<: Serializable](port: Int)
  extends Receiver[ProtocolElement](StorageLevel.MEMORY_AND_DISK_2) with Logging {

  @transient var server:ServerSocket=null

  def onStart() {
    log.info(s"Starting server socket on port ${port} and spawning acceptance thread.")
    server=new ServerSocket(port)
    // Start the thread that accetps new connections
    new Thread("Server Socket Acceptor") {
      override def run() { accept() }
    }.start()
  }

  def onStop() {
    server.close
    // There is nothing much to do as the thread calling receive()
    // is designed to stop by itself if isStopped() returns false
  }

  def accept() {
    log.debug(s"Accepting incoming connections")
    val socket=server.accept()
    log.info(s"Got a new incoming connection on socket ${socket}. Spawning reception thread.")
    new Thread("Client Socket Receiver") {
      override def run() { receive(socket) }
    }.start()
    accept()
  }

  /**
    * abstract method to prepare a reading
    * function on top of the inputstream.
    * The reading function will return null (or an exception)
    * @param stream
    * @return reading function
    */
  def createReader(stream:InputStream) : Function0[Iterable[ProtocolElement]]
 
  // Until stopped or connection broken continue reading
  // 
  def createHandler(reader:Function0[Iterable[ProtocolElement]]) : Function1[ProtocolElement,Iterable[ProtocolElement]]

  /** Create a socket connection and receive data until receiver is stopped */
  private def receive(socket:Socket) {
    var reader=createReader(socket.getInputStream)
    try {
      var handler=createHandler(reader)
      var commands = reader()
      log.trace(s"Read commands ${commands} from socket ${socket} when stopping status is ${isStopped}")
      while(!isStopped && commands != null) {
        commands.map(handler(_).map(store(_)))
        commands = reader()
      }
      socket.close()
    } catch {
      case _:Throwable => log.debug(s"Closing receiption thread on socket ${socket}")
    }
  }
}