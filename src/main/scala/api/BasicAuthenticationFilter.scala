package com.tsi.bdsp.api

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Base64;

import javax.servlet._
import javax.servlet.http._

/**
  * A hardcoded basic authentication filter to be used
  * in servlet pipelines
  */
class BasicAuthenticationFilter extends Filter {
    
    var userName:String = null
    var password:String = null
    
    // Init method is invoked for one time initialization
    def init(config:FilterConfig) {
      val decoder=Base64.getDecoder()
      userName=new String(decoder.decode("YmRzcA=="),"UTF-8")
      password=new String(decoder.decode("U2hhbm5vblNheXMyRnJlcXVlbmN5"),"UTF-8")
    }
    
    /*The doFilter method of the Filter is called by the container each time a request/response pair is passed 
    through the chain due to a client request for a resource at the end of the chain. */
    def doFilter(req:ServletRequest, res:ServletResponse, chain:FilterChain) {
      val hreq = req.asInstanceOf[HttpServletRequest]
      val hres = res.asInstanceOf[HttpServletResponse]
      val meth=hreq.getMethod()
      if(!"OPTIONS".equals(meth)) {
        var authorization = hreq.getHeader("Authorization")
        if ( authorization != null ) {
            val index = authorization.indexOf(' ')
            if ( index > 0 ) {
              authorization=authorization.substring(index+1)
              val decoder=Base64.getDecoder()
              val creds = new String(decoder.decode(authorization),"UTF-8").split(":")
              if ( creds.length == 2 && userName.equals(creds(0)) && password.equals( creds(1) ) ) {
                chain.doFilter(req, res)
                return
              }
            }
        }
        hres.setHeader( "WWW-Authenticate", "Basic realm=\"ProtectedSpark\"" )
        hres.sendError( HttpServletResponse.SC_UNAUTHORIZED )
      } else {
        hres.setHeader("Access-Control-Allow-Headers","access-control-allow-origin,Authorization")
        hres.setHeader("Access-Control-Allow-Methods","*")
        hres.setHeader("Access-Control-Allow-Origin",hreq.getHeader("Origin"))
        hres.setHeader("Access-Control-Allow-Credentials","true")
      }
    }

    def destroy() {
        // TODO Auto-generated method stub 
    }

}
