package com.tsi.bdsp.api

/**
 * An API deployment is any service which
 * is given a deployer to produce a 
 * set of endpoints
 */
trait Deployment {
  
  /**
   * deploy all endpoints under the given deployer
   * @param deployer 
   */
  def deploy(deployer:Deployer)
  
}
