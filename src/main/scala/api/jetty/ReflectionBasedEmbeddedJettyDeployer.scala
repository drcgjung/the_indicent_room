package com.tsi.bdsp.api.jetty

import com.tsi.bdsp.api.{Deployer,Deployment,BasicAuthenticationFilter}

import org.apache.spark.SparkContext

import org.scalatra.Handler

import javax.servlet.DispatcherType
import javax.servlet.ServletContext
import javax.servlet.Filter
import javax.servlet.Servlet

object ReflectionBasedEmbeddedJettyDeployer {
   val jettyPackageSpark3 = "org.sparkproject.jetty."  
   val jettyPackageSpark2 = "org.spark_project.jetty."
}

class ReflectionBasedEmbeddedJettyDeployer(
    val sc:SparkContext, 
    val jettyPackage:String = ReflectionBasedEmbeddedJettyDeployer.jettyPackageSpark3, 
    val filters:Iterable[(String,Class[_ <: Filter])] = Some("/*"->classOf[BasicAuthenticationFilter]),
    val path:String = "/bdsp",
    val deployment:Deployment,
    val args:Iterable[String]=None
) extends Deployer {
    
  val enumDispatcher = java.util.EnumSet.of(DispatcherType.ASYNC, DispatcherType.ERROR, DispatcherType.FORWARD, DispatcherType.INCLUDE, DispatcherType.REQUEST)
  
  val handlerClass = jettyPackage + "server.Handler"
  val contextHandlerClass = jettyPackage + "server.handler.ContextHandler"
  val resourceHandlerClass = jettyPackage + "server.handler.ResourceHandler"
  val webappContextClass = jettyPackage + "webapp.WebAppContext"
  val servletHolderClass = jettyPackage + "servlet.ServletHolder"
  val servletContextHandlerClass =jettyPackage + "servlet.ServletContextHandler"
  val defaultServletClass =jettyPackage +"servlet.DefaultServlet"  
   
  val staticContextHandler = Class.forName(servletContextHandlerClass).newInstance().asInstanceOf[AnyRef]
   
  staticContextHandler.getClass.getMethod("setInitParameter",classOf[String],classOf[String]).invoke(staticContextHandler,"org.eclipse.jetty.servlet.Default.gzip", "false")
  staticContextHandler.getClass.getMethod("setContextPath", classOf[String]).invoke(staticContextHandler, "/bdsp")

  /*filters.foreach( filter => {
        staticContextHandler.getClass.getMethod("addFilter", classOf[String], classOf[String],classOf[java.util.EnumSet[DispatcherType]]).invoke(staticContextHandler,
        filter._2.getName(),filter._1,enumDispatcher)
   })*/

   deployment.deploy(this)

   val ui = sc.getClass.getDeclaredMethod("ui").invoke(sc).asInstanceOf[Option[Any]].get
   ui.getClass.getMethod("attachHandler", staticContextHandler.getClass).invoke(ui,staticContextHandler)

   /** mount by knowing that scalatra handlers are servlets */
   def mount(endpoint:Handler, ctx:String, name:String) {
    mount(endpoint.asInstanceOf[Servlet],ctx,name)
   }

   /** mount servlet by holder */
   def mount(endpoint:Servlet, ctx:String, name:String, isAsync:Boolean, initParams:Iterable[(String,String)]) {
    val holder = Class.forName(servletHolderClass).getConstructor(classOf[javax.servlet.Servlet]).newInstance(endpoint).asInstanceOf[AnyRef]
    holder.getClass.getMethod("setAsyncSupported", classOf[Boolean]).invoke(holder, Boolean.box(isAsync))
    initParams.foreach( param => {
        holder.getClass.getMethod("setInitParameter",classOf[String],classOf[String]).invoke(holder,param._1, param._2)
    })
    staticContextHandler.getClass.getMethod("addServlet", holder.getClass, classOf[String]).invoke(staticContextHandler, holder, ctx+"/*")
   }

   def createDefaultServlet=Class.forName(defaultServletClass).newInstance().asInstanceOf[Servlet]
}
