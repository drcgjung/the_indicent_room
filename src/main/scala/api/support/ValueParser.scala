package com.tsi.bdsp.api.support

import scala.util.parsing.combinator.RegexParsers

object ValueParser extends RegexParsers {
  lazy val b = "[^\\[\\]\\(\\)]+".r

  def brackets: ValueParser.Parser[String] =
    ("(" ~ rep1(b | brackets) ~ ")" | "[" ~ rep1(b | brackets) ~ "]") ^^ { case bl ~ v ~ br => v.mkString(",") }

  def all: ValueParser.Parser[List[String]] = rep(brackets)

  def apply(s: String): ValueParser.ParseResult[List[String]] = parseAll(all, s)
}
