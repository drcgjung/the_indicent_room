package com.tsi.bdsp.api.support

import org.json4s.{DefaultFormats, Formats}
import org.scalatra.json.JacksonJsonSupport
import org.scalatra.swagger.{ApiInfo, Swagger, SwaggerSupport}

trait ApiDocSupport extends SwaggerSupport with JacksonJsonSupport {
  implicit val jsonFormats: Formats = DefaultFormats + TimestampJsonSerializer

  options("/*"){
    if(request.headers.get("Origin").isDefined || request.headers.get("Referer").isDefined) {
      response.headers += ("Access-Control-Allow-Origin" -> request.headers.get("Origin").getOrElse(request.headers("Referer")))
    }
  }

  before() {
    contentType = formats("json")
    if(request.headers.get("Origin").isDefined || request.headers.get("Referer").isDefined) {
      response.headers += ("Access-Control-Allow-Origin" -> request.headers.get("Origin").getOrElse(request.headers("Referer")))
    }
  }
}
