package com.tsi.bdsp.api.support

import scala.util.parsing.combinator.RegexParsers

sealed trait VideoDataItem
case class ArrayValues(values: List[VideoDataItem]) extends VideoDataItem
case class StringValue(value: String) extends VideoDataItem

object ArrayDataParser extends RegexParsers {

  def value: ArrayDataParser.Parser[VideoDataItem] = (arr | string)
  def arr: ArrayDataParser.Parser[ArrayValues] = "[" ~> repsep(value, ",") <~ "]" ^^ ArrayValues
  def string: ArrayDataParser.Parser[StringValue] = "[a-z0-9A-Z+/=]+".r ^^ StringValue

  def apply(raw: String) = parseAll(value, raw)

  def getSingleValue(raw: String): String = {
    apply(raw)
      .map(_.asInstanceOf[ArrayValues])
      .map(_.values.head.asInstanceOf[StringValue].value)
      .getOrElse(null)
  }

  def getArrayValue(raw: String): Array[String] = {
    val videoDataItem = apply(raw).get
    def getItems(item: VideoDataItem): Seq[String] = {
      item match {
        case arrayValues: ArrayValues =>
          arrayValues.values.flatMap(getItems)
        case _ =>
          val stringValue = item.asInstanceOf[StringValue]
          Seq(stringValue.value)
      }
    }
    getItems(videoDataItem).toArray
  }
}
