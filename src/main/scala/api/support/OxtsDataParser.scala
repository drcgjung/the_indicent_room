package com.tsi.bdsp.api.support

import scala.util.parsing.combinator.RegexParsers

sealed trait DataTuple
case class DATAITEMS(items: List[DataTuple]) extends DataTuple
case class DATAENTRY(key: String, value: Double) extends DataTuple
case class DATAKEY(key: String) extends DataTuple
case class DATAVALUE(value: Double) extends DataTuple

// Scala parser combinators for multiple array items
object OxtsDataParser extends RegexParsers {

  def value: Parser[DataTuple] = (arr | pair)
  def arr: OxtsDataParser.Parser[DATAITEMS] = "[" ~> repsep(value, ",") <~ "]" ^^ DATAITEMS
  def pair: OxtsDataParser.Parser[DATAENTRY] = keyLiteral ~ "," ~ doubleValue ^^ { case DATAKEY(k) ~ _ ~ DATAVALUE(v) =>  DATAENTRY(k, v) }
  def keyLiteral: OxtsDataParser.Parser[DATAKEY] = "(\\w+)((\\.\\[\\d+\\])|(\\.\\w+))*".r ^^ DATAKEY
  def doubleValue: OxtsDataParser.Parser[DATAVALUE] = "^(-)?(\\d+)(\\.\\d+)?".r ^^ (d => DATAVALUE(d.toDouble))

  def apply(raw: String): OxtsDataParser.ParseResult[DataTuple] = parseAll(value, raw)

  /**
    * Parses the raw multiple array string into seq. of tuples
    * @param raw raw nd. array string
    * @return [[Seq]] of [[Tuple2]]
    */
  def getPairs(raw: String): Seq[(String, Double)] = {
    val items: DATAITEMS = apply(raw).getOrElse(DATAITEMS(List())).asInstanceOf[DATAITEMS]
    def toPairs(items: DataTuple): Seq[(String, Double)] = {
      items match {
        case dataItems: DATAITEMS => dataItems.items.flatMap(toPairs)
        case dataEntry: DATAENTRY => Seq((dataEntry.key, dataEntry.value))
        case _ => Seq()
      }
    }
    toPairs(items)
  }
}