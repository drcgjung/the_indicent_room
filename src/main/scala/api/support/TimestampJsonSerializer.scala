package com.tsi.bdsp.api.support

import org.json4s.{CustomSerializer, JNull, JString}

import java.sql.Timestamp
import java.time.format.DateTimeFormatter

/**
  * While JSON serializing datetime of Timestamp with micoseconds get normally cut only to seconds
  */
object TimestampJsonSerializer extends CustomSerializer[Timestamp](formats => (
  {
    case JString(ts) => Timestamp.valueOf(ts)
    case JNull => null
  }, {
    case ts: Timestamp => JString(ts.toLocalDateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))
  }
))
