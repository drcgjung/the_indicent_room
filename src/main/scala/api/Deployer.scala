package com.tsi.bdsp.api

import javax.servlet.Servlet
import org.scalatra.Handler

/**
 * An API deployer is any service which may
 * expose endpoints in forms of services to
 * the outer world.
 */
trait Deployer {
  
  /**
   * mount a new handler 
   * @param endpoint handler
   * @param ctx uri context
   * @param nickname of the handler
   */
  def mount(endpoint:Handler, ctx:String, name:String)
  
  /**
   * mount a new servlet
   * @param endpoint servlet
   * @param ctx uri context
   * @param nickname of the servlet
   */
  def mount(endpoint:Servlet, ctx:String, name:String, isAsync:Boolean =false, initParams:Iterable[(String,String)]=None)

  /**
    * @return a new instance of a default servlet
    */
  def createDefaultServlet:Servlet

}
