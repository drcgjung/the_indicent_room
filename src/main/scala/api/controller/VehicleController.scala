package com.tsi.bdsp.api.controller

import com.tsi.bdsp.api.service.vehicle.{VehicleMetaData, VehicleOxtsData, VehiclePointCloudData, VehicleService, VehicleVideoData}
import com.tsi.bdsp.api.support.ApiDocSupport
import org.apache.spark.sql.SparkSession
import org.scalatra.ScalatraServlet
import org.scalatra.swagger.{AllowableValues, DataType, ParamType, Parameter, Swagger}

import java.sql.Timestamp

class VehicleController(implicit val swagger: Swagger, implicit val sparkSession: SparkSession) extends ScalatraServlet with ApiDocSupport {

  override protected def applicationDescription: String = "Vehicle data API"

  private val vehicleService = new VehicleService(sparkSession)

  get("/", operation(
    apiOperation[Seq[Map[String,Any]]](nickname = "getVehicle")
      summary "Get current vehicle data"
    )) {
    vehicleService.getAllVehicleMetadata()
  }

  get("/channels/:key", operation(
    apiOperation[Seq[Map[String,Any]]](nickname = "getChannels")
      summary "Get vehicle channels"
    parameters(
      Parameter(name = "key", `type` = DataType.String, description = Some("vehicle data key"),
        paramType = ParamType.Path, required = true)
    )
    )) {
    val key: String = params("key")
    vehicleService.getAllChannelMetadata(key)
  }

  get("/oxtsdata/:key", operation(
    apiOperation[Seq[Map[String,Any]]](nickname = "getOxtsData")
    summary "Get vehicle Kitti oxts data sets"
    parameters(
      Parameter(name = "key", `type` = DataType.String, description = Some("vehicle data key"),
        paramType = ParamType.Path, required = true)
    )
  )) {
    val key: String = params("key")
    vehicleService.getOxtsDataByKey(key)
  }

  get("/videodata/:key", operation(
    apiOperation[Seq[Map[String,Any]]](nickname = "getVideoData")
    summary "Get vehicle Kitti video frames"
    parameters(
      Parameter(name = "key", `type` = DataType.String, description = Some("vehicle data key"),
        paramType = ParamType.Path, required = true
      ),
      Parameter(name = "coloured", `type` = DataType.Boolean, description = Some("flag if coloured frames should used"),
        paramType = ParamType.Query, required = false, defaultValue = Some("true")
      ),
      Parameter(name = "side", `type` = DataType.String, description = Some("side of picture LEFT or RIGHT"),
        paramType = ParamType.Query, required = false, allowableValues = AllowableValues("LEFT", "RIGHT"), defaultValue = Some("LEFT")
      ),
      Parameter(name = "pos", `type` = DataType.Long, description = Some("current pagination position"),
        paramType = ParamType.Query, required = false, defaultValue = Some("0")
      ),
      Parameter(name = "length", `type` = DataType.Int, description = Some("Pagination length of chunk"),
        paramType = ParamType.Query, required = false, defaultValue = Some("10")
      )
    )
  )) {
    val key: String = params("key")
    val coloured: Boolean = params("coloured").toBoolean
    val side: String = params("side")
    val pos: Long = params("pos").toLong
    val length: Int = params("length").toInt
    vehicleService.getVideoDataChunk(key, coloured, side, pos, length)
  }

  get("/pointclouddata/:key", operation(
    apiOperation[Seq[Map[String,Any]]](nickname = "getPointCloudData")
    summary "Get vehicle Kitti point cloud data as compressed Base64 string"
      parameters(
      Parameter(name = "key", `type` = DataType.String, description = Some("vehicle data key"),
        paramType = ParamType.Path, required = true
      ),
      Parameter(name = "pos", `type` = DataType.Long, description = Some("current pagination position"),
        paramType = ParamType.Query, required = false, defaultValue = Some("0")
      ),
      Parameter(name = "length", `type` = DataType.Int, description = Some("Pagination length of chunk"),
        paramType = ParamType.Query, required = false, defaultValue = Some("10")
      )
    )
  )) {
    val key: String = params("key")
    val pos: Long = params("pos").toLong
    val length: Int = params("length").toInt
    vehicleService.getPointCloudChunk(key, pos, length)
  }
}
