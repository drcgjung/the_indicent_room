package com.tsi.bdsp.api.controller

import org.scalatra.ScalatraServlet
import org.scalatra.swagger.{Api, ApiInfo, JacksonSwaggerBase, Swagger, SwaggerEngine}

object ApiSwaggerInfo extends ApiInfo(
  title = "Kitti Vehicle REST Api",
  description = "Kitti REST Api endpoints",
  termsOfServiceUrl = "REST Api",
  contact = "somebody",
  license = "Commercial",
  licenseUrl = "Commercial"
)

class ApiSwagger extends Swagger(Swagger.SpecVersion, "1.0.0", ApiSwaggerInfo)
class ApiSwaggerController(implicit val swagger: Swagger) extends ScalatraServlet with JacksonSwaggerBase
