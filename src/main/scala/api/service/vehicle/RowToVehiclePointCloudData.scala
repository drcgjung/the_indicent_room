package com.tsi.bdsp.api.service.vehicle

import com.tsi.bdsp.api.support.ArrayDataParser
import com.tsi.bdsp.data.SparkUtils.ExtendedColumn
import com.tsi.bdsp.data.TensorColumns
import org.apache.spark.sql.Row

import java.sql.Timestamp

class RowToVehiclePointCloudData extends Function1[Row, Seq[VehiclePointCloudData]] with Serializable {

  // We need 2 attributes
  @transient lazy val fieldsNum = 2

  override def apply(aggRow: Row): Seq[VehiclePointCloudData] = {
    val key = aggRow.getAs[String](TensorColumns.colKey.colName)
    val startTime = aggRow.getAs[Timestamp](TensorColumns.colStartTime.colName)
    val fields: Seq[String] = aggRow.getSeq(aggRow.fieldIndex(TensorColumns.colField.colName))
    val values: Seq[String] = aggRow.getSeq(aggRow.fieldIndex(TensorColumns.colValue.colName))

    val vehiclePointCloudData = fields.zip(values).sliding(fieldsNum, fieldsNum).toSeq.map(pointCloudFieldValues => {
      val getRawValue = (colName: String) => pointCloudFieldValues.find(_._1.equals(colName)).map(_._2)
      val index = ArrayDataParser.getSingleValue(getRawValue("index").get).toInt
      val pointCloudData = ArrayDataParser.getSingleValue(getRawValue("pointCloudData").get)
      VehiclePointCloudData(
        key = key,
        startTime = startTime,
        index = index,
        pointCloudData = pointCloudData
      )
    })

    vehiclePointCloudData
  }
}
