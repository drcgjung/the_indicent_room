package com.tsi.bdsp.api.service.vehicle

import java.sql.Timestamp

case class OxtsDataItem(var channel: String, val value: Double)

case class VehicleOxtsData(
  var startTime: Timestamp = null,
  var endTime: Timestamp = null,
  var oxtsData: Array[OxtsDataItem] = Array()
)
