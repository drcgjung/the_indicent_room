package com.tsi.bdsp.api.service.vehicle

import java.sql.Timestamp

case class VehiclePointCloudData(
  var key: String = null,
  var index: Int = -1,
  var startTime: Timestamp = null,
  var endTime: Timestamp = null,
  var pointCloudData: String = null,
)
