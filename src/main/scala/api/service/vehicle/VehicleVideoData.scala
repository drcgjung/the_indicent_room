package com.tsi.bdsp.api.service.vehicle

import java.sql.Timestamp

case class VehicleVideoData(
  var key: String = null,
  var index: Int = -1,
  var startTime: Timestamp = null,
  var endTime: Timestamp = null,
  var imgData: String = null,
  var imgType: String = null,
  var isColoured: Boolean = false,
  var imgSide: String = null
)
