package com.tsi.bdsp.api.service.vehicle

import com.tsi.bdsp.api.support.ArrayDataParser
import com.tsi.bdsp.data.SparkUtils.ExtendedColumn
import com.tsi.bdsp.data.TensorColumns
import org.apache.spark.sql.Row

import java.sql.Timestamp

/**
  * Flat map function from [[Row]] to [[VehicleVideoData]]
  */
class RowToVehicleVideoData extends Function1[Row, Seq[VehicleVideoData]] with Serializable {

  // We need 5 attributes
  @transient lazy val fieldsNum = 5

  // attention this must be serializable
  override def apply(aggRow: Row): Seq[VehicleVideoData] = {

    val key = aggRow.getAs[String](TensorColumns.colKey.colName)
    val startTime = aggRow.getAs[Timestamp](TensorColumns.colStartTime.colName)
    // 4 pictures with 5 field names and values
    val fields: Seq[String] = aggRow.getSeq(aggRow.fieldIndex(TensorColumns.colField.colName))
    val values: Seq[String] = aggRow.getSeq(aggRow.fieldIndex(TensorColumns.colValue.colName))


    val vehicleVideoData = fields.zip(values).sliding(fieldsNum, fieldsNum).toSeq.map(imgFieldValues => {
      val getRawValue = (colName: String) => imgFieldValues.find(_._1.equals(colName)).map(_._2)
      val index = ArrayDataParser.getSingleValue(getRawValue("index").get).toInt
      val imgData = ArrayDataParser.getSingleValue(getRawValue("imgData").get)
      val imgType = ArrayDataParser.getSingleValue(getRawValue("imgType").get)
      val isColoured = ArrayDataParser.getSingleValue(getRawValue("coloured").get).toBoolean
      val imgSide = ArrayDataParser.getSingleValue(getRawValue("side").get)
      VehicleVideoData(
        key = key,
        startTime = startTime,
        index = index,
        imgData = imgData,
        imgType = imgType,
        imgSide = imgSide,
        isColoured = isColoured
      )
    })

    vehicleVideoData
  }
}
