package com.tsi.bdsp.api.service.vehicle

import com.tsi.bdsp.api.service.SparkService
import com.tsi.bdsp.api.support.{OxtsDataParser, ArrayDataParser}
import com.tsi.bdsp.data.SparkUtils.ExtendedColumn
import com.tsi.bdsp.data.TensorColumns
import com.tsi.bdsp.messages.{MsgType, VideoMessage}
import org.apache.spark.sql.{Dataset, Row, SparkSession}
import org.apache.spark.sql.functions.{collect_list, udf, lit, col}

import java.sql.Timestamp

class VehicleService(private val sparkSession: SparkSession) extends SparkService(sparkSession) {

  /**
    * Gets ORC vehicle meta data with assigned data types
    *
    * @return [[Seq]] of [[VehicleMetaData]]
    */
  def getAllVehicleMetadata(): Seq[Map[String,Any]] = {
    
    var startTime=System.currentTimeMillis()
    System.out.println(s"VehicleService.getAllVehicleMetadata()");

    val df: Dataset[Row] = getOrcTensorData(MsgType.GENERAL)
  
    val results=df.collect.map( row => row.schema.fields.map( field => { field.name -> row.getAs[Any](field.name)}).toMap)

    var endTime=System.currentTimeMillis()
    System.out.println(s"VehicleService.getAllVehicleMetadata() took ${(endTime-startTime)/1000.0}s==> ${results.length}");

    results
  }

   /**
    * Gets ORC vehicle meta data with assigned data types
    *
    * @return [[Seq]] of [[VehicleMetaData]]
    */
  def getAllChannelMetadata(key:String): Seq[Map[String,Any]] = {
    
    var startTime=System.currentTimeMillis()
    System.out.println(s"VehicleService.getAllChannelMetadata(${key})");

    val df: Dataset[Row] = getOrcTensorData(MsgType.CHANNEL).where(TensorColumns.colKey === lit(key))
  
    val results=df.collect.map( row => row.schema.fields.map( field => { field.name -> row.getAs[Any](field.name)}).toMap)

    var endTime=System.currentTimeMillis()
    System.out.println(s"VehicleService.getAllChannelMetadata() took ${(endTime-startTime)/1000.0}s==> ${results.length}");

    results
  }

  /**
    * Gets for a vehicle data key assigned Kitti Oxtsdata sets
    * @param key vehicle data key
    * @return oxts data entries as [[Seq]] of [[VehicleOxtsData]]
    */
  def getOxtsDataByKey(key: String): Seq[Map[String,Any]] = {

    var startTime=System.currentTimeMillis()
    System.out.println(s"VehicleService.getOxtsDataByKey(${key})");

    val df: Dataset[Row] = getOrcTensorData(MsgType.OXTSDATA).filter(TensorColumns.colKey === lit(key)).orderBy(col("startTime"),col("channel"))
    val results=df.collect.map( row => row.schema.fields.map( field => { field.name -> row.getAs[Any](field.name)}).toMap)

    var endTime=System.currentTimeMillis()
    System.out.println(s"VehicleService.getOxtsDataByKey(${key}) took ${(endTime-startTime)/1000.0}s ==> ${results.length}");
    
    results.toSeq
  }

  /**
    * Gets Kitti video frames as chunk of currently 10 frames
    * @param key vehicle data key
    * @param coloured flag if coloured pictures should used
    * @param side LEFT or RIGHT image
    * @param pos pagination position
    * @param length pagination length
    * @return chunked video frames as [[Seq]] of [[VehicleVideoData]]
    */
  def getVideoDataChunk(key: String, coloured: Boolean, side: String, pos: Long, length: Int): Seq[Map[String,Any]] = {

    var startTime=System.currentTimeMillis()
    System.out.println(s"VehicleService.getVideoDataChunk($key, $coloured, $side, $pos, $length)");

    val df: Dataset[Row] = getOrcTensorData(MsgType.VIDEO).filter(
      (TensorColumns.colKey === lit(key)) && 
      (col("coloured") === lit(coloured.toString())) && 
      (col("side")===lit(side)) && 
      (col("index") geq lit(pos.toInt)) && 
      (col("index") lt lit(pos.toInt+length))
    ).orderBy(col("index"))
    
    val results=df.collect.map( row => row.schema.fields.map( field => { field.name -> row.getAs[Any](field.name)}).toMap)

    var endTime=System.currentTimeMillis()
    System.out.println(s"VehicleService.getVideoDataChunk($key, $coloured, $side, $pos, $length) took ${(endTime-startTime)/1000.0}s ==> ${results.length}");

    results.toSeq
  }

  /**
    * Gets Kitti point cloud as chunk of point clouds
    * @param key vehicle data key
    * @param pos pagination pos.
    * @param length pagination length
    * @return chunk of point cloud data
    */
  def getPointCloudChunk(key: String, pos: Long, length: Int): Seq[Map[String,Any]] = {

    var startTime=System.currentTimeMillis()
    System.out.println(s"VehicleService.getPointCloudChunk($key, $pos, $length)");

    val df: Dataset[Row] = getOrcTensorData(MsgType.POINTCLOUD).filter( 
      (TensorColumns.colKey === lit(key)) && 
      (col("index") geq lit(pos.toInt)) && 
      (col("index") lt lit(pos.toInt+length))
    ).orderBy(col("index"))
    
    val results=df.collect.map( row => row.schema.fields.map( field => { field.name -> row.getAs[Any](field.name)}).toMap)

    var endTime=System.currentTimeMillis()
    System.out.println(s"VehicleService.getPointCloudChunk($key, $pos, $length) took ${(endTime-startTime)/1000.0}s ==> ${results.length}");

    results
  }
}
