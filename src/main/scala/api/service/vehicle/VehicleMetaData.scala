package com.tsi.bdsp.api.service.vehicle

case class VehicleMetaData(
  var key: String = null,
  var date: String = null,
  var driveId: String = null,
  var dataTypes: Seq[String] = Seq()
)
