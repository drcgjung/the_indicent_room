package com.tsi.bdsp.api.service

import com.tsi.bdsp.api.support.ValueParser
import com.tsi.bdsp.application.AppSettings
import com.tsi.bdsp.data.SparkUtils._
import com.tsi.bdsp.data.TensorColumns
import com.tsi.bdsp.messages.MsgType
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.{Dataset, Row, SparkSession}

abstract class SparkService(private val sparkSession: SparkSession) {

  private val orcResources: String = sparkSession.sparkContext.getConf.get(AppSettings.ORC_RESOURCES)

  protected def getOrcTensorData(tensorType: String): Dataset[Row] = {
    sparkSession.read.option("basePath", orcResources).orc(s"${orcResources}/tensorType=${tensorType}")
  }

  protected def getSingleValue(value: String): String = {
    val parsed: ValueParser.ParseResult[List[String]] = ValueParser(value)
    parsed.get.head
  }

  /**
    * Get data tensor types from meta data key
    * @param key uuid as string
    * @return all tensor types for key
    */
  protected def getOrcDataTypes(key: String): Seq[String] = {
    val initialDf: Dataset[Row] = sparkSession.emptyDataFrame
      .withColumn(TensorColumns.colKey.colName, lit(null))
      .withColumn(TensorColumns.colTensorType.colName, lit(null))

    val selDf: Dataset[Row] = Set(
      MsgType.GENERAL,
      MsgType.OXTSDATA,
      MsgType.POINTCLOUD,
      MsgType.VIDEO
    )
    .foldLeft(initialDf)((df, tensorType) => {
      df.union(getOrcTensorData(tensorType).select(
        TensorColumns.colKey,
        TensorColumns.colTensorType
      ))
    })
    .distinct()

    selDf.filter(TensorColumns.colKey === key).collect()
      .map(row => row.getAs[String](TensorColumns.colTensorType.colName))
      .toSeq
  }
}
