package com.tsi.bdsp.api.backend

import com.tsi.bdsp.api.controller.{ApiSwagger, ApiSwaggerController, VehicleController}
import com.tsi.bdsp.api.jetty.ReflectionBasedEmbeddedJettyDeployer
import com.tsi.bdsp.api.{Deployer, Deployment}
import org.apache.spark.sql.SparkSession

import java.io.File

class SparkBackendServer(spark: SparkSession, args: Array[String]) extends Deployment {

  private val internalDeployer = new ReflectionBasedEmbeddedJettyDeployer(
    sc = spark.sparkContext,
    deployment = this
  )

    /**
      * deploy all endpoints under the given deployer
      *
      * @param deployer
      */
  override def deploy(deployer: Deployer): Unit = {
    implicit val swagger = new ApiSwagger()
    implicit val sparkSession = spark

    val localWebFile=new File("src/main/resources/web")
    val localSwaggerFile=new File("src/main/resources/swagger-ui")

    val webBase=if(localWebFile.exists()) {
      localWebFile.toURL().toString()
    } else {
      getClass.getClassLoader.getResource("web").toExternalForm
    }
    val swaggerBase=if(localSwaggerFile.exists()) {
      localSwaggerFile.toURL().toString()
    } else {
      getClass.getClassLoader.getResource("swagger-ui").toExternalForm
    }

    System.out.println(s"Web: ${webBase} Swagger: ${swaggerBase}")

    deployer.mount(deployer.createDefaultServlet,"","default",false,
      Seq(
        "resourceBase"->webBase
      )
    )
    deployer.mount(deployer.createDefaultServlet, "/swagger-ui", "swagger-ui", isAsync = false,
      Seq(
        "resourceBase" -> swaggerBase,
        "dirAllowed" -> "true",
        "pathInfoOnly" -> "true"
      )
    )
    deployer.mount(new ApiSwaggerController, "/swagger-api", "swagger-api")
    deployer.mount(new VehicleController, "/vehicle", "vehicle")
  }
}
