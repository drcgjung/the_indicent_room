package com.tsi.bdsp.data

import org.apache.spark.sql.Column
import org.apache.spark.sql.catalyst.expressions.NamedExpression

object SparkUtils {
  implicit class ExtendedColumn(col: Column) {
    def colName: String = {
      col.expr.asInstanceOf[NamedExpression].name
    }
  }
}
