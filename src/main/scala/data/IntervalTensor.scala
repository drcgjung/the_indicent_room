package com.tsi.bdsp.data

import org.apache.spark.sql.Row

import java.sql.Timestamp

case class IntervalTensorTuple(
  var key: String = null,
  var tensorType: String = null,
  var field: String = null,
  var value: String = null,
  var containerType: String = null,
  var elementType: String = null,
  var startTime: Timestamp = null,
  var endTime: Timestamp = null
) extends ITensorTuple

class IntervalTensor(classifier: String, level: Byte = 0, relation: Byte = Relations.PARENT)
  extends TimeTensor(classifier, level, relation) with IntervalTensorAcess {

  protected var endTimeField: String = _
  protected var endTime: Timestamp = _

  override def withStartTime(startTimeField: String): IntervalTensor = {
    super.withStartTime(startTimeField).asInstanceOf[IntervalTensor]
  }

  override def withEndTime(endTimeField: String): IntervalTensor = {
    this.endTimeField = endTimeField
    this
  }

  override protected def setFieldTypes(fieldTypes: Map[String, (String, String)]): Unit = {
    super.setFieldTypes(fieldTypes.filterNot(_._1.equals(endTimeField)))
  }

  override protected def setFieldValues(fieldValues: Map[String, Any]): Unit = {
    endTime = fieldValues.find(_._1.equals(endTimeField)).map(_._2.asInstanceOf[Timestamp]).orNull
    super.setFieldValues(fieldValues.filterNot(_._1.equals(endTimeField)))
  }

  override def flatten(): Seq[ITensorTuple] = {
    super.flatten().map(tt => {
      IntervalTensorTuple(
        key = tt.key,
        tensorType = tt.tensorType,
        field = tt.field,
        value = tt.value,
        containerType = tt.containerType,
        elementType = tt.elementType,
        startTime = startTime,
        endTime = endTime
      )
    })
  }

}
