package com.tsi.bdsp.data

import org.apache.spark.sql.Row
import org.apache.spark.sql.functions.col
import org.json4s.JsonAST.{JObject, JValue}

object TensorColumns {
  val colKey = col("key")
  val colTensorType = col("tensorType")
  val colStartTime = col("startTime")
  val colEndTime = col("endTime")
  val colField = col("field")
  val colValue = col("value")
  val colContainerType = col("containerType")
  val colElementType = col("elementType")
}

case class TensorTuple(
  var key: String = null,
  var tensorType: String = null,
  var field: String = null,
  var value: String = null,
  var containerType: String = null,
  var elementType: String = null
) extends ITensorTuple

class Tensor(classifier: String, level: Byte, relation: Byte) extends TensorAccess with Typing {

  private var theData = Array[Any]()
  private var theShape: Shape = Shape()

  @transient
  lazy val meta: Meta = Meta(classifier = classifier, level = level, relation = relation)

  def this(classifier: String, level: Byte = 0, relation: Byte = Relations.PARENT, shape: Option[Shape]) {
    this(classifier, level, relation)
    theShape = shape.getOrElse(theShape)
    theData = new Array[Any](theShape.x)
    if(theShape.x > 0 && theShape.y > 0) {
      theData(0) = new Array(theShape.y)
      if(theShape.z > 0) {
        theData(0).asInstanceOf[Array[Any]](0) = new Array(theShape.z)
      }
    }
  }

  def this(inputData: Iterable[Any], classifier: String, level: Byte, relation: Byte, shape: Shape) {
    this(classifier, level, relation, Option(shape))
    append(inputData)
    meta.fieldTypes = Array(resolveType(inputData))
  }

  def this(inputData: Iterable[Any], classifier: String, level: Byte, relation: Byte) {
    this(inputData, classifier, level, relation, null)
  }

  def this(inputData: Iterable[Any], classifier: String) {
    this(inputData, classifier, 0.toByte, Relations.PARENT)
  }

  def this(inputData: Iterable[Any]) {
    this(inputData, "valueType", 0.toByte, Relations.PARENT)
  }

  def this(shape: Option[Shape]) {
    this("valueType", 0.toByte, Relations.PARENT, shape)
  }

  def this(classifier: String) {
    this(classifier, 0, Relations.PARENT)
  }

  def data: Array[Any] = theData
  def shape: Shape = theShape

  def append(data: Iterable[Any]): Unit = {
    theData = theData ++ data.toSeq
    theShape = resolveShape(data, theShape)
  }

  def append(data: Any): Unit = append(Seq(data))

  def prepend(data: Iterable[Any]): Unit = {
    theData = data.toArray ++ theData
    theShape = resolveShape(data, theShape)
  }

  def prepend(data: Any): Unit = prepend(Seq(data))

  protected def setFieldTypes(fieldTypes: Map[String, (String, String)]): Unit = {
    meta.fieldTypes = meta.fieldTypes ++ fieldTypes.values.filterNot(_._1.equals("key"))
  }

  protected def setFieldValues(fieldValues: Map[String, Any]): Unit = {
    meta.key = fieldValues.find(_._1.equals("key")).map(_._2.asInstanceOf[String]).orNull
    val filtered: Map[String, Any] = fieldValues.filterNot(_._1.equals("key"))
    meta.fields = meta.fields ++ filtered.keys
    append(filtered.values)
  }

  def removeIndex(idx: Int): Tensor = {
    theData = theData.patch(idx, Nil, 1)
    theShape.x -= 1
    meta.fieldTypes = meta.fieldTypes.patch(idx, Nil, 1)
    meta.fields = meta.fields.patch(idx, Nil, 1)
    this
  }

  protected def setTensorType(tensorTypeField: String): Unit = {
    meta.classifier = meta.fields.zipWithIndex
      .find(fIdx => fIdx._1.equals(tensorTypeField))
      .map(_._2)
      .map(idx => {
        val tensorTypeValue = theData(idx).asInstanceOf[String]
        removeIndex(idx)
        tensorTypeValue
      })
      .orNull

  }

  override def fromJson(jobj: JObject): Tensor = {
    val fieldTypes: Map[String, (String, String)] = jobj.foldField(Map[String, (String, String)]())((map, tuple2) => {
      val (fieldName: String, jval: JValue) = tuple2
      map ++ Map(fieldName -> resolveJsonType(jval))
    })
    setFieldTypes(fieldTypes)

    val fieldValues: Map[String, Any] = jobj.values
    setFieldValues(fieldValues)

    meta.level = 0.toByte
    meta.relation = Relations.PARENT
    setTensorType(classifier)

    this
  }

  override def fromRow(row: Row): Tensor = {
    val fieldTypes: Map[String, (String, String)] = row
      .schema
      .map(sfield => (sfield.name, resolveDataType(sfield.dataType)))
      .toMap

    setFieldTypes(fieldTypes)

    val fieldValues: Map[String, Any] = row.schema
      .zipWithIndex
      .map(fieldIdx => (fieldIdx._1.name, row.get(fieldIdx._2)))
      .toMap

    setFieldValues(fieldValues)
    setTensorType(classifier)

    this
  }

  override def flatten(): Seq[ITensorTuple] = {
    theData.zipWithIndex.map(indexed => {
      val (value: Any, idx: Int) = indexed
      val strValue = resolveIterable(Seq(value))
      val (containerType, elementType) = meta.fieldTypes(idx)
      TensorTuple(
        key = meta.key,
        tensorType = meta.classifier,
        field = meta.fields(idx),
        value = strValue,
        containerType = containerType,
        elementType = elementType
      )
    })
  }

  def flatMapped[F <: ITensorTuple](): Seq[F] = flatten().map(_.asInstanceOf[F])


  override def productElement(n: Int): Any = data(n)
  override def productArity: Int = shape.x
  override def canEqual(that: Any): Boolean = that.isInstanceOf[Tensor]
}
