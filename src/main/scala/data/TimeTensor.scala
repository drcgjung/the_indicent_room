package com.tsi.bdsp.data

import java.sql.Timestamp

case class TimeTensorTuple(
  var key: String = null,
  var tensorType: String = null,
  var field: String = null,
  var value: String = null,
  var containerType: String = null,
  var elementType: String = null,
  var startTime: Timestamp = null
) extends ITensorTuple

class TimeTensor(classifier: String, level: Byte = 0, relation: Byte = Relations.PARENT)
  extends Tensor(classifier, level, relation) with TimeTensorAcess {

  protected var startTimeField: String = _
  protected var startTime: Timestamp = _

  override def withStartTime(startTimeField: String): TimeTensor = {
    this.startTimeField = startTimeField
    this
  }

  override protected def setFieldTypes(fieldTypes: Map[String, (String, String)]): Unit = {
    super.setFieldTypes(fieldTypes.filterNot(_._1.equals(startTimeField)))
  }

  override protected def setFieldValues(fieldValues: Map[String, Any]): Unit = {
    startTime = fieldValues.find(_._1.equals(startTimeField)).map(_._2.asInstanceOf[Timestamp]).orNull
    super.setFieldValues(fieldValues.filterNot(_._1.equals(startTimeField)))
  }

  override def flatten(): Seq[ITensorTuple] = {
    super.flatten().map(t => {
      TimeTensorTuple(
        key = t.key,
        tensorType = t.tensorType,
        field = t.field,
        value = t.value,
        containerType = t.containerType,
        elementType = t.elementType,
        startTime = startTime
      )
    })
  }

}
