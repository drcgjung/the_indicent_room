package com.tsi.bdsp.data

case class Shape(var x: Int = 0, var y: Int = 0, var z: Int = 0)
