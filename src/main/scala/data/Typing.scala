package com.tsi.bdsp.data

import org.apache.spark.sql.types.{ArrayType, BinaryType, BooleanType, ByteType, DataType, DoubleType, IntegerType, LongType, MapType, ShortType, StringType, TimestampType}
import org.json4s.JsonAST.{JArray, JValue, JBool => JSONBool, JDecimal => JSONDecimal, JDouble => JSONDouble, JInt => JSONInt, JLong => JSONLong, JString => JSONString}

import java.lang.{Boolean => JBool, Byte => JByte, Double => JDouble, Float => JFloat, Integer => JInt, Long => JLong, Short => JShort}
import java.sql.Timestamp
import java.time.format.DateTimeFormatter
import java.util.Base64
import scala.reflect.{ClassTag, classTag}
import scala.reflect.runtime.universe._

trait Typing {

  def getTypeName[CT: TypeTag, ET: TypeTag]: (String, String) = {
    val containerType = typeTag[CT].tpe.typeSymbol.fullName
    val elementType = typeTag[ET].tpe.typeSymbol.fullName
    (containerType, elementType)
  }

  def getTypeName[ET: TypeTag]: String = {
    getTypeName[ET, ET]._2
  }

  def resolveType(input: Any): (String, String) = {
    input match {
      case _: String => getTypeName[String, String]
      case _: Int => getTypeName[Int, Int]
      case _: JInt => getTypeName[JInt, JInt]
      case _: Long => getTypeName[Long, Long]
      case _: JLong => getTypeName[JLong, JLong]
      case _: Double => getTypeName[Double, Double]
      case _: JDouble => getTypeName[JDouble, JDouble]
      case _: Float => getTypeName[Float, Float]
      case _: JFloat => getTypeName[JFloat, JFloat]
      case _: Byte => getTypeName[Byte, Byte]
      case _: JByte => getTypeName[JByte, JByte]
      case _: Boolean => getTypeName[Boolean, Boolean]
      case _: JBool => getTypeName[JBool, JBool]
      case _: Short => getTypeName[Short, Short]
      case _: JShort => getTypeName[JShort, JShort]
      case _: Timestamp => getTypeName[Timestamp, Timestamp]
      case _ => {
        input.isInstanceOf[Iterable[Any]] match {
          case true => {
            val elem = input.asInstanceOf[Iterable[Any]].head
            (classTag[Iterable[Any]].toString(), resolveType(elem)._2)
          }
          case _ => throw new IllegalArgumentException(s"Unsupported type ${input.getClass} to get resolved!")
        }
      }
    }
  }

  def resolveDataType(dataType: DataType): (String, String) = {
    dataType match {
      case StringType => getTypeName[String, String]
      case IntegerType => getTypeName[JInt, JInt]
      case LongType => getTypeName[JLong, JLong]
      case DoubleType => getTypeName[JDouble, JDouble]
      case BooleanType => getTypeName[JBool, JBool]
      case ByteType => getTypeName[JByte, JByte]
      case ShortType => getTypeName[JShort, JShort]
      case TimestampType => getTypeName[Timestamp, Timestamp]
      case ArrayType(StringType, _) => getTypeName[Array[String], String]
      case ArrayType(ByteType, _) | BinaryType =>  getTypeName[Array[JByte], JByte]
      case MapType(StringType, StringType, _) => getTypeName[Map[String, String], String]
      case _ => {
        dataType match {
          case ArrayType(_, _) => {
            val at = dataType.asInstanceOf[ArrayType]
            throw new IllegalArgumentException(s"Couldn't resolve Array type with unknown element type ${at.elementType.typeName}!")
          }
          case MapType(_, _, _) => {
            val mt = dataType.asInstanceOf[MapType]
            throw new IllegalArgumentException(s"Couldn't resolve Map type with unknonw key and value type ${mt.keyType.typeName} -> ${mt.valueType.typeName}!")
          }
          case _ => throw new IllegalArgumentException(s"Couldn't resolve unknown data type ${dataType.typeName}!")
        }
      }
    }
  }

  def resolveJsonType(jval: JValue): (String, String) = {
    jval match {
      case JSONString(s) => resolveType(s)
      case JSONInt(num) => resolveType(num)
      case JSONDouble(num) => resolveType(num)
      case JSONLong(num) => resolveType(num)
      case JSONDecimal(num) => resolveType(num)
      case JSONBool(bool) => resolveType(bool)
      case JArray(arr) => resolveType(arr)
      case _ => throw new IllegalArgumentException(s"JVavlue $jval is not a known JSON type!")
    }
  }

  def resolveIterable(values: Iterable[Any]): String = {
    values.map {
      case item: Product => resolveIterable(item.productIterator.toIterable)
      case item: Iterable[_] => resolveIterable(item)
      case item => resolveValue(item)
    }.mkString("[", ",", "]")
  }

  def resolveValue(inputValue: Any): String = {
    inputValue match {
      case s: String => s
      case num @ (
        _: Int | _: JInt |
        _: Long | _: JLong |
        _: Double | _: JDouble |
        _: Float | _: JFloat |
        _: Byte | _: JByte |
        _: Boolean | _: JBool |
        _: Short | _: JShort
      ) => String.valueOf(num)
      case ts: Timestamp => ts.toLocalDateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)
      case bi: BigInt => resolveValue(bi.byteValue())
      case bytes: Array[Byte] => Base64.getEncoder.encodeToString(bytes)
      case iter: Iterable[Any] =>{
        iter match {
          case map: Map[Any, Any] =>
            resolveValue(map.head._2)
          case _ =>
            resolveValue(iter.head)
        }
      }
      case _ => throw new IllegalArgumentException(s"Unknown item type of ${inputValue.getClass.getName}!")
    }
  }

  def resolveShape[T](data: T, shape: Shape = Shape())(implicit ct: ClassTag[T], tt: TypeTag[T]): Shape = {

    def recIncDim(input: Any, shape: Shape, dim: Int): Shape = {

      val setDim = (size: Int) => dim match {
        case 0 => shape.x += size
        case 1 => shape.y += size
        case 2 => shape.z += size
      }

      input.isInstanceOf[Iterable[Any]] match {
        case true => {
          val dl = input.asInstanceOf[Iterable[Any]]

          setDim(dl.size)
          recIncDim(dl.head, shape, dim + 1)
        }
        case _ => {
          setDim(1)
          shape
        }
      }
    }

    try {
      recIncDim(data, shape, 0)
    } catch {
      case t: Throwable => throw new IllegalArgumentException(s"Data can't be shaped!", t)
    }
  }

  /**
    * Decodes base64 encoded string as raw bytes
    * @param value base64 encoded string
    * @return byte array
    */
  protected def getBytes(value: String): Array[Byte] = Base64.getDecoder.decode(value)
}
