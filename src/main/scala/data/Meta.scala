package com.tsi.bdsp.data

import java.util.UUID

object Relations {
  val PARENT = 0.toByte
  val CHILD = 1.toByte
  val SIBLING = 2.toByte
}

case class Meta(
  // meta data header attributes
  var classifier: String = null,
  var level: Byte = Byte.MinValue,
  var relation: Byte = Byte.MinValue,
  var key: String = null,
  var parentKey: String = null,
  // field attributes
  var fields: Array[String] = Array(),
  var fieldTypes: Array[(String, String)] = Array()
) extends Serializable
