package com.tsi.bdsp.data

import java.sql.Timestamp

trait IntervalTensorAcess extends TimeTensorAcess {

  def withEndTime(endTimeField: String): IntervalTensor
}
