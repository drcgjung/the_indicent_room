package com.tsi.bdsp.data

import java.sql.Timestamp

trait TimeTensorAcess extends TensorAccess {

  def withStartTime(startTimeField: String): TimeTensor
}
