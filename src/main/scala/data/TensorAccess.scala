package com.tsi.bdsp.data

import org.apache.spark.sql.Row
import org.json4s.JsonAST.JObject

trait ITensorTuple {
  def key: String
  def tensorType: String
  def field: String
  def value: String
  def containerType: String
  def elementType: String
}

trait TensorAccess extends Product with Serializable {
  def fromJson(jobj: JObject): Tensor
  def fromRow(row: Row): Tensor
  def flatten(): Seq[ITensorTuple]
}
