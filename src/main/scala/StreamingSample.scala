package com.tsi.bdsp

import com.tsi.bdsp.api.backend.SparkBackendServer
import com.tsi.bdsp.application.{AppSettings, BaseApp}
import com.tsi.bdsp.data.{IntervalTensor, IntervalTensorTuple, Tensor, TensorTuple}
import com.tsi.bdsp.messages._
import com.tsi.bdsp.streaming._
import org.apache.log4j.Logger
import org.apache.spark.rdd.RDD
import org.apache.spark.sql._
import org.apache.spark.sql.expressions.{Window, WindowSpec}
import org.apache.spark.sql.functions.{col, lead}
import org.apache.spark.streaming._
import org.apache.spark.streaming.dstream.{DStream, ReceiverInputDStream}
import org.json4s._
import org.json4s.native.JsonMethods

import java.io._
import java.nio.charset._


object StreamingSample extends BaseApp {
  @transient lazy val logger = Logger.getLogger(this.getClass)
  val spark: SparkSession = SparkSession.builder().config(conf).getOrCreate()
  val ssc = new StreamingContext(spark.sparkContext, Seconds(10))

  /*
  val sampleFrontend = new Deployment() {
    def deploy(deployer:Deployer) {
      deployer.mount(deployer.createDefaultServlet,"","default",false,Seq("resourceBase"->getClass.getClassLoader.getResource("web").toString()))
    }
  }

  new com.tsi.bdsp.api.jetty.ReflectionBasedEmbeddedJettyDeployer(sc=ssc.sparkContext,filters=None,deployment=sampleFrontend)

  */

  val sparkBackendServer = new SparkBackendServer(spark, args)

  val tensors: ReceiverInputDStream[JValue] = ssc.receiverStream(new ServerSocketReceiver[JValue](9999) {
    override def createReader(input:InputStream): () => Iterable[JValue] = {
      val reader = new BufferedReader(new InputStreamReader(input, StandardCharsets.UTF_8)) 
      () => { 
        log.trace("Trying to read next line")
        val line: String = reader.readLine
        // log.debug(s"Got next line ${line} which will be parsed")
        try {
          if(line == null || line.isEmpty) {
            log.warn(s"Got empty message")
            null
          } else {
            val jVal = JsonMethods.parse(line)
            Some(jVal)
          }
        } catch {
          case e:Exception => {
            log.warn(s"Run into ${e} when trying to parse.")
            None
          }
        }
      }
    }

    override def createHandler(reader:Function0[Iterable[JValue]]): JValue => Iterable[JValue] = {
      // val jsonMessages: Iterable[JValue] = reader()
      // log.info(s"Handler found meta header ${headers}.")
      jsonMessage: JValue => {
        implicit val formats = DefaultFormats
        val valueType = (jsonMessage \ "valueType").extract[String]
        log.trace(s"Got JSON message from type ${valueType}")
        // val merged=headers.foldLeft(input)((input,header) => input merge header)
        // log.debug(s"Handler found next message ${merged}.")
        // Some(merged)
        Some(jsonMessage)
      }
    }
  })

  /*
  val pairs: DStream[(JValue, Int)] = tensors.map((tensor: JValue) => {
      (tensor \ "value"->1 )
  })


  val tensorCounts: DStream[(JValue, Int)] = pairs.reduceByKey(_ + _)

  */

  val messageStream: DStream[(String, Message)] = tensors.map(new Json2MessageMapper())

  val generalMessageStream: DStream[(String, Message)] = messageStream.filter(_._1.equals(MsgType.GENERAL))
  val oxtsDataMessageStream: DStream[(String, Message)] = messageStream.filter(_._1.equals(MsgType.OXTSDATA))
  val pointCloudMessageStream: DStream[(String, Message)] = messageStream.filter(_._1.equals(MsgType.POINTCLOUD))
  val videoMessageStream: DStream[(String, Message)] = messageStream.filter(_._1.equals(MsgType.VIDEO))

  saveStream[GeneralMessage](
    generalMessageStream,
    None,
    (spark: SparkSession, rdd: RDD[GeneralMessage]) => {
      import spark.implicits._
      spark.createDataset(rdd)
    },
    toTensorData
  )

  saveStream[OxtsDataMessage](
    oxtsDataMessageStream,
    Some(Window.partitionBy("valueType").orderBy("startTime")),
    (spark: SparkSession, rdd: RDD[OxtsDataMessage]) => {
      import spark.implicits._
      spark.createDataset(rdd)
    },
    toIntervalTensorData
  )

  saveStream[PointCloudMessage](
    pointCloudMessageStream,
    Some(Window.partitionBy("valueType").orderBy("startTime")),
    (spark: SparkSession, rdd: RDD[PointCloudMessage]) => {
      import spark.implicits._
      spark.createDataset(rdd)
    },
    toIntervalTensorData
  )

  saveStream[VideoMessage](
    videoMessageStream,
    Some(Window.partitionBy("valueType", "index").orderBy("startTime")),
    (spark: SparkSession, rdd: RDD[VideoMessage]) => {
      import spark.implicits._
      spark.createDataset(rdd)
    },
    toIntervalTensorData
  )

  ssc.start()             // Start the computation
  ssc.awaitTermination()  // Wait for the computation to

  private def toIntervalTensorData(df: Dataset[Row]): Dataset[Row] = {
    import df.sparkSession.implicits._
    val ds: Dataset[IntervalTensorTuple] = df.flatMap(row =>
      new IntervalTensor("valueType")
        .withStartTime("startTime")
        .withEndTime("endTime")
        .fromRow(row)
        .flatMapped()
    )
    ds.toDF()
  }

  private def toTensorData(df: Dataset[Row]): Dataset[Row] = {
    import df.sparkSession.implicits._
    val ds: Dataset[TensorTuple] = df.flatMap(row =>
      new Tensor("valueType")
        .fromRow(row)
        .flatMapped()
    )
    ds.toDF()
  }

  private def createInterval[T](dataset: Dataset[T], windowSpec: WindowSpec): Dataset[Row] = {
    val leadEndTime: Column = lead(col("startTime"), 1).over(windowSpec)
    dataset.toDF().withColumn("endTime", leadEndTime)
  }

  private def writeOrc(dataset: Dataset[Row], orcResource: String): Unit = {
    // dataset.printSchema()
    dataset.write
      .mode(SaveMode.Append)
      .partitionBy("tensorType")
      .orc(orcResource)
  }

  private def saveStream[M <: Message](
    stream: DStream[(String, Message)], windowSpec: Option[WindowSpec],
    getData: (SparkSession, RDD[M]) => Dataset[M],
    toTensorData: (Dataset[Row] => Dataset[Row])): Unit = {

    stream.foreachRDD { rdd =>
      try {
        if (!rdd.isEmpty()) {
          val sparkConf = rdd.sparkContext.getConf
          val orcResources: String = sparkConf.get(AppSettings.ORC_RESOURCES)
          val spark = SparkSession.builder().config(sparkConf).getOrCreate()
          val ds: Dataset[M] = getData(spark, rdd.map(_._2).asInstanceOf[RDD[M]])
          var df: Dataset[Row] = if(windowSpec.isEmpty) {
            ds.toDF()
          } else {
            createInterval(ds, windowSpec.get)
          }
          df = toTensorData(df)

          writeOrc(df, orcResources)
        }
      } catch {
        case t: Throwable =>
          logger.error("An error occurred while writing ORC files!", t)
          throw t
      }
    }
  }
}
