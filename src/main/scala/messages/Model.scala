package com.tsi.bdsp.messages

import com.tsi.bdsp.api.support.ArrayDataParser
import com.tsi.bdsp.data.SparkUtils.ExtendedColumn
import com.tsi.bdsp.data.{TensorColumns, Typing}
import org.apache.spark.sql.Row
import org.json4s.DefaultFormats
import org.json4s.JsonAST.JObject

import java.sql.Timestamp
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object MsgType {
  val GENERAL = "GENERAL"
  val VIDEO = "VIDEO"
  val POINTCLOUD = "POINTCLOUD"
  val OXTSDATA = "SCALAR"
  val CHANNEL = "CHANNEL"
}

object ImgSide {
  val LEFT = "LEFT"
  val RIGHT = "RIGHT"
}

trait Message extends Typing with Serializable {
  var valueType: String

  @transient
  lazy val timeformatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME

  /**
    * Create message instance from parsing JSON type
    * @param jobj raw json type from json4s
    * @return message instance
    */
  def fromJson(jobj: JObject): Message

  /**
    * Converts literal timestamp string from JSON
    * @param value string value local date time format
    * @return local date time as [[Timestamp]]
    */
  protected def getTimestamp(value: String): Timestamp = {
    val localDateTime = LocalDateTime.from(timeformatter.parse(value))
    Timestamp.valueOf(localDateTime)
  }
}

trait BaseMessage extends Message {
  var key: String
  var index: Int
  var startTime: Timestamp
  var endTime: Timestamp
}

case class GeneralMessage(
  var key: String = null,
  var dataDate: String = null,
  var driveId: String = null,
  var camToCamCalibTime: Timestamp = null,
  var imuToVeloCalibTime: Timestamp = null,
  var veloToCamCalibTime: Timestamp = null,
  var calibData: Map[String, String] = Map(), // pyton dict with dynamic key values
  var valueType: String = MsgType.GENERAL
) extends Message {

  /**
    * Create message instance from parsing JSON type
    *
    * @param jobj raw json type from json4s
    * @return message instance
    */
  override def fromJson(jobj: JObject): GeneralMessage = {
    implicit val formats = DefaultFormats
    this.key = (jobj \ "key").extract[String]
    this.dataDate = (jobj \ "dataDate").extract[String]
    this.driveId = (jobj \ "driveId").extract[String]
    this.camToCamCalibTime = getTimestamp((jobj \ "camToCamCalibTime").extract[String])
    this.imuToVeloCalibTime = getTimestamp((jobj \ "imuToVeloCalibTime").extract[String])
    this.veloToCamCalibTime = getTimestamp((jobj \ "veloToCamCalibTime").extract[String])
    this.calibData = mapCalibData((jobj \ "calibData").extract[Map[String, Any]])
    this
  }

  private def mapCalibData(rawData: Map[String, Any]): Map[String, String] = {
    rawData.map { entry =>
      val (key: String, data: Any) = entry
      val listData: Option[String] = data match {
        case lld: List[List[Double]] => Option(resolveIterable(lld))
        case d: Double => Option(resolveValue(d))
        case _ => None
      }
      key -> listData.orNull
    }
  }
}

case class OxtsDataMessage(
  var key: String = null,
  var index: Int = 0,
  var startTime: Timestamp = null,
  var endTime: Timestamp = null,
  var oxts: Map[String, String] = Map(),
  var valueType: String = MsgType.OXTSDATA
) extends BaseMessage {

  /**
    * Create message instance from parsing JSON type
    *
    * @param jobj raw json type from json4s
    * @return message instance
    */
  override def fromJson(jobj: JObject): OxtsDataMessage = {
    implicit val formats = DefaultFormats
    this.key = (jobj \ "key").extract[String]
    this.index = (jobj \ "index").extract[Int]
    this.startTime = getTimestamp((jobj \ "timestamp").extract[String])
    this.valueType = (jobj \ "valueType").extract[String]
    this.oxts = mapOxtData((jobj \ "oxt").extract[Map[String, Any]])
    this
  }

  private def mapOxtData(rawData: Map[String, Any]): Map[String, String] = {
    rawData.flatMap { entry =>
      val (key: String, data: Any) = entry
      data match {
        case dmap: Map[String, Any] => mapOxtData(dmap.map(e => (s"${key}.${e._1}", e._2)))
        case dlist: Iterable[Any] => mapOxtData(dlist.zipWithIndex.map(eIdx => (s"${key}.[${eIdx._2}]", eIdx._1)).toMap)
        case d => Map(key -> resolveValue(d))
      }
    }
  }
}

/** Class to represent General message with assigned mapping methods */
case class PointCloudMessage(
  var key: String = null,
  var index: Int = 0,
  var startTime: Timestamp = null,
  var endTime: Timestamp = null,
  var valueType: String = MsgType.POINTCLOUD,
  var pointCloudData: Array[Byte] = Array() // zlib compressed velo data points as base64
) extends BaseMessage {

  def fromJson(jobj: JObject): PointCloudMessage = {
    implicit val formats = DefaultFormats
    this.key = (jobj \ "key").extract[String]
    this.index = (jobj \ "index").extract[Int]
    this.startTime = getTimestamp((jobj \ "timestamp").extract[String])
    this.valueType = (jobj \ "valueType").extract[String]
    this.pointCloudData = getBytes((jobj \ "pointCloudData").extract[String])
    this
  }
}

/** Class to represent Video message with assigned mapping methods */
case class VideoMessage(
  var key: String = null,
  var index: Int = 0,
  var startTime: Timestamp = null,
  var endTime: Timestamp = null,
  var valueType: String = MsgType.VIDEO,
  var imgData: Array[Byte] = null, // encode img. bytes as base64 string
  var imgType: String = null,
  var coloured: Boolean = false,
  var side: String = null
) extends BaseMessage {

  def fromJson(jobj: JObject): VideoMessage = {
    implicit val formats = DefaultFormats
    this.key = (jobj \ "key").extract[String]
    this.index = (jobj \ "index").extract[Int]
    this.startTime = getTimestamp((jobj \ "timestamp").extract[String])
    this.valueType = (jobj \ "valueType").extract[String]
    this.imgData = getBytes((jobj \ "imgData").extract[String])
    this.imgType = (jobj \ "imgType").extract[String]
    this.coloured = (jobj \ "coloured").extract[Boolean]
    this.side = (jobj \ "side").extract[String]
    this
  }
}

