package com.tsi.bdsp.messages

import org.json4s.DefaultFormats
import org.json4s.JsonAST.{JArray, JObject, JValue}

/**
  * Class represents as JSON message to [[Message]] mapping function
  */
class Json2MessageMapper extends Function1[JValue, (String, Message)] with Serializable {

  override def apply(jval: JValue): (String, Message) = {
    jval match {
      case obj: JObject => parseObject(obj)
      case arr: JArray => parseArray(arr)
      case _ => (null, null)
    }
  }

  private def parseObject(jObj: JObject): (String, Message) = {
    implicit val formats = DefaultFormats
    val valueType: String =(jObj \ "valueType").extract[String]
    // each mapping logic is assigned to each message type itself
    valueType match {
      case MsgType.GENERAL => (MsgType.GENERAL, GeneralMessage().fromJson(jObj))
      case MsgType.OXTSDATA => (MsgType.OXTSDATA, OxtsDataMessage().fromJson(jObj))
      case MsgType.POINTCLOUD => (MsgType.POINTCLOUD, PointCloudMessage().fromJson(jObj))
      case MsgType.VIDEO => (MsgType.VIDEO, VideoMessage().fromJson(jObj))
      case _ => (null, null)
    }
  }

  private def parseArray(jarr: JArray): (String, Message) = {
    // JSON messages with pure arrays are not expected currently
    (null, null)
  }
}
