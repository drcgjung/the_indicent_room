package com.tsi.bdsp.application

import org.apache.spark.SparkConf

import java.io.File

/**
  * Base class for spark applications
  */
abstract class BaseApp extends App {
  
  var master:Option[String]=Some("local[*]")
  var appName=getClass.getName()

  val conf=new SparkConf()

  if(master.isDefined) {
      conf.setMaster(master.get)
  }

  val rootPath: String = new File(System.getProperty("user.dir")).getAbsolutePath

  conf.setAppName(appName)
  conf.set(AppSettings.ORC_RESOURCES, s"${rootPath}/resources/orc_demo")

}
