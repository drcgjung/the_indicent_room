import dataclasses
import datetime as dt
import io
import json
import os
import socket as soc
import zlib
from typing import Generator, Any, Dict, Tuple

from PIL import Image
from numpy.core.multiarray import ndarray

from . import LOG, CALIB_IMU_TO_VELO, CALIB_CAM_TO_CAM, CALIB_VELO_TO_CAM
from .model import KittiData, GeneralMessage, VideoMessage, KittiMessageJsonEncoder, ImgSide, PointCloudMessage, \
    OxtsDataMessage


class Messenger(object):

    def __init__(self, data: KittiData):
        self.__kitti_data: KittiData = data
        self.__msg_cnt = 0

    @property
    def message_count(self):
        return self.__msg_cnt

    def __img_to_bytes(self, img: Image, img_type: str) -> bytes:
        byte_array = io.BytesIO()
        img.save(byte_array, format=img_type.upper())
        byte_array = byte_array.getvalue()
        return byte_array

    def __compress_velo(self, velo: ndarray) -> bytes:
        velo_bytes = velo.tobytes()
        velo_compressed = zlib.compress(velo_bytes)
        return velo_compressed

    def __is_named_tuple_instance(self, any) -> bool:
        return (
            isinstance(any, tuple) and
            hasattr(any, '_asdict') and
            hasattr(any, '_fields')
        )

    def __named_tuple_to_dict(self, any):
        if not self.__is_named_tuple_instance(any):
            return any

        a_dict = any._asdict()
        for key, value in a_dict.items():
            a_dict[key] = self.__named_tuple_to_dict(value)

        return a_dict

    def yield_general_message(self) -> Generator[GeneralMessage, Any, None]:
        """
        Capsules kitti raw data into message pieces
        :return: Generator of GeneralMessage
        """
        yield self.get_general_message()

    def get_general_message(self) -> GeneralMessage:
        cam2cam_f = CALIB_CAM_TO_CAM.split('.')[0]
        imu2velo_f = CALIB_IMU_TO_VELO.split('.')[0]
        velo2cam_f = CALIB_VELO_TO_CAM.split('.')[0]
        calib_data = self.__kitti_data.calibData
        cam2cam_ct = calib_data[cam2cam_f][0]
        del calib_data[cam2cam_f]
        imu2velo_ct = calib_data[imu2velo_f][0]
        del calib_data[imu2velo_f]
        velo2cam_ct = calib_data[velo2cam_f][0]
        del calib_data[velo2cam_f]

        return GeneralMessage(
            dataDate=self.__kitti_data.date,
            driveId=self.__kitti_data.driveId,
            key=self.__kitti_data.key,
            camToCamCalibTime=cam2cam_ct,
            imuToVeloCalibTime=imu2velo_ct,
            veloToCamCalibTime=velo2cam_ct,
            calibData=calib_data,
        )

    def yield_pointcloud_message(self) -> Generator[PointCloudMessage, Any, None]:
        """
        Capsules velodyne kitti raw data as point cloud message
        :return: Generator of PointCloudMessage
        """
        for idx, timestamp in enumerate(self.__kitti_data.timestamps):
            yield self.get_pointcloud_message(idx, timestamp)

    def get_pointcloud_message(self, idx: int, timestamp: dt.datetime = None) -> PointCloudMessage:
        if timestamp is None:
            timestamp = self.__kitti_data.timestamps[idx]

        velo = self.__kitti_data.idxVelo(idx)
        return PointCloudMessage(
            key=self.__kitti_data.key,
            index=idx,
            timestamp=timestamp,
            pointCloudData=self.__compress_velo(velo)
        )

    def yield_oxtsdata_message(self) -> Generator[OxtsDataMessage, Any, None]:
        """
        Capsules oxts data (lat, lon, alt, velocity, acceleration, angular rate, navigation, orienation etc.)
        as own message type OxtsDataMessage
        :return: Generator of OxtsDataMessage
        """
        for idx, timestamp in enumerate(self.__kitti_data.timestamps):
            yield self.get_oxtsdata_message(idx, timestamp)

    def get_oxtsdata_message(self, idx: int, timestamp: dt.datetime = None) -> OxtsDataMessage:
        if timestamp is None:
            timestamp = self.__kitti_data.timestamps[idx]

        oxt = self.__kitti_data.oxts[idx]
        return OxtsDataMessage(
            key=self.__kitti_data.key,
            index=idx,
            timestamp=timestamp,
            oxt=self.__named_tuple_to_dict(oxt)
        )

    def __create_video_message(self, tuple: Tuple) -> VideoMessage:
        """
        Create from tuple video message
        :param tuple: contains index, timestamp, image type, image side and if its coloured or not
        :return: VideoMessage
        """
        idx, timestamp, img_type, side, coloured = tuple
        video_msg = VideoMessage(key=self.__kitti_data.key, index=idx, timestamp=timestamp,
                                 imgType=img_type, side=side, coloured=coloured)
        if side == ImgSide.LEFT and not coloured:
            video_msg.imgData = self.__img_to_bytes(self.__kitti_data.idxCam0(idx), img_type)
        elif side == ImgSide.RIGHT and not coloured:
            video_msg.imgData = self.__img_to_bytes(self.__kitti_data.idxCam1(idx), img_type)
        elif side == ImgSide.LEFT and coloured:
            video_msg.imgData = self.__img_to_bytes(self.__kitti_data.idxCam2(idx), img_type)
        elif side == ImgSide.RIGHT and coloured:
            video_msg.imgData = self.__img_to_bytes(self.__kitti_data.idxCam3(idx), img_type)

        return video_msg

    def yield_create_video_message(self) -> Generator[Tuple, Any, None]:
        """
        Generator of tuple for parsing video message for lazy evaluation
        :return: yielded tuple of index, timestamp image type image side and if coloured of not
        """
        img_type = self.__kitti_data.imageType
        for idx, timestamp in enumerate(self.__kitti_data.timestamps):
            for gen_tuple in [
                (idx, timestamp, img_type, ImgSide.LEFT, False),
                (idx, timestamp, img_type, ImgSide.RIGHT, False),
                (idx, timestamp, img_type, ImgSide.LEFT, True),
                (idx, timestamp, img_type, ImgSide.RIGHT, True)
            ]:
                yield gen_tuple

    def yield_video_message(self) -> Generator[VideoMessage, Any, None]:
        for gen_tuple in self.yield_create_video_message():
            yield self.__create_video_message(gen_tuple)

    def yield_dict_message(self, generator: Generator) -> Generator[Dict[str, Any], Any, None]:
        """
        Converts KittiMessage as python dictionary
        :return: Generator of python dictionary
        """
        for kitti_message in generator:
            dict_msg = dataclasses.asdict(kitti_message)
            yield dict_msg

    def yield_json_message(self, generator: Generator) -> Generator[str, Any, None]:
        """
        Serialize KittiMessage dataclass as json string
        :return: Generator for json message as string
        """
        for kitti_message in generator:
            json_msg: str = json.dumps(kitti_message, cls=KittiMessageJsonEncoder)
            yield json_msg

    def send_message(self, sock: soc.socket, dict_msg: Dict[str, Any], **kwargs) -> bool:
        """
        Sends all parsed kitti messages as json messages over the socket
        :param sock: connected socket
        :param dict_msg: python dictionary message
        :param kwargs: add. dict key value which get merged
        :return: true if all messages where sent successfully false if something went wrong
        """
        result = False

        try:
            if sock._closed:
                return result

            if kwargs:
                dict_msg.update(dict(kwargs))

            json_msg: str = json.dumps(dict_msg, cls=KittiMessageJsonEncoder)
            json_msg = json_msg + os.linesep
            json_bytes = json_msg.encode()
            is_sucessful = sock.sendall(json_bytes)
            if is_sucessful is None:
                LOG.info(f'Sent kitti JSON message #{self.__msg_cnt} ({len(json_bytes)})')
                self.__msg_cnt += 1
                result = True
            else:
                LOG.error(f'Some error occurred while sending last message!')
        except soc.timeout:
            LOG.error(f'An socket timeout hits after {sock.gettimeout()}s while sending kitti message!')
        except soc.error as se:
            LOG.error(f'An socket error occurred!', se)
        except Exception as e:
            LOG.exception(f'An execption was thrown while sending kitti message!', e)

        return result

    def send_all_data_messages(self, sock: soc.socket, **kwargs) -> bool:
        """
        Send all according data as json messages
        :param sock: socket to send over
        :param kwargs: dict of add. parameters to merge in
        :return: true if send was successful
        """
        break_condition = None
        if kwargs and 'break_condition' in kwargs:
            break_condition = kwargs['break_condition']
            kwargs.pop('break_condition')

        def as_dict(kitti_msg):
            return dataclasses.asdict(kitti_msg)

        # create general message as first message
        gen_msg = self.get_general_message()
        self.send_message(sock, as_dict(gen_msg), **kwargs)

        for idx, timestamp in enumerate(self.__kitti_data.timestamps):
            # check if break cond. hits
            if break_condition and break_condition():
                break

            # 1 prepare and send oxts data kitti message
            idx_od_msg = self.get_oxtsdata_message(idx, timestamp)
            self.send_message(sock, as_dict(idx_od_msg), **kwargs)

            # 2 prepare and send point cloud kitti message
            idx_pc_msg = self.get_pointcloud_message(idx, timestamp)
            self.send_message(sock, as_dict(idx_pc_msg), **kwargs)

            # 3 prepare and send video kitti messages (for each frame one message)
            for gen_tuple in filter(lambda t5: t5[0] == idx, self.yield_create_video_message()):
                if break_condition and break_condition():
                    break
                video_message = self.__create_video_message(gen_tuple)
                self.send_message(sock, as_dict(video_message), **kwargs)

        return True
