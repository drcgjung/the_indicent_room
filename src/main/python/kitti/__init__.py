import logging
from typing import Any, Iterator, Iterable

"""
kitti module for handling kitti data
"""

# pattern raw string for kitti raw data directory
DATA_DIR_PATTERN: str = r'(?P<basedir>[a-zA-Z0-9:\\/_]+)(\\|/)(?P<date>(?P<year>[0-9]{4})_(?P<month>[0-9]{1,2})_(?P<day>[0-9]{1,2}))((\\|/)(?P<internaldir>[a-zA-Z0-9\\/_]+))?$'

# configure logging
logging.basicConfig(
    level=logging.INFO,
    format='[%(asctime)s] %(levelname)-8s %(name)-12s %(message)s'
)
LOG = logging.getLogger(__name__)
LOG.addHandler(logging.StreamHandler())

CALIB_IMU_TO_VELO = 'calib_imu_to_velo.txt'
CALIB_VELO_TO_CAM = 'calib_velo_to_cam.txt'
CALIB_CAM_TO_CAM = 'calib_cam_to_cam.txt'
CALIB_TIME_FIELD = 'calib_time'
CALIB_TIME_FORMAT = '%d-%b-%Y %H:%M:%S'



def flatmap(map_fun, items: Iterable[Iterable[Any]]) -> Iterable[Any]:
    """
    Simple flatmap function
    :param map_fun: map function as lambda
    :param items: list of lists to flatten
    :return: flattened items iterator
    """
    flatten = []
    for item in items:
        flatten.extend(map_fun(item))
    return flatten
