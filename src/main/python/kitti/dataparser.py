import datetime as dt
import os
import re
import uuid

import numpy as np
from os.path import basename
from typing import Pattern, Optional, Match

from pykitti import raw

from . import CALIB_CAM_TO_CAM, CALIB_IMU_TO_VELO, CALIB_VELO_TO_CAM, DATA_DIR_PATTERN, CALIB_TIME_FIELD, \
    CALIB_TIME_FORMAT
from .model import KittiData


class DataParser(object):

    def __init__(self, data_dir: str, drive_id: str):
        self.__data_dir: str = data_dir
        self.__drive_id: str = drive_id
        self.__base_dir = None
        self.__date = None
        self.__dataset = None
        self.__parse_data_dir()

    def __parse_data_dir(self):
        p: Pattern[str] = re.compile(DATA_DIR_PATTERN, re.IGNORECASE)
        m: Optional[Match[str]] = p.match(self.__data_dir)
        if m:
            self.__base_dir = m.group('basedir')
            self.__date = m.group('date')

    def __enrich_calib_data(self, calib_data: dict) -> dict:
        calib_path = os.path.join(self.__base_dir, self.__date)
        for calib_file in [CALIB_CAM_TO_CAM, CALIB_IMU_TO_VELO, CALIB_VELO_TO_CAM]:
            calib_file_path = os.path.join(calib_path, calib_file)
            calib_file_name = os.path.splitext(basename(calib_file_path))[0]
            with open(calib_file_path, 'r') as cf:
                for line in cf.readlines():
                    key, value = line.split(':', 1)
                    if key == CALIB_TIME_FIELD:
                        dt_value = dt.datetime.strptime(str(value).strip(), CALIB_TIME_FORMAT)
                        calib_data[calib_file_name] = np.array([dt_value])
                        break

        return calib_data

    def __generate_data_key(self) -> str:
        str_key = f'{self.__date}##{self.__drive_id}'
        return str(uuid.uuid3(uuid.NAMESPACE_DNS, str_key))



    def parse(self) -> KittiData:
        """
        Parse kitti raw data
        :return: generator and iterator as KittiData
        """
        if self.__base_dir and self.__date and self.__drive_id:
            self.__dataset = raw(base_path=self.__base_dir, date=self.__date, drive=self.__drive_id)
            calib_data = self.__dataset.calib._asdict()
            calib_data = self.__enrich_calib_data(calib_data)
            return KittiData(
                date=self.__date,
                driveId=self.__drive_id,
                key=self.__generate_data_key(),
                genCam0=self.__dataset.cam0,
                idxCam0=self.__dataset.get_cam0,
                genCam1=self.__dataset.cam1,
                idxCam1=self.__dataset.get_cam1,
                genCam2=self.__dataset.cam2,
                idxCam2=self.__dataset.get_cam2,
                genCam3=self.__dataset.cam3,
                idxCam3=self.__dataset.get_cam2,
                genVelo=self.__dataset.velo,
                idxVelo=self.__dataset.get_velo,
                iterGray=self.__dataset.gray,
                iterRGB=self.__dataset.rgb,
                timestamps=self.__dataset.timestamps,
                oxts=self.__dataset.oxts,
                calibData=calib_data,
                datasetType=self.__dataset.dataset,
                imageType=self.__dataset.imtype
            )
        else:
            KittiData()

