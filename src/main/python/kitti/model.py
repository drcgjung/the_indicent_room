import base64
import dataclasses
import datetime as dt
import json
from dataclasses import dataclass, field
from enum import Enum
from typing import Generator, Any, Iterator, Tuple, List, Dict, Callable

import numpy as np
from PIL.Image import Image
from numpy.core.multiarray import ndarray


@dataclass
class KittiData:
    """
    Data class for pykitti raw dataset
    """
    date: str = None  # data date from path string
    driveId: str = None  # drive id from path
    key: str = None # unique id for vehicle data
    genCam0: Generator[Any, Any, None] = None  # cam0 image generator
    idxCam0: Callable[[int], Image] = None  # cam0 lambda function
    genCam1: Generator[Any, Any, None] = None  # cam1 image generator
    idxCam1: Callable[[int], Image] = None  # cam1 lambda function
    genCam2: Generator[Any, Any, None] = None  # cam2 image generator
    idxCam2: Callable[[int], Image] = None  # cam2 lambda function
    genCam3: Generator[Any, Any, None] = None  # cam3 image generator
    idxCam3: Callable[[int], Image] = None  # cam3 lambda function
    iterGray: Iterator[Tuple[Any, Any]] = None  # generator that loads monochrome stereo pairs (cam0, cam1)
    iterRGB: Iterator[Tuple[Any, Any]] = None  # generator that loads RGB stereo pairs (cam2, cam3)
    genVelo: Generator[Any, Any, None] = None  # generator that loads velodyne scans as [x,y,z,reflectance]
    idxVelo: Callable[[int], ndarray] = None  # velodyne lambda function
    timestamps: List[dt.datetime] = None
    oxts: Dict[str, Any] = None  # OXTS packets and 6-dof poses as named tuples
    calibData: Dict[str, ndarray] = None
    imageType: str = None
    datasetType: str = None


class MsgType(Enum):
    GENERAL = 0
    VIDEO = 1
    POINTCLOUD = 2
    SCALAR = 3


class ImgSide(Enum):
    LEFT = 0
    RIGHT = 1


@dataclass
class BaseMessage:
    index: int = None
    key: str = None
    timestamp: dt.datetime = None
    valueType: MsgType = None


@dataclass
class GeneralMessage(BaseMessage):
    """
    Data class for kitti message object hold generic info like OxtsData and CalibData
    """
    dataDate: str = None
    driveId: str = None
    camToCamCalibTime: dt.datetime = None
    imuToVeloCalibTime: dt.datetime = None
    veloToCamCalibTime: dt.datetime = None
    calibData: Dict[str, ndarray] = None
    valueType: MsgType = field(default=MsgType.GENERAL)


@dataclass
class PointCloudMessage(BaseMessage):
    pointCloudData: bytes = None  # zlib compressed velo data
    valueType: MsgType = field(default=MsgType.POINTCLOUD)


@dataclass
class OxtsDataMessage(BaseMessage):
    oxt: Dict[str, Any] = None
    valueType: MsgType = field(default=MsgType.SCALAR)


@dataclass
class VideoMessage(BaseMessage):
    imgData: bytes = None
    imgType: str = None
    coloured: bool = False
    side: ImgSide = None
    valueType: MsgType = field(default=MsgType.VIDEO)


class KittiMessageJsonEncoder(json.JSONEncoder):

    def __filter_none(self, d: dict):
        for key, value in list(d.items()):
            if value is None:
                del d[key]
            elif isinstance(value, dict):
                self.__filter_none(value)

        return d

    def default(self, any: Any) -> Any:
        if dataclasses.is_dataclass(any):
            encoded = self.__filter_none(dataclasses.asdict(any))
        else:
            if isinstance(any, (dt.date, dt.datetime)):
                encoded = any.isoformat()
            elif isinstance(any, bytes):
                encoded = base64.b64encode(any).decode('ascii')
            elif isinstance(any, ndarray):
                encoded = any.tolist()
            elif isinstance(any, (MsgType, ImgSide)):
                encoded = str(any.name)
            elif isinstance(any, (np.float_, np.float16, np.float32, np.float64)):
                encoded = float(any)
            else:
                encoded = super().default(any)

        return encoded
