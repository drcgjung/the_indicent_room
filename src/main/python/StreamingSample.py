import os
import socket
from pathlib import Path

from kitti.dataparser import DataParser
from kitti.messenger import Messenger

if __name__ == "__main__":
    """
        Usage: Main [loops]
    """

    HOST, PORT = "localhost", 9999

    path_cwd = Path(os.getcwd())
    root = str(path_cwd)
    res_dir_path = Path(f'{root}/resources'.replace('\\', '/'))
    data_dir: str = f'{str(res_dir_path)}/2011_09_26'
    drive_id = '0001'

    # Create a socket (SOCK_STREAM means a TCP socket)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:

        data_parser = DataParser(data_dir, drive_id)
        kitti_data = data_parser.parse()
        messenger = Messenger(kitti_data)

        # Connect to server and send data
        sock.connect((HOST, PORT))
        sock.settimeout(5 * 60)  # 5 minutes timeout

        cond = lambda: messenger.message_count == 100000
        messenger.send_all_data_messages(sock, source='Python', channel='Test', break_condition=cond)

    finally:
        sock.close()

