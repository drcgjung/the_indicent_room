export interface RenderLabelMessage {
    title: string;
    color: number;
    pointCloud: {
        position: number[];
        size: number[];
    };
    video: {
        position: number[];
        size: number[];
    };
}

export interface LabelBox {
    title: string; // label title as sprite text
    position: number[]; // x, y, z coords.
    size: number[]; // w, h, l params.
    color: number; // rgb as hex number
}