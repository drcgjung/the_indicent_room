export enum DataType {
    VEHICLE_METADATA = 0,
    VEHICLE_VIDEODATA = 1,
    VEHICLE_POINTCLOUDDATA = 2,
    VEHICLE_VIDEOPOINTCLOUDDATA = 3,
    VEHICLE_CHANNELDATA = 4,
}

export interface BaseVehicleData {
    _key: string;
    frames: number;
    startTime: Date;
    endTime: Date;
    tensorType: string;
    driveId: string;
    vehicle: string;
    dataDate: string;
}

export interface VehicleMetaData extends BaseVehicleData {
    veloToCamCalibTime: string;
    calibData: string;
    camToCamCalibTime: string;
    imuToVeloCalibTime: string;
}

export interface ChannelMetaData extends BaseVehicleData {
    option1: string;
    option2: string;
    option3: string;
    frequency: string;
    device: string;
    targetType: string;
}

export interface OxtsDataItem {
    channel: string,
    value: number;
}

export interface VehicleOxtsData {
    channel: string,
    startTime: Date,
    endTime: Date,
    tensorType: string,
    _key: string,
    value: number;
}

export interface VehicleVideoData extends BaseVehicleData {
    imgData: string;
    imgType: string;
    isColoured: boolean;
    imgSide: string;
}

export interface VehiclePointCloudData extends BaseVehicleData {
    pointCloudData?: string;
    pointCloudCoords?: Float32Array;
}

export function vehicleName(vehicle: BaseMetaData): string {
    return `${vehicle.dataDate}_${vehicle.driveId}`;
}

export type VideoPointCloudTuple = [VehicleVideoData[], VehiclePointCloudData[]];