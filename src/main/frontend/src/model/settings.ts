export const SETTINGS = {
    colors: {
        magenta: '#E20074',
        text: '#FEFEFE',
        dark: '#474747',
        label: '#FFFF00',
    }
};

export function toHexColor(hexColor: string): number {
    const regexp = /(#)([a-fA-F0-9]+)/;
    if (!regexp.test(hexColor)) {
        return 0xffffff;
    } else {
        const [, , num] = regexp.exec(hexColor) || [];
        const hexNum = parseInt(num, 16);
        return hexNum;
    }
}