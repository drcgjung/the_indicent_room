export interface TimeInterval {
    startTime: Date,
    endTime: Date;
}

export interface TimeValue {
    startTime: Date;
    value: number;
}