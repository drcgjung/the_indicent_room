import { toHexColor } from './settings';
test('test settings function', () => {
    const color = '#E20074';
    const num = toHexColor(color);
    expect(num).toBe(0xE20074);
});
