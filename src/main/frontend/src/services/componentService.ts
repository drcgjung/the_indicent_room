import { Observable, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';

export interface ComponentMessage {
    from: string,
    to: string,
    data: any
}

export class ComponentService {

    private static instance?: ComponentService;

    private subject: Subject<ComponentMessage>;

    constructor() {
        this.subject = new Subject()
    }

    sendMessage(msg: ComponentMessage): void {
        this.subject.next(msg);
    }

    flush(): void {
        this.subject.next();
    }

    onMessage(): Observable<ComponentMessage> {
        return this.subject.asObservable();
    }

    getBusMessage(busId: string): Observable<ComponentMessage> {
        return this.onMessage().pipe(filter(msg => msg.to == busId));
    }

    static getInstance(): ComponentService {
        if(!this.instance) {
            this.instance = new ComponentService();
        }
        return this.instance;
    }
}
