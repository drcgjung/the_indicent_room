import roboto from '../assets/fonts/roboto.json';
import { Font, FontLoader } from 'three';

export class ThreeService {

    static getRobotoFont(): Font {
        return new FontLoader().parse(roboto);
    }
}