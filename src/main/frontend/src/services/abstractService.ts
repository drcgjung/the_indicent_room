import axios, { AxiosBasicCredentials, AxiosInstance, AxiosResponse } from 'axios';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface ServiceOptions {
    user: string;
    password: string;
    protocol: string,
    host: string;
    port: number;
    context: string;
}

export abstract class AbstractService implements ServiceOptions {
    protocol: string | 'http';
    host: string | 'localhost';
    port: number | 4040;
    context: string | 'bdsp';
    user: string | null;
    password: string | null;
    path: string

    protected client: AxiosInstance;

    constructor(options: ServiceOptions) {
        this.protocol = options.protocol;
        this.host = options.host;
        this.port = options.port;
        this.context = options.context;
        this.user = options.user;
        this.password = options.password;
        this.client = this.initClient();
    }

    protected initClient(): AxiosInstance {
        var headersO:any=  {
            'Accept': 'application/json',
            'Content-Type': 'application/json;charset=utf-8',
        }
        var authO:AxiosBasicCredentials|undefined= undefined;
        var useCredentials = false;
        if(this.user!=null) {
            authO={
                username: this.user!!,
                password: this.password!!
            };
            useCredentials = true;
            headersO['Access-Control-Allow-Origin']= '*';
        }
        return axios.create({
            withCredentials: useCredentials,
            baseURL: `${this.protocol}://${this.host}:${this.port}/${this.context}`,
            auth: authO,
            headers: headersO
        })
    }
}
