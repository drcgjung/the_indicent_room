import { AbstractService, ServiceOptions } from './abstractService';
import {
    OxtsDataItem,
    VehicleMetaData,
    VehicleOxtsData,
    VehiclePointCloudData,
    VehicleVideoData
    } from '../model/vehicle.model';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
/* eslint-disable @typescript-eslint/no-explicit-any */

export class VehicleService extends AbstractService {

    private static vehicleService: VehicleService;

    private constructor(options: ServiceOptions) {
        super(options);
    }

    /**
     * Get vehicle metadata from service
     * @return {Observable}
     */
    getVehicleMetaData$(): Observable<any[]> {
        return from(this.client.get('/vehicle'))
               .pipe(map(resp => resp.data.map((item: any) => item)));
    }

    /**
     * Get vehicle metadata from service
     * @return {Observable}
     */
    getChannelMetaData$(dataKey: string): Observable<any[]> {
        return from(this.client.get(`/vehicle/channels/${dataKey}`))
               .pipe(map(resp => resp.data.map((item: any) => item)));
    }

    /**
     * Gets vehicles video data
     * @param dataKey 
     * @param coloured 
     * @param side 
     * @param pos 
     * @param length 
     * @returns {Observable} of {VehicleVideoData} array
     */
    getVehicleVideoData$(dataKey: string, coloured = true, side = 'LEFT', pos = 0, length = 10): Observable<VehicleVideoData[]> {
        return from(this.client.get(`/vehicle/videodata/${dataKey}?coloured=${coloured}&side=${side}&pos=${pos}&length=${length}`))
            .pipe(map(resp => resp.data.map((item: any) => {
                return <VehicleVideoData>{
                    dataKey: item['key'],
                    index: item['index'],
                    startTime: new Date(item['startTime']),
                    endTime: new Date(item['endTime']),
                    isColoured: Boolean(item['isColoured']),
                    imgSide: item['imgSide'],
                    imgType: item['imgType'],
                    imgData: item['imgData'],
                };
            })))
            .pipe(map((data: VehicleVideoData[]) => data.map(vvd => {
                vvd.endTime = vvd.startTime > vvd.endTime ? vvd.startTime : vvd.endTime;
                return vvd;
            })));
    }

    /**
     * Gets vehicle lidar / point cloud data
     * @param dataKey 
     * @param pos 
     * @param length 
     * @returns {Observable} of {VehiclePointCloudData} array
     */
    getVehiclePointCloudData$(dataKey: string, pos = 0, length = 10): Observable<VehiclePointCloudData[]> {
        return from(this.client.get(`/vehicle/pointclouddata/${dataKey}?pos=${pos}&length=${length}`))
            .pipe(map(resp => resp.data.map((item: any) => {
                return <VehiclePointCloudData>{
                    dataKey: item['key'],
                    index: item['index'],
                    startTime: new Date(item['startTime']),
                    endTime: new Date(item['endTime']),
                    pointCloudData: item['pointCloudData']
                };
            })))
            .pipe(map((data: VehiclePointCloudData[]) => data.map(vpcd => {
                vpcd.endTime = vpcd.startTime > vpcd.endTime ? vpcd.startTime : vpcd.endTime;
                return vpcd;
            })));
    }

    /**
     * Gets vehicle oxtsdata that means KITTI channnel data sets
     * @param dataKey 
     * @returns {Observable} of {VehicleOxtsData} array
     */
    getVehicleChannelData$(dataKey: string): Observable<VehicleOxtsData[]> {
        return from(this.client.get(`/vehicle/oxtsdata/${dataKey}`))
            .pipe(map(resp => resp.data.map((item: any) => {
                return <VehicleOxtsData>{
                    startTime: new Date(item['startTime']),
                    endTime: new Date(item['endTime']),
                    channel: item['channel'],
                    tensorType: item['tensorType'],
                    _key:item['key'],
                    value:item['value']
                };
            })));
    }

    static getInstance(): VehicleService {
        if (this.vehicleService == undefined) {
            this.vehicleService = new VehicleService({
                protocol: `${process.env.REACT_APP_SERVICE_PROTOCOL}`,
                host: `${process.env.REACT_APP_SERVICE_HOST}`,
                port: Number(process.env.REACT_APP_SERVICE_PORT),
                context: `${process.env.REACT_APP_SERVICE_CONTEXT}`,
                user: `${process.env.REACT_APP_SERVICE_USER}`,
                password: `${process.env.REACT_APP_SERVICE_PASSWORD}`
            });
        }
        return this.vehicleService;
    }
}
