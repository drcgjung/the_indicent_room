import { Headbar } from './components/headerbar';
import { Jobbar } from './components/jobbar';
import { MainContent } from './components/maincontent';
import { Sidebar } from './components/sidebar';
import React, { Component } from 'react';

export interface Properties {
    userName: string;
    lang: string;
    title: string;
}

export interface AppState {
    headBarSize: number;
}

export class App extends Component<Properties, AppState> {
    constructor(props: Properties) {
        super(props);
        this.state = {
            headBarSize: 300
        };
    }

    render(): JSX.Element {
        return (
            <div className="app">
                <Sidebar key="left_sidebar" menuCollapsed={true} menuToggled={false} />
                <main>
                    <header>
                        <Headbar title={this.props.title} />
                    </header>
                    <div className='main_content_container'>
                        <MainContent />
                    </div>
                </main>
                <Jobbar key="right_sidebar" menuCollapsed={true} menuToggled={false} />
            </div>
        );
    }
}
