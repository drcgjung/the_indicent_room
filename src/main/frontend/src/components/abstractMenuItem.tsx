import { Headbar } from './headerbar';
import { ComponentService } from '../services/componentService';
import React, { Component } from 'react';

export interface ItemState {
    onclick: () => void;
}

export abstract class AbstractMenuItem<P> extends Component<P, ItemState> {
    protected msgBus: ComponentService;

    constructor(props: P) {
        super(props);
        this.msgBus = ComponentService.getInstance();
        this.state = {
            onclick: () => {
                this.onClick();
            }
        };
    }

    abstract onClick(): void;
    abstract getIcon(): JSX.Element;
    abstract getStyleClass(): string;
    abstract getLabel(): string;
    abstract getVehicle(): string;
    abstract getFormattedTimestamp(): string;

    protected normalHeadbarSize(fromBusId: string): void {
        this.msgBus.sendMessage({
            from: fromBusId,
            to: Headbar.BUSID,
            data: false
        });
    }

    render(): JSX.Element {
        return (
            <div className={this.getStyleClass()} onClick={this.state.onclick}>
                <div className='info_item'>
                    <span>{this.getIcon()}  {this.getLabel()}</span>
                    <br></br>
                    <span>{this.getVehicle()}</span>
                </div>
            </div>
        );
    }
}