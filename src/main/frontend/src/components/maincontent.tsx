import { ChannelDataView } from './channels/channelDataView';
import { ChannelMenuItem } from './channels/channelMenuItem';
import { LanesDataView } from './lanes/lanesDataView';
import { ThreeJsLane } from './lanes/threejslane';
import { Loader } from './loader';
import { FooterMessage, MainViewFooter } from './mainviewparts/mainViewFooter';
import { PointCloudDataView } from './pointcloud/pointCloudDataView';
import { PointCloudViewer } from './pointcloud/pointCloudViewer';
import { RangeSlider, RangeSliderMessage } from './utils/rangeSlider';
import { VehicleDataView } from './vehicle/vehicleDataView';
import { VehicleMenuItem } from './vehicle/vehicleMenuItem';
import { ImageSlider } from './video/imageslider';
import { VideoDataView } from './video/videoDataView';
import { TimeInterval } from '../model/mainview.model';
import { VehicleMetaData, ChannelMetaData } from '../model/vehicle.model';
import { ComponentMessage, ComponentService } from '../services/componentService';
import React, { Component } from 'react';
import {Headbar} from './headerbar';
import { RiCheckboxBlankLine } from 'react-icons/ri';

/* eslint-disable @typescript-eslint/no-explicit-any */

export interface MainProperties {
    data?: any;
}

export interface MainState {
    dataKey: string,
    activeView: string,
    view: JSX.Element;
    video: PageState;
    pointCloud: PageState;
    timerInterval: TimeInterval;
}

export interface PageState {
    pos: number;
    length: number;
}

export class MainContent extends Component<MainProperties, MainState> {
    static BUSID = 'MainContent';

    private msgBus: ComponentService;

    constructor(props: MainProperties) {
        super(props);
        const pageState: PageState = {
            pos: Number(process.env.REACT_APP_SERVICE_VIDEO_POS),
            length: Number(process.env.REACT_APP_SERVICE_VIDEO_DATALEN)
        };
        const now = new Date();
        const twoHoursLater = new Date(now.getTime() + 2 * 60 * 1000);
        this.state = {
            dataKey: '',
            view: null,
            activeView: "NONE",
            video: pageState,
            pointCloud: pageState,
            timerInterval: {
                startTime: now,
                endTime: twoHoursLater
            },
        };
        this.msgBus = ComponentService.getInstance();
        this.msgBus.getBusMessage(MainContent.BUSID)
            .subscribe(msg => {
                if (msg.from == VehicleMenuItem.BUSID) {
                    this.setVehicleView(msg);
                } else if (msg.from == ChannelMenuItem.VIDEO_BUSID) {
                    this.setVideoView(msg);
                } else if (msg.from == ChannelMenuItem.POINTCLOUD_BUSID) {
                    this.setPointCloudView(msg);
                } else if (msg.from == ChannelMenuItem.CHANNEL_BUSID) {
                    this.setChannelDataView(msg);
                } else {
                    console.error(`Message from ${msg.from} is currently not supported!`);
                }
            });
    }

    private setVehicleView(msg: ComponentMessage): void {
        const vehicle: VehicleMetaData = msg.data;
        if(this.state.activeView!="VEHICLE" || this.state.dataKey!=vehicle._key) {
            this.setState({
            dataKey: vehicle._key,
            activeView:"VEHICLE",
            view: <VehicleDataView
                key={vehicle._key}
                _key={vehicle._key}
                dataDate={vehicle.dataDate}
                driveId={vehicle.driveId}
                vehicle={vehicle.vehicle} 
                frames={vehicle.frames} 
                startTime={vehicle.startTime}
                endTime={vehicle.endTime}
                tensorType={vehicle.tensorType}
                calibData={vehicle.calibData}
                camToCamCalibTime={vehicle.camToCamCalibTime}
                imuToVeloCalibTime={vehicle.imuToVeloCalibTime}
                veloToCamCalibTime={vehicle.veloToCamCalibTime}
                />
            });
            this.setHeadbarSmaller(false); 
            this.showFooter(false);
        }
    }

    private setHeadbarSmaller(smaller:boolean) {
        this.msgBus.sendMessage({
            from:MainContent.BUSID,
            to:Headbar.BUSID,
            data:smaller
        });
    }

    private setVideoView(msg: ComponentMessage): void {
        const channel: ChannelMetaData = msg.data;
        const videoView="VIDEO"+channel.option2;
        const multiView="MULTI"+channel.option2;
        if(this.state.activeView=="POINTCLOUD" && this.state.dataKey==channel._key) {
            this.setLanesDataView(msg,channel.frequency,channel.option2,channel.option3=="true");
        } else if((this.state.activeView!=videoView && this.state.activeView!=multiView) || this.state.dataKey!=channel._key) {
            
            const vdStartTime: number = new Date(String(channel.startTime)).getTime();
            const vdEndTime: number = new Date(String(channel.endTime)).getTime();
            const allTimeValues: number[] = [];
            for (let i = 0; i < channel.frames; i++) {
                allTimeValues.push(vdStartTime + i*(vdEndTime-vdStartTime)/channel.frames);
            }
            this.setState({
                dataKey:channel._key,
                activeView:videoView,
                view: <VideoDataView
                    key={channel._key+channel.option2+channel.option3}
                    _key={channel._key}
                    imgType={channel.frequency}
                    imgSide={channel.option2}
                    coloured={channel.option3=="true"}
                    times={allTimeValues}
                    startTime={allTimeValues[0]}
                    endTime={allTimeValues[9]}
                />
            });
            this.setHeadbarSmaller(true); 
            this.showFooter(true);
            this.setTimeRange(allTimeValues[0], allTimeValues[9], allTimeValues, ImageSlider.BUSID);
        }
    }

    private setPointCloudView(msg: ComponentMessage): void {
        const channel: ChannelMetaData = msg.data;
        if(this.state.activeView.indexOf("VIDEO")==0 && this.state.dataKey==channel._key) {
            var existingView = this.state.view as VideoDataView;
            this.setLanesDataView(msg,existingView.props.imgType,existingView.props.imgSide,existingView.props.coloured);
        } else if((this.state.activeView!="POINTCLOUD" && this.state.activeView.indexOf("MULTI")==0) || this.state.dataKey!=channel._key) {
            const pcdStartTime: number = new Date(String(channel.startTime)).getTime();
            const pcdEndTime: number = new Date(String(channel.endTime)).getTime();
            const allTimeValues: number[] = [];
            for (let i = 0; i < channel.frames; i++) {
                allTimeValues.push(pcdStartTime + i*(pcdEndTime-pcdStartTime)/channel.frames);
            }
            this.setState({
                dataKey:channel._key,
                activeView:"POINTCLOUD",
                view: <PointCloudDataView
                  key={channel._key}
                  _key={channel._key}
                  times={allTimeValues}
                  startTime={allTimeValues[0]}
                  endTime={allTimeValues[4]}
               />,
            });
            this.setHeadbarSmaller(true); 
            this.showFooter(true);
            this.setTimeRange(allTimeValues[0], allTimeValues[4], allTimeValues, PointCloudViewer.BUSID);
        }
    }

    private setLanesDataView(msg: ComponentMessage, imgType:string, imgSide:string, isColoured:boolean): void {
        const channel: ChannelMetaData = msg.data;
        const multiView = "MULTI"+imgSide;
        if(this.state.activeView!=multiView || this.state.dataKey!=channel._key) {
            const pcdStartTime: number = new Date(String(channel.startTime)).getTime();
            const pcdEndTime: number = new Date(String(channel.endTime)).getTime();
            const allTimeValues: number[] = [];
            for (let i = 0; i < channel.frames; i++) {
                allTimeValues.push(pcdStartTime + i*(pcdEndTime-pcdStartTime)/channel.frames);
            }            
            this.setState({
                dataKey:channel._key,
                activeView:multiView,
                view: <LanesDataView
                        key={"lane"+channel._key+channel.option2+channel.option3}
                        _key={channel._key}
                        imgType={imgType}
                        imgSide={imgSide}
                        coloured={isColoured}
                        times={allTimeValues}
                        startTime={allTimeValues[0]}
                        endTime={allTimeValues[4]}
                    />,
            });
            this.setHeadbarSmaller(true); 
            this.showFooter(true);
            this.setTimeRange(allTimeValues[0], allTimeValues[4], allTimeValues, ThreeJsLane.BUSID);
        }
    }

    private setChannelDataView(msg: ComponentMessage): void {
        const channel: ChannelMetaData = msg.data;
        if(this.state.activeView!="CHANNEL" || this.state.dataKey!=channel._key) {
          
            const chStarTime: number = new Date(String(channel.startTime)).getTime();
            const chEndTime: number = new Date(String(channel.endTime)).getTime();
            const allTimeValues: number[] = [];
            for (let i = 0; i < channel.frames; i++) {
                allTimeValues.push(chStarTime + i*(chEndTime-chStarTime)/channel.frames);
            }
            this.setState({
                dataKey:channel._key,
                activeView:"CHANNEL",
                view: <ChannelDataView
                    key={channel._key}
                    _key={channel._key}
                    times={allTimeValues}
                />
            });
            this.setHeadbarSmaller(true); 
            this.showFooter(true);
            this.setTimeRange(allTimeValues[0],allTimeValues[allTimeValues.length-1], allTimeValues, ChannelDataView.BUSID);
        }
    }

    private showFooter(show: boolean) {
        this.msgBus.sendMessage({
            from: MainContent.BUSID,
            to: MainViewFooter.BUSID,
            data: {
                show: show
            } as FooterMessage
        });
    }

    private setTimeRange(start: number, end: number, values: number[], busId: string): void {
        this.msgBus.sendMessage({
            from: MainContent.BUSID,
            to: RangeSlider.BUSID,
            data: {
                start: start,
                end: end,
                pips:values,
                receiverBusId: busId
            } as RangeSliderMessage
        });
    }

    render(): JSX.Element {
        return (
            <>
                <div className='main_view_content'>
                    {this.state.view}
                </div>
                <MainViewFooter>
                    <RangeSlider className='time_slider'
                        startTotal={this.state.timerInterval.startTime.getTime()}
                        endTotal={this.state.timerInterval.endTime.getTime()}
                        withPips={10*60*1000} />
                </MainViewFooter>
            </>
        );
    }
}
