import { JobStatus, JobStatusChart } from './jobs/jobstatus';
import { Label } from './labels/label';
import { LabelType } from './labels/labelType';
import * as React from 'react';
import { Component } from 'react';
import { ListGroup } from 'react-bootstrap';
import {
    FaPaperPlane,
    FaRegPaperPlane,
    FaRoad,
    FaTachometerAlt,
    FaTag,
    FaTasks
    } from 'react-icons/fa';
import { GiGears } from 'react-icons/gi';
import { RiMenuFoldFill, RiMenuUnfoldFill } from 'react-icons/ri';
import {
    Menu,
    ProSidebar,
    SidebarContent,
    SidebarFooter,
    SidebarHeader,
    SubMenu
    } from 'react-pro-sidebar';
/* eslint-disable @typescript-eslint/no-extra-non-null-assertion */
/* eslint-disable @typescript-eslint/no-non-null-assertion */



export interface JobbarProps {
    menuCollapsed: boolean;
    menuToggled: boolean;
    coloured?: boolean;
    imgSide?: string;
}

export interface JobbarState {
    isMenuCollapsed: boolean;
    isMenuToggled: boolean;
}
export class Jobbar extends Component<JobbarProps, JobbarState> {

    clickOnCollapse: () => void;
    clickOnToggle: () => void;
    loadJobStatus: () => JobStatus[];

    constructor(props: JobbarProps) {
        super(props);
        this.state = {
            isMenuCollapsed: props.menuCollapsed,
            isMenuToggled: props.menuToggled,
        };

        /** 
         * in react methods are state based that means attributes of the state is to be set over
         * React component state setter
         */
        this.clickOnCollapse = () => this.onCollapse();
        this.clickOnToggle = () => this.onToggle();
        this.loadJobStatus = () => {
            return [
                {
                    jobName: '2e84ee0074d3',
                    jobType: 'Labeling "Full-Breaking"',
                    jobResults: [{
                        type: 'Full-Break',
                        drives: ['2011_09_26_0001'],
                        attributes: [{ velocity: -20.0 }],
                        startTime: 11,
                        endTime: 27,
                    }, {
                        type: 'Full-Break',
                        drives: ['2011_09_26_0079'],
                        attributes: [{ velocity: -40.0 }],
                        startTime: 87,
                        endTime: 99,
                    }],
                    jobProgress: 23
                },
                {
                    jobName: 'de75f3447caf',
                    jobType: 'Labeling "Front Objects"',
                    jobResults: [{
                        type: 'Front Object',
                        drives: ['2011_09_26_0001'],
                        attributes: [{ vehicleType: 'SUV' }],
                        startTime: 0,
                        endTime: 5,
                        labelInfo: {
                            title: 'SUV',
                            type: LabelType.OBJECT,
                            color: 0xffff00,
                            pointCloud: {
                                position: [25, 9, -1],
                                size: [5, 2, 2]
                            },
                            video: {
                                position: [-0.21, -0.01],
                                size: [0.09, 0.065]
                            }
                        }
                    },
                    {
                        type: 'Front Object',
                        drives: ['2011_09_26_0079'],
                        attributes: [{ vehicleType: 'PCar' }],
                        startTime: 0,
                        endTime: 5,
                        labelInfo: {
                            title: 'Passenger Car',
                            type: LabelType.OBJECT,
                            color: 0x00ffff,
                            pointCloud: {
                                position: [16, 4.5, -1],
                                size: [6, 2.5, 2.5]
                            },
                            video: {
                                position: [-0.18, -0.035],
                                size: [0.12, 0.08]
                            }
                        }
                    }],
                    jobProgress: 71
                },
                {
                    jobName: '6f8e77900ff0',
                    jobType: 'Transcoding',
                    jobResults: [{
                        type: 'MDF',
                        drives: ['2011_09_26_0001', '2011_09_26_0079'],
                        attributes: [],
                        startTime: 1,
                        endTime: 208,
                    }],
                    jobProgress: 54
                }
            ];
        };
    }

    private onCollapse(): void {
        if (this.state.isMenuCollapsed) {
            this.setState({
                isMenuCollapsed: false,
                isMenuToggled: this.state.isMenuToggled
            });
        } else {
            this.setState({
                isMenuCollapsed: true,
                isMenuToggled: this.state.isMenuToggled
            });
        }
    }

    private onToggle(): void {
        if (this.state.isMenuToggled) {
            this.setState({
                isMenuCollapsed: this.state.isMenuCollapsed,
                isMenuToggled: false
            });
        } else {
            this.setState({
                isMenuCollapsed: this.state.isMenuCollapsed,
                isMenuToggled: true
            });
        }
    }

    render(): JSX.Element {
        return (
            <ProSidebar
                className='right-side-bar'
                onToggle={this.clickOnToggle}
                toggled={this.state.isMenuToggled}
                collapsed={this.state.isMenuCollapsed}
                breakPoint="md">
                <SidebarHeader>
                    <div className="close_menu_button_right" onClick={this.clickOnCollapse}>
                        {!this.state.isMenuCollapsed ? <RiMenuUnfoldFill /> : <RiMenuFoldFill />}
                    </div>
                </SidebarHeader>
                <SidebarContent>
                    <Menu iconShape="square">
                        <SubMenu icon={<FaPaperPlane />} title="Projects & Scenarios">
                            <ListGroup className='job_list'>
                                <ListGroup.Item key="KITTI" className='job_list_item'>
                                    <span><FaRegPaperPlane /> KITTI</span>
                                </ListGroup.Item>
                                <ListGroup.Item key="KITTI-City" variant="secondary" className='job_list_item'>
                                    <span><FaRoad /> City</span>
                                </ListGroup.Item>
                                <ListGroup.Item key="KITTI-Residential" variant="secondary" className='job_list_item'>
                                    <span><FaRoad /> Residential</span>
                                </ListGroup.Item>
                                <ListGroup.Item key="CARLA" className='job_list_item'>
                                    <span><FaRegPaperPlane /> CARLA</span>
                                </ListGroup.Item>
                                <ListGroup.Item key="Town01" variant="secondary" className='job_list_item'>
                                    <span><FaRoad /> Town01</span>
                                </ListGroup.Item>
                            </ListGroup>
                        </SubMenu>
                        <SubMenu icon={<FaTasks />} title="Jobs & Labels">
                            <ListGroup className='job_list'>
                                {
                                    this.loadJobStatus().flatMap((job, idx) => {
                                        const initial = [
                                            <ListGroup.Item key={`job_item_${idx}`} className='job_list_item'>
                                                <span><GiGears /> {job.jobName}</span>
                                                <br></br>
                                                <span>{job.jobType}</span>
                                            </ListGroup.Item>
                                        ];
                                        job.jobResults.forEach((result, idxr) => {
                                            initial.push(
                                                <ListGroup.Item key={`job_item_${idx}_${idxr}`} variant="secondary" className='job_list_item'>
                                                    {'labelInfo' in result ?
                                                        <Label
                                                            key={`label_${idx}_${idxr}`}
                                                            title={result.labelInfo!!.title}
                                                            type={result.labelInfo!!.type}
                                                            drive={result.drives[0]}
                                                            startTime={result.startTime}
                                                            endTime={result.endTime}
                                                            vehicleType={result.attributes[0]}
                                                            pointCloud={result.labelInfo!!.pointCloud}
                                                            video={result.labelInfo!!.video} /> :
                                                        <span><FaTag /> {result.type} ({result.startTime} - {result.endTime})</span>
                                                    }
                                                </ListGroup.Item>
                                            );
                                        });
                                        return initial;
                                    })
                                }
                            </ListGroup>
                        </SubMenu>
                        <SubMenu icon={<FaTachometerAlt />} title="Monitor">
                            <JobStatusChart loadJobStatus={this.loadJobStatus} />
                        </SubMenu>
                    </Menu>
                </SidebarContent>
                <SidebarFooter>
                </SidebarFooter>
            </ProSidebar>
        );
    }
}
