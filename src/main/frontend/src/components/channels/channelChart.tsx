import { TimeValue } from '../../model/mainview.model';
import { SETTINGS } from '../../model/settings';
import { dateFormat } from '../utils/formatUtil';
import {Chart, ChartData } from 'chart.js';
import React, {
    MutableRefObject,
    useEffect,
    useMemo,
    useRef
    } from 'react';

interface ChartProperties {
    values: Map<string,TimeValue[]>;
    times: number[];
    count: number;
}

export function HSLToRGB(h:number,s:number,l:number) {
    // Must be fractions of 1
    s /= 100;
    l /= 100;
  
    let c = (1 - Math.abs(2 * l - 1)) * s,
        x = c * (1 - Math.abs((h / 60) % 2 - 1)),
        m = l - c/2,
        r = 0,
        g = 0,
        b = 0;
     if (0 <= h && h < 60) {
            r = c; g = x; b = 0;  
     } else if (60 <= h && h < 120) {
            r = x; g = c; b = 0;
     } else if (120 <= h && h < 180) {
            r = 0; g = c; b = x;
     } else if (180 <= h && h < 240) {
            r = 0; g = x; b = c;
     } else if (240 <= h && h < 300) {
            r = x; g = 0; b = c;
     } else if (300 <= h && h < 360) {
            r = c; g = 0; b = x;
     }
     r = Math.round((r + m) * 255);
     g = Math.round((g + m) * 255);
     b = Math.round((b + m) * 255);
        
     return "rgb(" + r + "," + g + "," + b + ")";
  }

export const ChannelChart: React.FC<ChartProperties> = ({ values, times, count }) => {

    const canvasRef = useRef<HTMLCanvasElement>() as MutableRefObject<HTMLCanvasElement>;
    const negLogValue = (n: number) => {
        if(isNaN(n)) {return 0; } else {return -1 * Math.log(Math.abs(n)); }
    };
    const textColor = SETTINGS.colors.text;
    const colors=new Map<string,string>();
    values.forEach((series,channel) => {
        var newColor=HSLToRGB((colors.size*360)/values.size,100,80);
        colors.set(channel,newColor);
    });

    useEffect(() => {
        if (canvasRef.current) {
            var dataSets = new Array<Chart.ChartDataSets>();
            values.forEach((series,channel) => {
                dataSets.push({
                    label: channel,
                    steppedLine: 'after',
                    borderColor: colors.get(channel),
                    fill: false,
                    data: series.map(tv => negLogValue(tv.value)),
                    hidden: dataSets.length<34,
                });
            });
            const canvas: HTMLCanvasElement = canvasRef.current;
            canvas.height=640;
            const chartData: ChartData = {
                labels: times.map(time => new Date(time)),
                datasets: dataSets,
            };
            const chart = new Chart(canvas, {
                    type: 'line',
                    data: chartData,
                    options: {
                        legend: {
                            display: true,
                            labels: {
                                fontColor: textColor
                            }
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            type: 'logarithmic',
                            display: false,
                            ticks: {
                                fontColor: textColor
                            }
                        }],
                        xAxes: [{
                            type: 'time',
                            ticks: {
                                display: false,
                                fontColor: textColor
                            },
                            time: {
                                displayFormats: {
                                    // 'millisecond': 'SSS [ms]',
                                    'second': 'hh:mm:ss a', // 11:20:01 AM
                                }
                            }
                        }]
                    },
                    title: {
                        display: false,
                        text: ''
                    }
                }
            });
            chart.update();

        }
    }, [values, times, count]);

    return (
        <canvas className='channel_chart' ref={canvasRef} />
    );
};