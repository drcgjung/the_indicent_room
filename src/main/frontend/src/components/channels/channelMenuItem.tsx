import { ChannelMetaData } from '../../model/vehicle.model';
import { MainContent } from '../maincontent';
import React, { Component } from 'react';
import { FaChartLine, FaVideo, FaCloud } from 'react-icons/fa';
import { ComponentService } from '../../services/componentService';
import { AiOutlineFundView } from 'react-icons/ai';
import {MenuItem } from 'react-pro-sidebar';
import { Headbar } from '../headerbar';

export interface ItemState {
    onclick: () => void;
}

export class ChannelMenuItem extends Component<ChannelMetaData, ItemState> {

    static CHANNEL_BUSID = 'ChannelMenuItem';
    static VIDEO_BUSID = 'VideoMenuItem';
    static POINTCLOUD_BUSID = 'PointCloudMenuItem';
    static LANE_BUSID = 'LaneMenuItem';

    private msgBus: ComponentService;

    constructor(props: ChannelMetaData) {
        super(props);
        this.msgBus = ComponentService.getInstance();
        this.state = {
            onclick: () => {
                this.onClick();
            }
        };
    }

    onClick(): void {
        var sender=ChannelMenuItem.CHANNEL_BUSID;
        if(this.props.targetType=="VIDEO") {
            sender=ChannelMenuItem.VIDEO_BUSID;
        } else if(this.props.targetType=="POINTCLOUD") {
            sender=ChannelMenuItem.POINTCLOUD_BUSID;
        } else if(this.props.targetType=="MULTI") {
            sender=ChannelMenuItem.LANE_BUSID;
        }
        this.msgBus.sendMessage({
            from: sender,
            to: MainContent.BUSID,
            data: this.props
        });
    }

    render(): JSX.Element {

        var icon:JSX.Element = <FaChartLine />;
        if(this.props.targetType=="VIDEO") {
            icon=<FaVideo />
        } else if(this.props.targetType=="POINTCLOUD") {
            icon=<FaCloud  />
        } else if(this.props.targetType=="MULTI") {
            icon =<AiOutlineFundView  />
        }

        var hidden="channel_menu_item"

        return (
            <MenuItem key={this.key}>
            <div className={hidden} onClick={this.state.onclick}>
                <div>
                    <span> {icon} {this.props.targetType} {this.props.device} ({this.props.frames})</span>
                    <br/>
                    <span className="info_item">{this.props.option1} ({this.props.frequency})</span>
                </div>
            </div>
            </MenuItem>
        );
    }

}