import { icon, LatLngExpression, Map } from 'leaflet';
import markerIcon from 'leaflet/dist/images/marker-icon.png';
import markerIconShadow from 'leaflet/dist/images/marker-shadow.png';
import React, { useMemo, useState } from 'react';
import { useEffect } from 'react';
import { MapContainer, Marker, TileLayer, Polyline } from 'react-leaflet';
import styles from '../../assets/scss/styles.scss';

/* eslint-disable @typescript-eslint/no-extra-non-null-assertion */
/* eslint-disable @typescript-eslint/no-non-null-assertion */

interface MapProperties {
    geoData: LatLngExpression[];
    geoPosition: LatLngExpression;
}

export const LeafletMap: React.FC<MapProperties> = ({ geoData, geoPosition }) => {
    const zoom = 30;

    const [map, setMap] = useState<Map>();

    const mIcon = icon({ 
        iconUrl: markerIcon, 
        shadowUrl: markerIconShadow,  
        iconSize:     [25, 41], // size of the icon
        shadowSize:   [41, 41], // size of the shadow
        iconAnchor:   [12, 41], // point of the icon which will correspond to marker's location
        shadowAnchor: [4, 62],  // the same for the shadow
        popupAnchor:  [0, -41] // point from which the popup should open relative to the iconAnchor
    });

    const displayMap = useMemo(() => {
        var path=new Array<LatLngExpression>();
        for(let i=0;i<geoData.length;i++) {
            path.push(geoData[i]);
        }
        return (
            <MapContainer
                center={geoPosition}
                zoom={zoom}
                scrollWheelZoom={false}
                whenCreated={setMap}>
                <TileLayer
                    attribution="&copy; <a href='http://osm.org/copyright'>OpenStreetMap</a> contributors"
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <Polyline color={styles.magentarColor || '#E20074'} positions={path}/>
                <Marker key={`start_${0}`} icon={mIcon} position={geoData[0]} />
                <Marker key={`end_${0}`} icon={mIcon} position={geoData[geoData.length-1]} />
            </MapContainer>
        );
    }, [geoData, geoPosition]);

    useEffect(() => {
        map?.setView(geoPosition);
    }, [geoData, geoPosition]);

    return (
        <>
            {displayMap}
        </>
    );
};