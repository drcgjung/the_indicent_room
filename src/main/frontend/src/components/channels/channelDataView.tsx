import { ChannelChart } from './channelChart';
import { LeafletMap } from './leafletMap';
import { TimeValue } from '../../model/mainview.model';
import { VehicleOxtsData } from '../../model/vehicle.model';
import { VehicleService } from '../../services/vehicleService';
import { chunkArray } from '../utils/arrayUtils';
import { latLng, LatLngExpression } from 'leaflet';
import 'leaflet/dist/leaflet.css';
import React, { Component } from 'react';
/* eslint-disable @typescript-eslint/no-this-alias */
//import Plot from 'react-plotly.js';

export interface ViewProperties {
    _key: string;
    times: number[];
}

export interface ViewState {
    didMount: boolean;
    geoData: LatLngExpression[];
    geoPosition: LatLngExpression;
    channelData: Map<string,TimeValue[]>;
    channelCount: number;
}

export function zip<T1,T2>(a1:Array<T1>,a2:Array<T2>): Array<[T1,T2]> {
  return a1.map(function(e, i) {
    return [e, a2[i]];
  });
}

export class ChannelDataView extends Component<ViewProperties, ViewState> {

    static BUSID:string="ChannelDataView";

    private vehicleService: VehicleService;

    constructor(props: ViewProperties) {
        super(props);

        this.vehicleService = VehicleService.getInstance();

        this.state = {
            didMount: false,
            geoData: [latLng(49, 7)],
            geoPosition: latLng(49,7),
            channelData: new Map(),
            channelCount: 0
        };

        this.vehicleService.getVehicleChannelData$(this.props._key).subscribe((channelData: VehicleOxtsData[]) => {
            this.state.geoData.length=0;
            this.state.channelData.clear();
            channelData.forEach((signal: VehicleOxtsData) => {
              var array=[signal as TimeValue];
              if(this.state.channelData.has(signal.channel)) {
                array=this.state.channelData.get(signal.channel)!!;
                array.push(signal);
              }
              this.state.channelData.set(signal.channel,array);
            });
            zip(this.state.channelData.get("packet.lat")!!,this.state.channelData.get("packet.lon")!!).map((tuple) => {
              return latLng(tuple[0].value,tuple[1].value); 
            }).forEach((latLng)=> {
              this.state.geoData.push(latLng);
            });

            var center = Math.ceil(this.state.geoData.length/2);
            if (this.state.didMount) {
                this.setState({
                    geoData: this.state.geoData,
                    geoPosition: this.state.geoData[center],
                    channelData: this.state.channelData,
                    channelCount: this.state.channelData.size
                });
            }
        });
    }

    componentDidMount(): void {
        this.setState({ didMount: true });
    }

    render(): JSX.Element {
        return (
            <div className='channel_container'>
                <div className='channel_map'>
                    <LeafletMap geoData={this.state.geoData} geoPosition={this.state.geoPosition}/>
                </div>
                <div className='channel_charts'>
                    <ChannelChart values={this.state.channelData} times={this.props.times} count={this.state.channelCount} />
                </div>
            </div>
        );
    }
}