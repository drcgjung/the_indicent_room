import { ChannelMenuItem } from './channels/channelMenuItem';
import { LanesMenuItem } from './lanes/lanesMenuItem';
import { Loader } from './loader';
import { PointCloudMenuItem } from './pointcloud/pointcloudMenuItem';
import { SearchField } from './utils/searchField';
import { VehicleMenuItem } from './vehicle/vehicleMenuItem';
import { VideoMenuItem } from './video/videoMenuItem';
import logo from '../assets/images/logo.svg';
import {
    ChannelMetaData,
    DataType,
    VehicleMetaData,
    vehicleName,
    VehicleOxtsData,
    VehiclePointCloudData,
    VehicleVideoData,
    VideoPointCloudTuple
    } from '../model/vehicle.model';
import { VehicleService } from '../services/vehicleService';
import * as React from 'react';
import { Component } from 'react';
import { ListGroup } from 'react-bootstrap';
import {
    FaCar,
    FaFastForward,
    FaGlobe,
    FaHdd,
    FaPause
    } from 'react-icons/fa';
import { FiHelpCircle } from 'react-icons/fi';
import { RiMenuFoldFill, RiMenuUnfoldFill } from 'react-icons/ri';
import {
    Menu,
    MenuItem,
    ProSidebar,
    SidebarContent,
    SidebarFooter,
    SidebarHeader,
    SubMenu
    } from 'react-pro-sidebar';
import { zip } from 'rxjs';
import { concatAll, filter, map } from 'rxjs/operators';
/* eslint-disable no-case-declarations */
/* eslint-disable @typescript-eslint/no-extra-non-null-assertion */
/* eslint-disable @typescript-eslint/no-non-null-assertion */


interface Pageable {
    pos: number,
    length: number;
}

export interface SidebarProps {
    menuCollapsed: boolean;
    menuToggled: boolean;
    coloured?: boolean;
    imgSide?: string;
}

export interface SidebarState {
    isMenuCollapsed: boolean;
    isMenuToggled: boolean;

    vehicleDataMenuItems: JSX.Element[];
    vehicleDataMenuItemsVisible: JSX.Element[];
    channelDataMenuItems: JSX.Element[];
    channelDataMenuItemsVisible: JSX.Element[];
}

export class Sidebar extends Component<SidebarProps, SidebarState> {
    private vehicleService: VehicleService;

    clickOnCollapse: () => void;
    clickOnToggle: () => void;
    callVehicleMetaData: (open: boolean) => void;
    callVehicleHistoricData: (open: boolean) => void;
    callOnSearch: (keyUpEvent: React.KeyboardEvent<HTMLInputElement>) => void;


    constructor(props: SidebarProps) {
        super(props);
        var vehicleDataMenuItems = [<MenuItem key='loaderChannelMenuItem'><Loader /></MenuItem>];
        var channelMenuItems = [<MenuItem key='loaderChannelMenuItem'><Loader /></MenuItem>];
        this.state = {
            isMenuCollapsed: props.menuCollapsed,
            isMenuToggled: props.menuToggled,

            vehicleDataMenuItems: vehicleDataMenuItems,
            vehicleDataMenuItemsVisible: vehicleDataMenuItems,

            channelDataMenuItems: channelMenuItems,
            channelDataMenuItemsVisible: channelMenuItems
        };

        this.vehicleService = VehicleService.getInstance();

        /** 
         * in react methods are state based that means attributes of the state is to be set over
         * React component state setter
         */
        this.clickOnCollapse = () => this.onCollapse();
        this.clickOnToggle = () => this.onToggle();
        this.callVehicleMetaData = (open: boolean) => {
            if (open) {
                this.vehicleService.getVehicleMetaData$()
                    .pipe(map(data => {
                        this.checkVehicleMetaDataMenuItems();
                        return data;
                    }))
                    .pipe(map(data => {
                        return data.sort((v1, v2) => 0 - (v1.dataDate > v2.dataDate ? 1 : -1));
                    }))
                    .pipe(concatAll())
                    .subscribe(vehicle => {
                        this.addVehicleDataMenuItem(vehicle);
                    }, error => {
                        console.error(error);
                    });
            }
        };

        var hasChannelData: Boolean = false;
        this.callVehicleHistoricData = (open: boolean) => {
            if (open && !hasChannelData && this.state.vehicleDataMenuItems.length > 1) {
                hasChannelData = true;
                this.state.vehicleDataMenuItems.forEach(element => {
                    this.handleChannelData(String(element.key));
                });
            }
        };

        this.callOnSearch = (keyUpEvent: React.KeyboardEvent<HTMLInputElement>) => {
            if (keyUpEvent.code === "Enter") {
                const input: string = keyUpEvent.currentTarget.value;
                this.onSearch(input);
            }
        };

        // run section
        this.callVehicleMetaData(true);
    }

    private checkVehicleMetaDataMenuItems(): void {
        // remove loader from menu items
        if (this.state.vehicleDataMenuItems.length === 1 && this.state.vehicleDataMenuItems[0].props.children.type === Loader) {
            this.state.vehicleDataMenuItems.length = 0;
        }
    }

    private handleChannelData(key: string) {
        const _this: Sidebar = this;
        this.vehicleService.getChannelMetaData$(key)
            .pipe((sequence) => {
                this.state.channelDataMenuItems.length = 0;
                return sequence;
            })
            .subscribe((channelData: ChannelMetaData[]) => {
                channelData.forEach(channel => this.addChannelDataMenuItem(channel));
            });
    }

    private onCollapse(): void {
        if (this.state.isMenuCollapsed) {
            this.setState({
                isMenuCollapsed: false,
                isMenuToggled: this.state.isMenuToggled
            });
        } else {
            this.setState({
                isMenuCollapsed: true,
                isMenuToggled: this.state.isMenuToggled
            });
        }
    }

    private addVehicleDataMenuItem(vehicle: VehicleMetaData) {
        const vname = vehicleName(vehicle);
        const element: JSX.Element =
            <VehicleMenuItem
                key={vehicle.key}
                _key={vehicle.key}
                dataDate={vehicle.dataDate}
                driveId={vehicle.driveId}
                vehicle={vehicle.vehicle}
                frames={vehicle.frames}
                startTime={vehicle.startTime}
                endTime={vehicle.endTime}
                tensorType={vehicle.tensorType}
                calibData={vehicle.calibData}
                camToCamCalibTime={vehicle.camToCamCalibTime}
                imuToVeloCalibTime={vehicle.imuToVeloCalibTime}
                veloToCamCalibTime={vehicle.veloToCamCalibTime}
            />;
        this.state.vehicleDataMenuItems.push(element);
        this.setState({
            vehicleDataMenuItems: this.state.vehicleDataMenuItems,
            vehicleDataMenuItemsVisible: this.state.vehicleDataMenuItems
        });
    }

    private addChannelDataMenuItem(channelData: ChannelMetaData) {
        const vname = vehicleName(channelData);
        const element: JSX.Element =
            <ChannelMenuItem
                key={channelData.key + '_' + channelData.targetType + "_" + channelData.option2 + "_" + channelData.option3}
                _key={channelData.key}
                dataDate={channelData.dataDate}
                driveId={channelData.driveId}
                vehicle={channelData.vehicle}
                frames={channelData.frames}
                startTime={channelData.startTime}
                endTime={channelData.endTime}
                tensorType={channelData.tensorType}
                option1={channelData.option1}
                option2={channelData.option2}
                option3={channelData.option3}
                frequency={channelData.frequency}
                device={channelData.device}
                targetType={channelData.targetType}
            />;
        var newState = this.state.channelDataMenuItems.concat(element);
        var newStateVisible = this.state.channelDataMenuItemsVisible.concat(element);
        this.setState({
            channelDataMenuItems: newState,
            channelDataMenuItemsVisible: newStateVisible
        });
    }

    private onToggle(): void {
        if (this.state.isMenuToggled) {
            this.setState({
                isMenuCollapsed: this.state.isMenuCollapsed,
                isMenuToggled: false
            });
        } else {
            this.setState({
                isMenuCollapsed: this.state.isMenuCollapsed,
                isMenuToggled: true
            });
        }
    }

    private onSearch(input: string): void {
        var commands = input.split(" ");
        var expressions = commands.map(command => {
            var pairs = command.split(":");
            var property = "driveId";
            var value = pairs[0];
            if (pairs.length > 1) {
                property = pairs[0];
                value = pairs[1];
            }
            return [property, value];
        });

        var newVehicleState = this.state.vehicleDataMenuItems.filter(elementParent => {
            var element = ((elementParent as any) as VehicleMenuItem);
            var properties = (element as any).props;
            var allow = true;
            expressions.forEach(expression => {
                if (!(properties[expression[0]].toString().indexOf(expression[1]) >= 0)) {
                    allow = false;
                }
            });
            return allow;
        });

        var newChannelState = this.state.channelDataMenuItems.filter(elementParent => {
            var element = ((elementParent as any) as ChannelMenuItem);
            var properties = (element as any).props;
            var allow = true;
            expressions.forEach(expression => {
                if (!(properties[expression[0]].toString().indexOf(expression[1]) >= 0)) {
                    allow = false;
                }
            });
            return allow;
        });

        this.setState({
            vehicleDataMenuItemsVisible: newVehicleState,
            channelDataMenuItemsVisible: newChannelState
        });
    }

    render(): JSX.Element {
        return (
            <ProSidebar
                className='left-side-bar'
                onToggle={this.clickOnToggle}
                toggled={this.state.isMenuToggled}
                collapsed={this.state.isMenuCollapsed}
                breakPoint="md">
                <SidebarHeader>
                    <div className="sidebar_header_logo">
                        <a href="http://www.bigdatasignalprocessing.com"><img width="48px" src={`${logo}`} alt="BDSP" />
                         Big Data Signal Processing</a>
                    </div>
                    <div className="close_menu_button_left" onClick={this.clickOnCollapse}>
                        {this.state.isMenuCollapsed ? <RiMenuUnfoldFill /> : <RiMenuFoldFill />}
                    </div>
                </SidebarHeader>
                <SidebarContent>
                    <Menu iconShape="square">
                        <SubMenu icon={<FaGlobe />} title="Tenants & Locations">
                            <ListGroup className='job_list'>
                                <ListGroup.Item key="Edge" className='job_list_item'>
                                    <span><FaHdd /> DBX Edge (MAPR/HDFS)</span>
                                </ListGroup.Item>
                                <ListGroup.Item key="MS Azure" className='job_list_item'>
                                    <span><FaHdd /> TSI Azure (DB/ADLS)</span>
                                </ListGroup.Item>
                                <ListGroup.Item key="Amazon EMS" className='job_list_item'>
                                    <span><FaHdd /> TSI AWS (EMR/S3)</span>
                                </ListGroup.Item>
                            </ListGroup>
                        </SubMenu>
                        <SubMenu icon={<FaCar />} title="Vehicles & Recordings" onOpenChange={this.callVehicleHistoricData}>
                            {this.state.vehicleDataMenuItemsVisible}
                        </SubMenu>
                        <SubMenu icon={<FaPause />} title="Devices & Channels" onOpenChange={this.callVehicleHistoricData}>
                            {this.state.channelDataMenuItemsVisible}
                        </SubMenu>
                        <SubMenu icon={<FaFastForward />} title="Live Streams (Offline)">{ }</SubMenu>
                    </Menu>
                </SidebarContent>
                <SidebarFooter>
                    <Menu iconShape="circle">
                        <MenuItem>
                            <SearchField
                                className='sidebar_search'
                                placeholder='Search...'
                                label='Search'
                                searchFunction={this.callOnSearch} />
                        </MenuItem>
                        <MenuItem icon={<FiHelpCircle />}>Info</MenuItem>
                    </Menu>
                </SidebarFooter>
            </ProSidebar>
        );
    }
}
