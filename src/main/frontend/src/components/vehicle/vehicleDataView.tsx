import { VehicleMetaData } from '../../model/vehicle.model';
import React, { Component } from 'react';
import { Table } from 'react-bootstrap';

export class VehicleDataView extends Component<VehicleMetaData> {
    constructor(props: VehicleMetaData) {
        super(props);
    }

    render(): JSX.Element {
        return (
            <Table striped bordered hover size='sm' variant='dark'>
                <thead>
                    <tr>
                        <th>Key</th>
                        <th>Vehicle</th>
                        <th>Drive</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>#Frames</th>
                        <th>Calibration</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{this.props._key}</td>
                        <td>{this.props.vehicle}</td>
                        <td>{this.props.driveId}</td>
                        <td>{this.props.startTime}</td>
                        <td>{this.props.endTime}</td>
                        <td>{this.props.frames}</td>
                        <td>{this.props.calibData}</td>
                    </tr>
                </tbody>
            </Table>
        );
    }

}
