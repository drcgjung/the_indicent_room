import { VehicleMetaData } from '../../model/vehicle.model';
import { ComponentService } from '../../services/componentService';
import { Headbar } from '../headerbar';
import { MainContent } from '../maincontent';
import React, { Component } from 'react';
import { FaCar } from 'react-icons/fa';
import {MenuItem } from 'react-pro-sidebar';

export interface ItemState {
    onclick: () => void;
}

export class VehicleMenuItem extends Component<VehicleMetaData, ItemState> {

    static BUSID = 'VehicleMenuItem';

    private msgBus: ComponentService;

    constructor(props: VehicleMetaData) {
        super(props);
        this.msgBus = ComponentService.getInstance();
        this.state = {
            onclick: () => {
                this.onClick();
            }
        };

    }

    private onClick(): void {
        this.msgBus.sendMessage({
            from: VehicleMenuItem.BUSID,
            to: MainContent.BUSID,
            data: this.props
        });
        this.msgBus.sendMessage({
            from: VehicleMenuItem.BUSID,
            to: Headbar.BUSID,
            data: false
        });
    }

    render(): JSX.Element {
        var hidden="vehicle_menu_item"
        return (
            <MenuItem key={this.key}>
            <div className={hidden} onClick={this.state.onclick}>
                <div>
                    <span><FaCar /> {this.props.driveId} {this.props.startTime.substring(0,this.props.startTime.length-5)}</span>
                    <br/>
                    <span>{this.props.vehicle} ({this.props.frames} Frames)</span>
                </div>
            </div>
            </MenuItem>
        );
    }

}
