import { ThreeJsLane } from './threejslane';
import { VehiclePointCloudData, VehicleVideoData, VideoPointCloudTuple } from '../../model/vehicle.model';
import { VehicleService } from '../../services/vehicleService';
import React, { Component } from 'react';
import { Observable, forkJoin } from 'rxjs';

export interface DataProperties {
    _key:string;
    imgType: string;
    imgSide: string;
    coloured: boolean;  
    times: number[];
    startTime: number;
    endTime: number;  
}

export interface DataState {
}

export class LanesDataView extends Component<DataProperties, DataState> {

    private loadData: (pos: number, length:number) => Observable<VideoPointCloudTuple>;
    private vehicleService: VehicleService;

    constructor(props: DataProperties) {
        super(props);
        this.vehicleService = VehicleService.getInstance();
        this.loadData = (pos: number, length:number) => {
            return forkJoin(
                this.vehicleService.getVehicleVideoData$(this.props._key,this.props.coloured, this.props.imgSide, pos, length),
                this.vehicleService.getVehiclePointCloudData$(this.props._key, pos, length)
            );
        };
    }

    render(): JSX.Element {
        var videoData= new Array<VehicleVideoData>(this.props.times.length);       
        var pcData= new Array<VehiclePointCloudData>(this.props.times.length);       

        return (
            <ThreeJsLane
            imgType={this.props.imgType}
            videoData={videoData}
            times={this.props.times}
            startTime={this.props.startTime}
            endTime={this.props.endTime}
            pointCloudData={pcData}
            loadData={this.loadData} />
        );
    }
}