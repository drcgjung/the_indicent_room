import { LabelBox, RenderLabelMessage } from '../../model/labeling.model';
import { VehiclePointCloudData, VehicleVideoData, VideoPointCloudTuple } from '../../model/vehicle.model';
import { ComponentService } from '../../services/componentService';
import { Label } from '../labels/label';
import { LabelType } from '../labels/labelType';
import { ThreeLoader } from '../loader';
import Controls from '../pointcloud/controls';
import { PointCloudSceneGroup } from '../pointcloud/pointCloudSceneGroup';
import { RangeChangedMessage, RangeSlider, RangeSliderMessage } from '../utils/rangeSlider';
import { VideoScene } from '../video/videoScene';
import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import {
    FaArrowLeft,
    FaArrowRight,
    FaBackward,
    FaForward,
    FaPlay,
    FaStop
    } from 'react-icons/fa';
import { Canvas } from 'react-three-fiber';
import { Observable } from 'rxjs';

export interface LaneProperties {
    imgType: string;
    videoData: VehicleVideoData[];
    pointCloudData: VehiclePointCloudData[];
    times: number[];
    startTime: number;
    endTime: number;
    loadData: (pos: number, length:Number) => Observable<VideoPointCloudTuple>;
}

export interface LaneState {
    start: number;
    pos: number;
    end: number;
    videoData: VehicleVideoData[];
    pointCloudData: VehiclePointCloudData[];
    loading: boolean;
    isPlaying: boolean;
    playHandle: number;
    renderMessages: RenderLabelMessage[];
    didMount: boolean;
}

export class ThreeJsLane extends Component<LaneProperties, LaneState> {

    static BUSID = 'ThreeJsLane';

    private msgBus: ComponentService;

    private goto: (idx: number) => void;
    private playPause: () => void;
    private toLeft: () => void;
    private toRight: () => void;
    private prevData: () => void;
    private nextData: () => void;

    constructor(props: LaneProperties) {
        super(props);
        this.msgBus = ComponentService.getInstance();
        var initialPos=0;
        while(this.props.times[initialPos]<this.props.startTime) {
            initialPos=initialPos+1;
        }
        var initialEnd=initialPos;
        while(this.props.times[initialEnd]<this.props.endTime) {
            initialEnd=initialEnd+1;
        }
        this.state = {
            start: initialPos,
            pos: initialPos,
            end: initialEnd,
            videoData: this.props.videoData,
            pointCloudData: this.props.pointCloudData,
            loading: true,
            isPlaying: false,
            playHandle: -1,
            renderMessages: [],
            didMount:false
        };

        this.setStateData(initialPos,initialEnd-initialPos);

        this.goto = (idx: number) => {
            this.setStateData(idx);
        };

        this.playPause = () => {
            if (this.state.isPlaying) {
                window.clearInterval(this.state.playHandle);
                this.setState({
                    playHandle: -1,
                    isPlaying: false,
                });
            } else {
                const playHandle = window.setInterval(this.toRight, 2000);
                this.setState({
                    playHandle: playHandle,
                    isPlaying: true
                });
            }
        };

        this.toLeft = () => {
            this.slideLeft(this.state.pos);
        };

        this.toRight = () => {
            this.slideRight(this.state.pos);
        };

        this.prevData = () => {
            var prevStart=Math.max(this.state.start-(this.state.end-this.state.start),0);
            var prevEnd=Math.min(prevStart+(this.state.end-this.state.start),this.props.videoData.length);
            this.setStateData(prevStart,(prevEnd-prevStart),prevStart,prevEnd);
        };

        this.nextData = () => {
            var nextEnd=Math.min(this.state.end+(this.state.end-this.state.start),this.props.videoData.length);
            var nextStart=Math.max(0,nextEnd-((this.state.end-this.state.start)));
            this.setStateData(nextStart,(nextEnd-nextStart),nextStart,nextEnd);
        };

        this.msgBus.getBusMessage(ThreeJsLane.BUSID).subscribe(msg => {
            if (msg.from === RangeSlider.BUSID && this.state.didMount) {
                const rangeChangeMsg: RangeChangedMessage = msg.data;
                var initialPos=0;
                while(this.props.times[initialPos]<rangeChangeMsg.start) {
                    initialPos=initialPos+1;
                }
                var initialEnd=initialPos;
                while(this.props.times[initialEnd]<rangeChangeMsg.end) {
                    initialEnd=initialEnd+1;
                }
                this.setStateData(initialPos,initialEnd-initialPos,initialPos,initialEnd,false);
            }  else if (msg.from === `${Label.BUSID}:${LabelType.OBJECT}`) {
                const renderMsg: RenderLabelMessage = msg.data;
                if (renderMsg === undefined) {
                    this.setState({
                        renderMessages: []
                    });
                } else {
                    this.setState({
                        renderMessages: this.state.renderMessages.concat([renderMsg])
                    });
                }
            }
        });
    }

    public componentDidMount() {
        this.setState({didMount:true});
    }

    private slideLeft(currIdx: number) {
        if(currIdx>this.state.start) {
            this.setStateData(currIdx-1);
        } else {
            this.setStateData(this.state.end);
        }
    }

    private slideRight(currIdx: number) {
        if(currIdx<this.state.end-1) {
            this.setStateData(currIdx+1);
        } else {
            this.setStateData(this.state.start);
        }
    }

    private setStateData(newPos: number, length:number = 1, start:number = -1, end:number = -1, updateSlider:boolean = true): void {
        if(start<0) {
            start=this.state.start;
        }
        if(end<0) {
            end=this.state.end;
        }
        if(newPos>=start) {
            var firstUndefinedFrame=newPos;
            length=Math.min(length,end-newPos);
            while((this.state.videoData[firstUndefinedFrame]!=null && this.state.pointCloudData[firstUndefinedFrame]!=null) && firstUndefinedFrame<newPos+length) {
                firstUndefinedFrame=firstUndefinedFrame+1;
            }
            length=Math.min(length,newPos+length-firstUndefinedFrame);
            if(length>0) {
                if(this.state.didMount) {
                    this.setState({
                        loading: true,
                    });
                }            
                this.props.loadData(firstUndefinedFrame,length).subscribe((tuple: VideoPointCloudTuple) => {
                    console.log(tuple);
                    for(let i=0;i<tuple[0].length;i++) {
                        this.state.videoData[firstUndefinedFrame+i]=tuple[0][i];
                    }
                    for(let i=0;i<tuple[1].length;i++) {
                        this.state.pointCloudData[firstUndefinedFrame+i]=tuple[1][i];
                    }
                    if(this.state.didMount) {
                        this.setState({
                            start:start,
                            end:end,
                            pos: newPos,
                            pointCloudData: this.state.pointCloudData,
                            videoData: this.state.videoData,
                            loading: false
                        });
                    }
                });
            } else {
  
              if(newPos!=this.state.pos && this.state.didMount) {
                 this.setState({start:start,end:end,pos:newPos});
              }            
  
            }
  
           if(updateSlider && (start!=this.state.start || end!=this.state.end)) {
             this.msgBus.sendMessage({
              from: ThreeJsLane.BUSID,
              to: RangeSlider.BUSID,
              data: {
                  start: this.props.times[start],
                  end: this.props.times[end],
                  receiverBusId: ThreeJsLane.BUSID
               } as RangeSliderMessage
            });
         }
       }
      }
  
    private getCurrentPointCloud(): String {
        if(this.state.pointCloudData[this.state.pos]==null) {
            return "";
        } else {
            var data=this.state.pointCloudData[this.state.pos].pointCloudData!!
            return data.substring(1,data.length-1);
        }
    }

    private getCurrentImageUrl(): string {
        if(this.state.videoData[this.state.pos]==null) {
            return "https://www.butenunbinnen.de/bilder/testbild-radio-bremen-fernsehen-108~_v-512x288_c-1594546688278.jpg"
        } else {
            return imageUrl("png", this.state.videoData[this.state.pos].imgData);
        }
    }

    private generateButtons(): JSX.Element[] {
        var result=new Array<JSX.Element>();
        for(let idx = this.state.start; idx < this.state.end; idx++) {
            result.push(<button
                key={idx}
                className={idx === this.state.pos ? 'dot active' : 'dot'}
                onClick={() => this.goto(idx)}>
            </button>);
        }
        return result; 
    }

    render(): JSX.Element {
        const videoHeight = 300;
        const pointCloudHeight = 300;
        return (
            <div className='lanes_viewer'>
                <div className='canvas_container'>
                    {
                        this.state.loading ?
                            <ThreeLoader show={this.state.loading} height={videoHeight + pointCloudHeight} /> :
                            <>
                                <Canvas style={{ height: videoHeight }}>
                                    <color attach='background' args={[0, 0, 0]} />
                                    <Controls />
                                    <VideoScene
                                        index={this.state.pos}
                                        videoData={this.state.videoData}
                                        labelBoxes={this.state.renderMessages.map(rm => {
                                            return {
                                                title: rm.title,
                                                color: rm.color,
                                                position: rm.video.position,
                                                size: rm.video.size
                                            } as LabelBox;
                                        })} />
                                </Canvas>
                                <Canvas style={{ height: pointCloudHeight }}>
                                    <Controls />
                                    <color attach='background' args={[0, 0, 0]} />
                                    <PointCloudSceneGroup
                                        index={this.state.pos}
                                        pointCloudData={this.state.pointCloudData}
                                        labelBoxes={this.state.renderMessages.map(rm => {
                                            return {
                                                title: rm.title,
                                                color: rm.color,
                                                position: rm.pointCloud.position,
                                                size: rm.pointCloud.size
                                            } as LabelBox;
                                        })} />
                                </Canvas>
                            </>
                    }
                </div>
                <div className='lanes_viewer_controls'>
                    <div className='left_controls'>
                        <div className='play_button'>
                            <Button variant='primary' size='sm' onClick={this.playPause}>
                                {this.state.isPlaying ? <FaStop /> : <FaPlay />}
                            </Button>
                        </div>
                    </div>
                    <div className='center_controls'>
                        <div className='left_buttons'>
                            <Button variant='primary' size='sm' onClick={this.prevData}>
                                <FaBackward />
                            </Button>
                            <Button variant='primary' size='sm' onClick={this.toLeft}>
                                <FaArrowLeft />
                            </Button>
                        </div>
                        <div className='indicator'>
                            {this.generateButtons()}
                        </div>
                        <div className='right_buttons'>
                            <Button variant='primary' size='sm' onClick={this.toRight}>
                                <FaArrowRight />
                            </Button>
                            <Button variant='primary' size='sm' onClick={this.nextData}>
                                <FaForward />
                            </Button>
                        </div>
                    </div>
                    <div className='right_controls'>

                    </div>
                </div>
            </div>
        );
    }
}

export function imageUrl(imgType: string, img: string): string {
    const url = `data:image/${imgType.toLowerCase()};base64,${img.substring(1,img.length-1)}`;
    return url;
}
