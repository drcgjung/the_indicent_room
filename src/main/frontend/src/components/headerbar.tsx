import { ComponentService } from '../services/componentService';
import React from 'react';
import { Component } from 'react';
import { Button, Jumbotron } from 'react-bootstrap';

export interface HeadProperties {
    title: string;
}

export interface HeadState {
    isSmaller: boolean;
}

export class Headbar extends Component<HeadProperties, HeadState> {

    static BUSID = 'Headbar';

    private msgBus: ComponentService;

    constructor(props: HeadProperties) {
        super(props);
        this.msgBus = ComponentService.getInstance();
        this.state = {
            isSmaller: false
        };
        this.msgBus.getBusMessage(Headbar.BUSID).subscribe(msg => {
            const smaller: boolean = msg.data;
            this.setState({
                isSmaller: smaller
            });
        });
    }

    render(): JSX.Element {
        return (
            <Jumbotron className={this.state.isSmaller ? 'headbar_jumbotron_smaller' : 'headbar_jumbotron'}>
                <h1>{this.props.title}</h1>
                <p>Distributed Tensor Storage &amp; Processing Engine.</p>
                <p>(c) 2017-2021 T-Systems International GmbH</p>
                <Button variant='primary' href='http://localhost:4040' target='_blank'>Federated Spark</Button>
                <Button variant='primary' href='http://www.cvlibs.net/datasets/kitti/' target='_blank'>KITTI Benchmark</Button>
                <Button variant='primary' href='http://carla.org/' target='_blank'>CARLA Simulation</Button>
            </Jumbotron>
        );
    }
}
