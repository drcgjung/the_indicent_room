import { ComponentService } from '../../services/componentService';
import React, { Component } from 'react';
import { filter } from 'rxjs/operators';

export interface FooterProperties {
    height?: number;
    children: JSX.Element | JSX.Element[];
}

export interface FooterState extends FooterProperties {
    height: number;
    show: boolean;
}

export interface FooterMessage {
    show: boolean;
    timeSlider: {
        intervalStart: number,
        intervalEnd: number,
    };
}

export class MainViewFooter extends Component<FooterProperties, FooterState> {

    static BUSID = 'MainViewFooter';

    private msgBus: ComponentService;

    constructor(props: FooterProperties) {
        super(props);
        this.msgBus = ComponentService.getInstance();
        this.state = {
            height: this.props.height || -1,
            children: this.props.children,
            show: false,
        };

        this.msgBus.getBusMessage(MainViewFooter.BUSID)
            .subscribe(msg => {
                const footerMsg = msg.data as FooterMessage;
                this.setState({
                    show: footerMsg.show
                });
            });
    }

    render(): JSX.Element {
        const styleClass = 'main_view_footer';
        return (
            <div className={this.state.show ? styleClass : `${styleClass} ${styleClass}_hidden`} style={{ height: this.state.height < 0 ? '' : this.state.height }}>
                {this.state.children}
            </div>
        );
    }
}