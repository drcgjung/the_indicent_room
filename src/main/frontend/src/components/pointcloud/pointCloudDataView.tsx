import { PointCloudViewer } from './pointCloudViewer';
import { VehiclePointCloudData } from '../../model/vehicle.model';
import { VehicleService } from '../../services/vehicleService';
import React, { Component } from 'react';
import { Observable } from 'rxjs';

export interface DataProperties {
    _key:string;
    times: number[];
    startTime: number;
    endTime: number;
}

export interface DataState {
}

export class PointCloudDataView extends Component<DataProperties, DataState> {

    private loadData: (pos: number, length:number) => Observable<VehiclePointCloudData[]>;
    private vehicleService: VehicleService;

    constructor(props: DataProperties) {
        super(props);
        this.vehicleService = VehicleService.getInstance();
        this.loadData = (pos: number, length:number) => {
            const dataKey: string = this.props._key;
            return this.vehicleService.getVehiclePointCloudData$(dataKey, pos, length);
        };
    }

    render(): JSX.Element {
        var pcData= new Array<PointCloudData>(this.props.times.length);       

        return (
            <PointCloudViewer
                pointCloudData={pcData}
                loadData={this.loadData}
                times={this.props.times}
                startTime={this.props.startTime}
                endTime={this.props.endTime}                
            />
        );
    }
}