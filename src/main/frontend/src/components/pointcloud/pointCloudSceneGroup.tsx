import { PointCloudScene } from './pointCloudScene';
import { LabelBox } from '../../model/labeling.model';
import { VehiclePointCloudData } from '../../model/vehicle.model';
import React, { useMemo } from 'react';
/* eslint-disable @typescript-eslint/no-extra-non-null-assertion */
/* eslint-disable @typescript-eslint/no-non-null-assertion */

interface GroupProperties {
    index: number;
    pointCloudData: VehiclePointCloudData[];
    labelBoxes: LabelBox[];
}

interface PointCloudTimestamp {
    rawData: string | null;
    timestamp: Date | null
}

export const PointCloudSceneGroup: React.FC<GroupProperties> = ({ index, pointCloudData, labelBoxes }) => {

    const pointCloudScenes: JSX.Element[] = useMemo(() => {
            
            var result=new Array<PointCloudTimestamp>();
        
            for(let i=0;i<pointCloudData.length;i++) {
                if(pointCloudData[i]!=null) {
                    var data=pointCloudData[i].pointCloudData!!
                result.push(
                    {
                        rawData: data.substring(1,data.length-1),
                        timestamp: pointCloudData[i].startTime
                    }
                )    
                }
            }
        return result.map(pct => <PointCloudScene rawData={pct.rawData} labelBoxes={labelBoxes} />);
    }, [pointCloudData, labelBoxes]);

    return (
        <group>{pointCloudScenes[index]}</group>
    );
};