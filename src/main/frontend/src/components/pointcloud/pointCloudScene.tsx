import { LabelBox } from '../../model/labeling.model';
import { SETTINGS, toHexColor } from '../../model/settings';
import { useEventListener } from '../utils/react';
import { createTextSprite } from '../utils/textSprite';
import { inflate } from 'pako';
import React, { useMemo } from 'react';
import { useFrame, useUpdate } from 'react-three-fiber';
import {
    Box3,
    Box3Helper,
    BufferGeometry,
    Color,
    Float32BufferAttribute,
    Group,
    Vector3
    } from 'three';
/* eslint-disable @typescript-eslint/no-extra-non-null-assertion */
/* eslint-disable @typescript-eslint/no-non-null-assertion */

interface SceneProperties {
    rawData: string;
    labelBoxes: LabelBox[];
    xPos?: number;
}

export interface Vector {
    x: number;
    y: number;
    z: number;
    r: number;
}

export interface VectorAggs {
    x: number[],
    y: number[];
    z: number[];
}

type VectorColor = [Vector, Color];

export const PointCloudScene: React.FC<SceneProperties> = ({ rawData, labelBoxes, xPos }) => {

    const defaultColor = new Color();
    defaultColor.setHex(toHexColor(SETTINGS.colors.magenta));
    const chunkSize = 4;

    const mapPointCloudData = (aggs: VectorAggs, rawCompressed?: string) => {
        const vectorColors: VectorColor[] = [];
        const buffer = Uint8Array.from(atob(rawCompressed!!), c => c.charCodeAt(0));
        const inflated: Uint8Array = inflate(buffer);
        const coords = new Float32Array(inflated.buffer);


        for (let idx = 0; idx < coords.length; idx += chunkSize) {
            // vertices
            const vector: Vector = {
                x: coords[idx],
                y: coords[idx + 1],
                z: coords[idx + 2],
                r: coords[idx + 3]
            };

            // colors
            vectorColors.push([vector, defaultColor]);

            // aggregates
            if (idx == 0) {
                aggs['x'][0] = vector.x;
                aggs['x'][1] = vector.x;
                aggs['y'][0] = vector.y;
                aggs['y'][1] = vector.y;
                aggs['z'][0] = vector.z;
                aggs['z'][1] = vector.z;
            } else {
                aggs['x'][0] = Math.min(aggs['x'][0], vector.x);
                aggs['x'][1] = Math.max(aggs['x'][1], vector.x);
                aggs['y'][0] = Math.min(aggs['x'][0], vector.y);
                aggs['y'][1] = Math.max(aggs['x'][1], vector.y);
                aggs['z'][0] = Math.min(aggs['x'][0], vector.z);
                aggs['z'][1] = Math.max(aggs['x'][1], vector.z);
            }
        }

        return vectorColors;
    };

    const aggs: VectorAggs = {
        x: [0, 0],
        y: [0, 0],
        z: [0, 0]
    };

    // if raw data gets updated recalculate vertices etc.
    const [vertices, colors, boxes] = useMemo(() => {
        const vectorColors: VectorColor[] = mapPointCloudData(aggs, rawData);
        const positions: number[] = vectorColors.map(vc => vc[0]).flatMap(vec => [vec.x, vec.y, vec.z]);
        const colors: number[] = vectorColors.map(vc => vc[1]).flatMap(col => [col.r, col.g, col.b]);
        const boxes = labelBoxes ? labelBoxes : [];
        return [new Float32Array(positions), new Float32Array(colors), boxes];
    }, [rawData, labelBoxes]);

    // geometry gets updated vertices and colors
    const geomRef = useUpdate((geometry: BufferGeometry) => {
        geometry.setAttribute('position', new Float32BufferAttribute(vertices, 3));
        geometry.attributes.position.needsUpdate = true;
        geometry.setAttribute('color', new Float32BufferAttribute(colors, 3));
        geometry.attributes.color.needsUpdate = true;
    }, [vertices, colors]);

    const boxGroupRef = useUpdate((boxGroup: Group) => {
        if (boxes.length === 0) {
            boxGroup.clear();
        } else {
            boxes.flatMap(lBox => {
                const box3 = new Box3();
                const [x, y, z] = lBox.position;
                const [w, h, l] = lBox.size;
                const color = new Color(lBox.color);
                box3.setFromCenterAndSize(new Vector3(x, y, z), new Vector3(w, h, l));
                const labelText = createTextSprite(lBox.title, `#${color.getHexString()}`, 5, [w, h], [x, y, z + 1]);
                return [new Box3Helper(box3, color), labelText];
            }).forEach(labelItem => {
                boxGroup.add(labelItem);
            });
        }
    }, [boxes]);

    const posVector = new Vector3(xPos);

    const keyPressed: Record<string, number> = {};

    useEventListener('keydown', (e: KeyboardEvent) => {
        if (!keyPressed[e.key]) {
            keyPressed[e.key] = new Date().getTime();
        }
    });

    useEventListener('keyup', (e: KeyboardEvent) => {
        delete keyPressed[e.key];
    });

    const speed = 0.1, radians = 0.025;

    useFrame(({ camera }) => {
        Object.entries(keyPressed).forEach(e => {
            const [key, time] = e;
            switch (key) {
                case 'w':
                    // look up
                    camera.rotateX(radians);
                    break;
                case 's':
                    // look down
                    camera.rotateX(-radians);
                    break;
                case 'd':
                    // look  right
                    camera.rotateY(-radians);
                    break;
                case 'a':
                    // look left
                    camera.rotateY(radians);
                    break;
                case 'q':
                    // roll left
                    camera.rotateZ(radians);
                    break;
                case 'e':
                    // roll right
                    camera.rotateZ(-radians);
                    break;
                case 'ArrowUp':
                    // move forward
                    camera.translateZ(-speed);
                    break;
                case 'ArrowDown':
                    // move backward
                    camera.translateZ(speed);
                    break;
                case 'ArrowLeft':
                    // move left
                    camera.translateX(-speed);
                    break;
                case 'ArrowRight':
                    // move right
                    camera.translateX(speed);
                    break;
                case 'y':
                    // move down
                    camera.translateY(-speed);
                    break;
                case 'x':
                    // move up
                    camera.translateY(speed);
                    break;
                case 'c':
                    camera.position.set(0, 0, 0);
                    camera.lookAt(0, 20, 0);
                    camera.updateMatrix();
                    camera.updateProjectionMatrix();
                    break;
                default: // do nothing
            }
        });
    });



    return (
        <mesh position={posVector}>
            <points>
                <bufferGeometry ref={geomRef} />
                <pointsMaterial attach='material' size={1} sizeAttenuation={false} vertexColors={true} />
                <group ref={boxGroupRef} />
            </points>
        </mesh>
    );
};