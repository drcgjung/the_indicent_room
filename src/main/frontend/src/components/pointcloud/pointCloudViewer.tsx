import Controls from './controls';
import { PointCloudScene } from './pointCloudScene';
import { VehiclePointCloudData } from '../../model/vehicle.model';
import { ComponentService } from '../../services/componentService';
import { ThreeLoader } from '../loader';
import { RangeChangedMessage, RangeSlider, RangeSliderMessage } from '../utils/rangeSlider';
import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import {
    FaArrowLeft,
    FaArrowRight,
    FaBackward,
    FaForward,
    FaPlay,
    FaStop
    } from 'react-icons/fa';
import { Canvas } from 'react-three-fiber';
import { Observable } from 'rxjs';

/* eslint-disable @typescript-eslint/no-extra-non-null-assertion */
/* eslint-disable @typescript-eslint/no-non-null-assertion */


export interface ViewerProperties {
    times: number[];
    startTime: number;
    endTime: number;
    pointCloudData: VehiclePointCloudData[];
    loadData: (pos: number, length:number) => Observable<VehiclePointCloudData[]>;
}

interface ViewerState {
    start: number;
    pos: number;
    end: number;
    pointCloudData: VehiclePointCloudData[];
    loading: boolean;
    isPlaying: boolean;
    playHandle: number;
    didMount:boolean;
}

export class PointCloudViewer extends Component<ViewerProperties, ViewerState> {

    static BUSID = 'PointCloudViewer';

    private goto: (idx: number) => void;
    private playPause: () => void;
    private toLeft: () => void;
    private toRight: () => void;
    private prevData: () => void;
    private nextData: () => void;

    private msgBus: ComponentService;

    constructor(props: ViewerProperties) {
        super(props);
        this.msgBus = ComponentService.getInstance();

        var initialPos=0;
        while(this.props.times[initialPos]<this.props.startTime) {
            initialPos=initialPos+1;
        }
        var initialEnd=initialPos;
        while(this.props.times[initialEnd]<this.props.endTime) {
            initialEnd=initialEnd+1;
        }

        this.state = {
            start: initialPos,
            pos: initialPos,
            end: initialEnd,
            pointCloudData: this.props.pointCloudData,
            loading: true,
            isPlaying: false,
            playHandle: -1,
            didMount:false
        };

        this.setStatePointClouds(initialPos,initialEnd-initialPos);

        this.goto = (idx: number) => {
            this.setStatePointClouds(idx);         
        };

        this.playPause = () => {
            if (this.state.isPlaying) {
                window.clearInterval(this.state.playHandle);
                this.setState({
                    playHandle: -1,
                    isPlaying: false,
                });
            } else {
                const playHandle = window.setInterval(this.toRight, 2000);
                this.setState({
                    playHandle: playHandle,
                    isPlaying: true
                });
            }
        };

        this.toLeft = () => {
            this.slideLeft(this.state.pos);
        };

        this.toRight = () => {
            this.slideRight(this.state.pos);
        };

        this.prevData = () => {
            var prevStart=Math.max(this.state.start-(this.state.end-this.state.start),0);
            var prevEnd=Math.min(prevStart+(this.state.end-this.state.start),this.props.pointCloudData.length);
            this.setStatePointClouds(prevStart,(prevEnd-prevStart),prevStart,prevEnd);
        };

        this.nextData = () => {
            var nextEnd=Math.min(this.state.end+(this.state.end-this.state.start),this.props.pointCloudData.length);
            var nextStart=Math.max(0,nextEnd-((this.state.end-this.state.start)));
            this.setStatePointClouds(nextStart,(nextEnd-nextStart),nextStart,nextEnd);
        };

        this.msgBus.getBusMessage(PointCloudViewer.BUSID).subscribe(msg => {
            if (msg.from === RangeSlider.BUSID && this.state.didMount) {
                const rangeChangeMsg: RangeChangedMessage = msg.data;
                var initialPos=0;
                while(this.props.times[initialPos]<rangeChangeMsg.start) {
                    initialPos=initialPos+1;
                }
                var initialEnd=initialPos;
                while(this.props.times[initialEnd]<rangeChangeMsg.end) {
                    initialEnd=initialEnd+1;
                }
                this.setStatePointClouds(initialPos,initialEnd-initialPos,initialPos,initialEnd,false);
            } 
        });
    }

    public componentDidMount():void {
        this.setState( {
            didMount:true
        });
    }

    private slideLeft(currIdx: number): void {
        if(currIdx>this.state.start) {
            this.setStatePointClouds(currIdx-1);
        } else {
            this.setStatePointClouds(this.state.end);
        }
    }

    private slideRight(currIdx: number): void {
        if(currIdx<this.state.end-1) {
            this.setStatePointClouds(currIdx+1);
        } else {
            this.setStatePointClouds(this.state.start);
        }
    }

    private setStatePointClouds(newPos: number, length:number = 1, start:number = -1, end:number = -1, updateSlider:boolean = true): void {
        if(start<0) {
            start=this.state.start;
        }
        if(end<0) {
            end=this.state.end;
        }
        if(newPos>=start) {
            var firstUndefinedFrame=newPos;
            length=Math.min(length,end-newPos);
            while(this.state.pointCloudData[firstUndefinedFrame]!=null && firstUndefinedFrame<newPos+length) {
                firstUndefinedFrame=firstUndefinedFrame+1;
            }
            length=Math.min(length,newPos+length-firstUndefinedFrame);
            if(length>0) {
                if(this.state.didMount) {
                    this.setState({
                        loading: true,
                    });
                }
                this.props.loadData(firstUndefinedFrame,length).subscribe((data: VehiclePointCloudData[]) => {

                    for(let i=0;i<data.length;i++) {
                        this.state.pointCloudData[firstUndefinedFrame+i]=data[i];
                    }

                    if(this.state.didMount) {
                        this.setState({
                            start:start,
                            end:end,
                            pos: newPos,
                            pointCloudData: this.state.pointCloudData,
                            loading: false,
                        });
                    }
                });

            } else {

                if(newPos!=this.state.pos && this.state.didMount) {
                   this.setState({start:start,end:end,pos:newPos});
                }            
    
              }
    
             if(updateSlider && (start!=this.state.start || end!=this.state.end)) {
               this.msgBus.sendMessage({
                from: PointCloudViewer.BUSID,
                to: RangeSlider.BUSID,
                data: {
                    start: this.props.times[start],
                    end: this.props.times[end],
                    receiverBusId: PointCloudViewer.BUSID
                 } as RangeSliderMessage
              });
           }
         }
        }
    
    private statePointCloud(idx: number): string {
        if(this.state.pointCloudData[idx]==null) {
            return "";
        } else {
            var pcd=this.state.pointCloudData[idx].pointCloudData!!;
            return pcd.substring(1,pcd.length-1);
        }
    }

    private generateButtons(): JSX.Element[] {
        var result=new Array<JSX.Element>();
        for(let idx = this.state.start; idx < this.state.end; idx++) {
            result.push(<button
                key={idx}
                className={idx === this.state.pos ? 'dot active' : 'dot'}
                onClick={() => this.goto(idx)}>
            </button>);
        }
        return result; 
    }


    render(): JSX.Element {
        const containerHeight = 500;
        return (
            <div className='point_cloud_viewer'>
                <div className='canvas_container'>
                    {
                        this.state.loading ?
                            <ThreeLoader show={this.state.loading} height={containerHeight} /> :
                            <Canvas style={{ height: containerHeight }}>
                                <color attach='background' args={[0, 0, 0]} />
                                <Controls />
                                <PointCloudScene labelBoxes={[]} rawData={this.statePointCloud(this.state.pos)} />
                            </Canvas>
                    }
                </div>
                <div className='point_cloud_viewer_controls'>
                    <div className='left_controls'>
                        <div className='play_button'>
                            <Button variant='primary' size='sm' onClick={this.playPause}>
                                {this.state.isPlaying ? <FaStop /> : <FaPlay />}
                            </Button>
                        </div>
                    </div>
                    <div className='center_controls'>
                        <div className='left-buttons'>
                            <Button variant='primary' size='sm' onClick={this.prevData}>
                                <FaBackward />
                            </Button>
                            <Button variant='primary' size='sm' onClick={this.toLeft}>
                                <FaArrowLeft />
                            </Button>
                        </div>
                        <div className='indicator'>
                            {this.generateButtons()}
                        </div>
                        <div className='right-buttons'>
                            <Button variant='primary' size='sm' onClick={this.toRight}>
                                <FaArrowRight />
                            </Button>
                            <Button variant='primary' size='sm' onClick={this.nextData}>
                                <FaForward />
                            </Button>
                        </div>
                    </div>
                    <div className='right_controls'>

                    </div>
                </div>
            </div>
        );
    }
}
