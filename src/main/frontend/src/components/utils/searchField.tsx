import React, { Component } from 'react';
import {
    Button,
    Col,
    FormControl,
    InputGroup,
    Row
    } from 'react-bootstrap';
import { FaSearch } from 'react-icons/fa';

export interface SearchProperties {
    className?: string,
    placeholder: string,
    label: string,
    searchFunction: (ev: React.KeyboardEvent<HTMLInputElement>) => void;
}

export interface SearchState extends SearchProperties {
    results: unknown[];
}

export class SearchField extends Component<SearchProperties, SearchState> {
    constructor(props: SearchProperties) {
        super(props);
        this.state = {
            className: this.props.className,
            placeholder: this.props.placeholder,
            label: this.props.label,
            searchFunction: this.props.searchFunction,
            results: []
        };
    }

    private getClassName(): string {
        const searchFieldClassName = 'search_field_container';
        return this.state.className ?
            `${searchFieldClassName} ${this.state.className}` :
            searchFieldClassName;
    }

    render(): JSX.Element {
        return (
            <div className={this.getClassName()}>
                <Row>
                    <Col xs='auto'>
                        <InputGroup size='sm'>
                            <FormControl
                                placeholder={this.state.placeholder}
                                aria-label={this.state.label}
                                aria-describedby='search_icon' onKeyUp={this.state.searchFunction} />
                            <InputGroup.Append>
                                <Button variant='secondary'><FaSearch /></Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                </Row>
            </div>
        );
    }
}