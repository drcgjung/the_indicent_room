/**
 * Formate numeric or date as locale base string value
 * @param date value as epoch millis
 * @param locale id like de-DE or en-US etc.
 * @returns {String}
 */
export function dateFormat(n: number, locale?: string): string {
    const date = new Date(n);

    const year = date.getFullYear().toString();
    const month = (date.getMonth() + 1).toString(); // starts with 0
    const day = date.getDate().toString(); // getDate is month day
    //
    const hours = date.getUTCHours().toString();
    const minutes = date.getUTCMinutes().toString();
    const seconds = date.getUTCSeconds().toString();
    const millis = date.getMilliseconds().toString();

    const lz = (str: string, n: number) => str.padStart(n, '0'); // leading zeros

    const datePart = `${year}-${lz(month, 2)}-${lz(day, 2)}`;
    const timePart = `${lz(hours, 2)}:${lz(minutes, 2)}:${lz(seconds, 2)}.${lz(millis, 3)}`;

    return `${timePart}\n${datePart}`;
}