import TextTexture from '@seregpie/three.text-texture';
import { Sprite, SpriteMaterial } from 'three';

export function createTextSprite(text: string, color: string, size: number, scale: number[], position: number[]): Sprite {
    const texture = new TextTexture({
        alignment: 'center',
        color: color,
        fontFamily: 'sans-serif',
        fontSize: size,
        text: text
    });
    texture.pixelRatio = 5;
    const textMaterial = new SpriteMaterial({ map: texture });
    const sprite = new Sprite(textMaterial);
    texture.redraw();
    const [sx, sy] = scale;
    sprite.scale.x = sx;
    sprite.scale.y = sy;
    const [px, py, pz] = position;
    sprite.position.x = px;
    sprite.position.y = py;
    sprite.position.z = pz;

    return sprite;
}