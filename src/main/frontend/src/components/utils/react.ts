import { MutableRefObject, useEffect, useRef } from 'react';

export type EventConsumer = (ev: Event) => void;

export function useEventListener(eventName: string, handler: EventConsumer, element = window): void {
    // store event handler ref
    const savedHandler = useRef<EventConsumer>() as MutableRefObject<EventConsumer>;

    useEffect(() => {
        // use event handler for stored ref
        savedHandler.current = handler;
    }, [handler]);

    useEffect(() => {
        // check if element accepts event handling
        const isSupported = element && element.addEventListener;
        if (!isSupported) return;

        // create event listener from stored ref
        const eventListener: EventListener = (event: Event) => savedHandler.current(event);
        element.addEventListener(eventName, eventListener);

        return () => {
            // clean event listener
            element.removeEventListener(eventName, eventListener);
        };
    }, [eventName, element]);
}