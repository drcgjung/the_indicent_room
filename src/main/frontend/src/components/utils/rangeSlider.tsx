import { dateFormat } from './formatUtil';
import { ComponentService } from '../../services/componentService';
import noUiSlider from 'nouislider';
import React, { Component } from 'react';

export enum FormatType {
    DATE = 0
}

export interface RangeProperties {
    className?: string;
    startTotal: number;
    endTotal: number;
    format?: FormatType;
    withPips?: number;
}

export interface RangeState {
    slider: noUiSlider.noUiSlider;
    start: number;
    startTotal: number;
    end: number;
    endTotal: number;
    values: number[];
    withPips: boolean;
    receiverBusId?: string;
}

export interface RangeSliderMessage {
    start:number;
    end:number;
    pips: number[] | null;
    receiverBusId: string;
}

export interface RangeChangedMessage {
    start: number;
    end: number;
}

export class RangeSlider extends Component<RangeProperties, RangeState> {

    static BUSID = 'RangeSlider';

    private msgBus: ComponentService;
    private startProvider: () => number;
    private endProvider: () => number;

    constructor(props: RangeProperties) {
        super(props);
        this.msgBus = ComponentService.getInstance();
        this.startProvider = () => this.props.startTotal;
        this.endProvider = () => this.props.endTotal;
        
        var pips=new Array<number>();
        if(this.props.withPips!=null && this.props.withPips!!>0) {
            var init=this.props.startTotal;
            while(init<=this.props.endTotal) {
                pips.push(init);
                init=init+this.props.withPips!!
            }
            if(pips[pips.length-1]<this.props.endTotal) {
                pips.push(this.props.endTotal)
            }
        }
        this.state = {
            slider: {} as noUiSlider.noUiSlider,
            start: this.props.startTotal,
            startTotal: this.props.startTotal,
            end: this.props.endTotal,
            endTotal: this.props.endTotal,
            values: pips,
            withPips: this.props.withPips!=null && this.props.withPips!!>0,
            receiverBusId: undefined
        };

        this.msgBus.getBusMessage(RangeSlider.BUSID)
            .subscribe(msg => {
                const sliderMsg = msg.data as RangeSliderMessage;
                var stot=this.state.startTotal;
                if(sliderMsg.pips!=null) {
                    stot=sliderMsg.pips!![0]
                }
                var etot=this.state.endTotal;
                if(sliderMsg.pips!=null) {
                    etot=sliderMsg.pips!![sliderMsg.pips!!.length-1];
                }
                var pv=this.state.values;
                if(sliderMsg.pips!=null) {
                    pv=sliderMsg.pips!!;
                }

                this.setState({
                    start: sliderMsg.start,
                    startTotal: stot,
                    endTotal: etot,
                    end: sliderMsg.end,
                    values: pv,
                    receiverBusId: sliderMsg.receiverBusId,
                });
            });
    }

    componentDidMount(): void {
        this.noUiSliderCreate();
    }

    componentWillUnmount(): void {
        this.state.slider.off();
    }

    private noUiSliderCreate(): void {
        const slider = document.getElementById('no_ui_slider') as HTMLElement;
        const _slider = noUiSlider.create(slider, {
            start: [this.state.start, this.state.end],
            connect: true,
            range: {
                'min': this.state.startTotal,
                'max': this.state.endTotal
            },
            pips: this.noUiSliderPips(),
        });

        _slider.on('set', (values, handle, encoded) => {
            if (handle === 0) {
                this.startProvider = () => parseFloat(_slider.get()[handle]);
            }
            if (handle === 1) {
                this.endProvider = () => parseFloat(_slider.get()[handle]);
            }
            this.setStartEnd(this.startProvider(), this.endProvider());
        });

        this.setState({
            slider: _slider
        });
    }

    private noUiSliderPips(): noUiSlider.PipsOptions | undefined {
        var pips=this.state.values;
        return !this.state.withPips ? undefined : {
            mode: 'values',
            values: pips,
            density: 1,
            format: {
                to: (n: number) => {
                    var count=0;
                    while(pips[count]<n && count<pips.length) {
                        count=count+1;
                    }
                    if(count%20==0 || count==pips.length-1) {
                        return dateFormat(n);
                    } else {
                        return "";
                    }
                }
            },
            stepped: true,
        };
    }

    componentDidUpdate(prevProps: RangeProperties, prevState: RangeState): void {
        this.noUiSliderUpdate();
    }

    private noUiSliderUpdate() {
        this.state.slider.updateOptions({
            start: [this.state.start, this.state.end],
            range: {
                'min': this.state.startTotal,
                'max': this.state.endTotal
            },
            pips: this.noUiSliderPips(),
        }, true);
    }

    private setStartEnd(start: number, end: number): void {
        if(this.state.start!=start || this.state.end!=end) {
            this.setState({
                start:start,
                end:end
            });
            if (this.state.receiverBusId) {
                this.msgBus.sendMessage({
                    to: this.state.receiverBusId,
                    from: RangeSlider.BUSID,
                    data: {
                        start: start,
                        end: end
                    } as RangeChangedMessage
                });
            }
        }
    }

    render(): JSX.Element {
        const styleClass = this.props.className ? `range_slider_container ${this.props.className}` : 'range_slider_container';
        return (
            <div className={styleClass}>
                <div id='no_ui_slider'></div>
            </div>
        );
    }
}