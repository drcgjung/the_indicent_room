import { ImageSlider } from './imageslider';
import { VehicleVideoData } from '../../model/vehicle.model';
import { VehicleService } from '../../services/vehicleService';
import React, { Component } from 'react';
import { Observable } from 'rxjs';

export interface DataProperties {
    _key:string
    imgType: string;
    imgSide: string;
    coloured: boolean;    
    times: number[];
    startTime: number;
    endTime: number;
}

export interface ViewState {
}

export class VideoDataView extends Component<DataProperties, ViewState> {

    private loadData: (pos: number, length:number) => Observable<VehicleVideoData[]>;
    private vehicleService: VehicleService;

    constructor(props: DataProperties) {
        super(props);
        this.vehicleService = VehicleService.getInstance();
        this.loadData = (pos: number, length:number) => {
            const dataKey = this.props._key;
            const coloured = this.props.coloured;
            const side = this.props.imgSide;
            return this.vehicleService.getVehicleVideoData$(dataKey, coloured, side, pos, length);
        };
    }

    render(): JSX.Element {
        var videoData= new Array<VehicleVideoData>(this.props.times.length);       
        return (
            <ImageSlider
                imgType={this.props.imgType}
                videoData={videoData}
                times={this.props.times}
                startTime={this.props.startTime}
                endTime={this.props.endTime}
                loadData={this.loadData} />
        );
    }
}
