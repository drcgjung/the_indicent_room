import { VehicleVideoData } from '../../model/vehicle.model';
import { ComponentService } from '../../services/componentService';
import { RangeChangedMessage, RangeSlider, RangeSliderMessage } from '../utils/rangeSlider';
import React, { Component } from 'react';
import { Button, Image } from 'react-bootstrap';
import {
    FaArrowLeft,
    FaArrowRight,
    FaPlay,
    FaStepBackward,
    FaStepForward,
    FaStop
    } from 'react-icons/fa';
import { Observable } from 'rxjs';
import { GiJigsawBox } from 'react-icons/gi';

export interface SliderProperties {
    imgType: string;
    videoData: VehicleVideoData[];
    times: number[];
    startTime: number;
    endTime: number;
    loadData: (pos: number, length:number) => Observable<VehicleVideoData[]>;
}

export interface SliderState {
    start: number;
    pos: number;
    end: number;
    videoData: VehicleVideoData[];
    loading: boolean;
    playHandle: number;
    isPlaying: boolean;
    didMount:boolean;
}

export class ImageSlider extends Component<SliderProperties, SliderState> {

    static BUSID = 'ImageSlider';

    private goto: (idx: number) => void;
    private playPause: () => void;
    private prevChunk: () => void;
    private toLeft: () => void;
    private toRight: () => void;
    private nextChunk: () => void;

    private msgBus: ComponentService;

    constructor(props: SliderProperties) {
        super(props);
        this.msgBus = ComponentService.getInstance();

        var initialPos=0;
        while(this.props.times[initialPos]<this.props.startTime) {
            initialPos=initialPos+1;
        }
        var initialEnd=initialPos;
        while(this.props.times[initialEnd]<this.props.endTime) {
            initialEnd=initialEnd+1;
        }
        this.state = {
            start: initialPos,
            pos: initialPos,
            end: initialEnd,
            videoData: this.props.videoData,
            loading: true,
            isPlaying: false,
            playHandle: -1,
            didMount:false
        };

        this.setStateImages(initialPos,initialEnd-initialPos);

        this.goto = (idx: number) => {
            this.setStateImages(idx);
        };

        this.playPause = () => {
            if (this.state.isPlaying) {
                window.clearInterval(this.state.playHandle);
                this.setState({
                    playHandle: -1,
                    isPlaying: false,
                });
            } else {
                const playHandle = window.setInterval(this.toRight, 1000);
                this.setState({
                    playHandle: playHandle,
                    isPlaying: true
                });
            }
        };

        this.prevChunk = () => {
            var prevStart=Math.max(this.state.start-(this.state.end-this.state.start),0);
            var prevEnd=Math.min(prevStart+(this.state.end-this.state.start),this.props.videoData.length);
            this.setStateImages(prevStart,(prevEnd-prevStart),prevStart,prevEnd);
        };
        this.toLeft = () => {
            this.slideLeft(this.state.pos);
        };
        this.toRight = () => {
            this.slideRight(this.state.pos);
        };
        this.nextChunk = () => {
            var nextEnd=Math.min(this.state.end+(this.state.end-this.state.start),this.props.videoData.length);
            var nextStart=Math.max(0,nextEnd-((this.state.end-this.state.start)));
            this.setStateImages(nextStart,(nextEnd-nextStart),nextStart,nextEnd);
        };

        this.msgBus.getBusMessage(ImageSlider.BUSID).subscribe(msg => {
            if (msg.from === RangeSlider.BUSID && this.state.didMount) {
                const rangeChangeMsg: RangeChangedMessage = msg.data;
                var initialPos=0;
                while(this.props.times[initialPos]<rangeChangeMsg.start) {
                    initialPos=initialPos+1;
                }
                var initialEnd=initialPos;
                while(this.props.times[initialEnd]<rangeChangeMsg.end) {
                    initialEnd=initialEnd+1;
                }
                this.setStateImages(initialPos,initialEnd-initialPos,initialPos,initialEnd,false);
            } 
        });
    }

    public componentDidMount():void {
        this.setState( {
            didMount:true
        });
    }

    private slideLeft(currIdx: number): void {
        if(currIdx>this.state.start) {
            this.setStateImages(currIdx-1);
        } else {
            this.setStateImages(this.state.end);
        }
    }

    private slideRight(currIdx: number): void {
        if(currIdx<this.state.end-1) {
            this.setStateImages(currIdx+1);
        } else {
            this.setStateImages(this.state.start);
        }
    }

    private getImageUrl(img: VehicleVideoData): string {
        if(img==null) {
            return "https://www.butenunbinnen.de/bilder/testbild-radio-bremen-fernsehen-108~_v-512x288_c-1594546688278.jpg"
        } else {
            return imageUrl("png", img.imgData);
        }
    }

    private setStateImages(newPos: number, length:number = 1, start:number = -1, end:number = -1, updateSlider:boolean = true) {
        if(start<0) {
            start=this.state.start;
        }
        if(end<0) {
            end=this.state.end;
        }
        if(newPos>=start) {
            var firstUndefinedFrame=newPos;
            length=Math.min(length,end-newPos);
            while(this.state.videoData[firstUndefinedFrame]!=null && firstUndefinedFrame<newPos+length) {
                firstUndefinedFrame=firstUndefinedFrame+1;
            }
            length=Math.min(length,newPos+length-firstUndefinedFrame);
            if(length>0) {
                if(this.state.didMount) {
                    this.setState({
                        loading: true,
                    });
                }
                this.props.loadData(firstUndefinedFrame,length).subscribe((data: VehicleVideoData[]) => {

                    for(let i=0;i<data.length;i++) {
                        this.state.videoData[firstUndefinedFrame+i]=data[i];
                    }
                    if(this.state.didMount) {
                        this.setState({
                            start:start,
                            end:end,
                            pos: newPos,
                            videoData: this.state.videoData,
                            loading: false,
                        });
                    }
            });
          } else {

            if(newPos!=this.state.pos && this.state.didMount) {
               this.setState({start:start,end:end,pos:newPos});
            }            

          }

         if(updateSlider && (start!=this.state.start || end!=this.state.end)) {
           this.msgBus.sendMessage({
            from: ImageSlider.BUSID,
            to: RangeSlider.BUSID,
            data: {
                start: this.props.times[start],
                end: this.props.times[end],
                receiverBusId: ImageSlider.BUSID
             } as RangeSliderMessage
          });
       }
     }
    }

    private generateButtons(): JSX.Element[] {
        var result=new Array<JSX.Element>();
        for(let idx = this.state.start; idx < this.state.end; idx++) {
            result.push(<button
                key={idx}
                className={idx === this.state.pos ? 'dot active' : 'dot'}
                onClick={() => this.goto(idx)}>
            </button>);
        }
        return result; 
    }

    render(): JSX.Element {
        return (
            <div className='image_slider'>
                <div className='image_container'>
                    <Image
                        className={this.state.loading ? 'loading_img' : 'loaded_img'}
                        src={this.getImageUrl(this.state.videoData[this.state.pos])}>
                    </Image>
                </div>
                <div className='image_slider_controls'>
                    <div className='left_controls'>
                        <div className='play_button'>
                            <Button variant='primary' size='sm' onClick={this.playPause}>
                                {this.state.isPlaying ? <FaStop /> : <FaPlay />}
                            </Button>
                        </div>
                    </div>
                    <div className='center_controls'>
                        <div className='left_buttons'>
                            <Button variant='primary' size='sm' onClick={this.prevChunk}><FaStepBackward /></Button>
                            <Button variant='primary' size='sm' onClick={this.toLeft}><FaArrowLeft /></Button>
                        </div>
                        <div className='indicator'>
                            {this.generateButtons()}
                        </div>
                        <div className='right_buttons'>
                            <Button variant='primary' size='sm' onClick={this.toRight}><FaArrowRight /></Button>
                            <Button variant='primary' size='sm' onClick={this.nextChunk}><FaStepForward /></Button>
                        </div>
                    </div>
                    <div className='right_controls'>

                    </div>
                </div>
            </div>
        );
    }
}

/**
 * Create image src url
 * @param imgType image extension like png jpg etc.
 * @param img image base 64 string
 * @returns url of image src data
 */
export function imageUrl(imgType: string, img: string): string {
    const url = `data:image/${imgType.toLowerCase()};base64,${img.substring(1,img.length-1)}`;
    return url;
}
