import { imageUrl } from './imageslider';
import { LabelBox } from '../../model/labeling.model';
import { SETTINGS } from '../../model/settings';
import { VehicleVideoData } from '../../model/vehicle.model';
import { createTextSprite } from '../utils/textSprite';
import TextTexture from '@seregpie/three.text-texture';
import React, { useMemo } from 'react';
import { useFrame, useUpdate } from 'react-three-fiber';
import {
    Box3,
    Box3Helper,
    Color,
    DoubleSide,
    Group,
    Mesh,
    Texture,
    Vector3
    } from 'three';
import { MeshBasicMaterial, PlaneGeometry } from 'three';
import { Sprite } from 'three';

interface SceneProperties {
    index: number;
    videoData: VehicleVideoData[];
    labelBoxes: LabelBox[];
}

export interface VideoTimestamp {
    image: HTMLImageElement;
    timestamp: Date | null
}

export const VideoScene: React.FC<SceneProperties> = ({ index, videoData, labelBoxes }) => {

    const [videoTimestamps, boxes] = useMemo(() => {
        const videoTimestamps: VideoTimestamp[] = videoData.map((data: VehicleVideoData) => {
            var url = "https://www.butenunbinnen.de/bilder/testbild-radio-bremen-fernsehen-108~_v-512x288_c-1594546688278.jpg";
            if(data !=null) {
                url=imageUrl(data.imgType, data.imgData);
            }
            const img = new Image();
            img.src = url;
            var timestamp = null;
            if(data!=null) {
                timestamp=data.startTime;
            }
            const videoTimestamp: VideoTimestamp = { image: img, timestamp: timestamp };
            return videoTimestamp;
        });
        const boxes: LabelBox[] = labelBoxes ? labelBoxes : [];
        return [videoTimestamps, boxes];
    }, [videoData, labelBoxes]);

    const updateImages = (group: Group, currIdx: number) => {
        group.clear();
        videoTimestamps.forEach((vt, idx) => {
            const img: HTMLImageElement = vt.image;
            // image texture
            img.onload = () => {
                const texture = new Texture(img);
                texture.needsUpdate = true;
                const geometry = new PlaneGeometry(1, 1);
                const material = new MeshBasicMaterial({ map: texture, side: DoubleSide });
                material.transparent = true;
                if (idx !== currIdx) {
                    material.opacity = 0.2;
                }
                const mesh = new Mesh(geometry, material);
                mesh.geometry = geometry;
                mesh.material = material;
                mesh.scale.y = img.height / img.width;
                mesh.position.x = idx;
                group.add(mesh);
            };
        });
    };

    const updateTextLabels = (group: Group, currIdx: number) => {
        group.clear();
        videoTimestamps.forEach((vt, idx) => {
            if(vt.timestamp!=null) {
                const localTime: string = vt.timestamp.toISOString();
                const textSprite: Sprite = createTextSprite(
                localTime,
                idx === currIdx ? SETTINGS.colors.magenta : SETTINGS.colors.text,
                10,
                [0.4, 0.08],
                [idx, -0.1, 0.1]
                );
                group.add(textSprite);
            }
        });
    };

    const setOpacity = (mesh: Mesh, opacity: number) => {
        const material: MeshBasicMaterial = mesh.material as MeshBasicMaterial;
        material.transparent = true;
        material.opacity = opacity;
    };

    const setText = (sprite: Sprite, color: string, style: string, weight: string) => {
        const text = sprite.material.map as TextTexture;
        text.color = color;
        text.fontStyle = style;
        text.fontWeight = weight;
        text.redraw();
    };

    const slide = (group: Group, textGroup: Group, idx: number, groupPos: number) => {
        const vec = new Vector3(groupPos, 0, 0);
        group.position.lerp(vec, 0.1); // smooth transition of pos. change
        textGroup.position.lerp(vec, 0.1);
        const notSelected = group.children.filter((_, i) => i !== idx);
        const selected = group.children.filter((_, i) => i === idx);
        notSelected.forEach(c => {
            setOpacity(c as Mesh, 0.2);
        });
        selected.forEach(c => {
            setOpacity(c as Mesh, 1.0);
        });
        const notSelectedText = textGroup.children.filter((_, i) => i !== idx);
        const selectedText = textGroup.children.filter((_, i) => i === idx);
        notSelectedText.forEach(sc => {
            setText(sc as Sprite, SETTINGS.colors.dark, 'italic', 'regular');
        });
        selectedText.forEach(sc => {
            setText(sc as Sprite, SETTINGS.colors.magenta, 'normal', 'bold');
        });
    };

    const imgGroupRef = useUpdate((group: Group) => {
        updateImages(group, index);
    }, [videoTimestamps]);

    const textGroupRef = useUpdate((group: Group) => {
        updateTextLabels(group, index);
    }, [videoTimestamps]);

    const boxGroupRef = useUpdate((group: Group) => {
        if (boxes.length === 0) {
            group.clear();
        } else {
            boxes.flatMap(lBox => {
                const box3 = new Box3();
                const [x, y] = lBox.position;
                const [w, h] = lBox.size;
                const z = 0.1;
                box3.setFromCenterAndSize(new Vector3(x, y, z), new Vector3(w, h, 0));
                const color = new Color(lBox.color);
                const labelText: Sprite = createTextSprite(lBox.title, `#${color.getHexString()}`, 5, [0.1, 0.04], [x, y + 0.05, z]);
                return [new Box3Helper(box3, color), labelText];
            }).forEach(labelItem => {
                group.add(labelItem);
            });
        }
    }, [boxes]);

    useFrame(({ camera }) => {
        camera.zoom = 20;
        camera.updateProjectionMatrix();
        if (imgGroupRef.current && textGroupRef.current) {
            const groupPos = index * -1; // move from right to left
            slide(imgGroupRef.current, textGroupRef.current, index, groupPos);
        }
    });

    return (
        <>
            <group ref={imgGroupRef} />
            <group ref={textGroupRef} />
            <group ref={boxGroupRef} />
        </>
    );
};