import styles from '../../assets/scss/styles.scss';
import { LabelType } from '../labels/labelType';
import { Chart, ChartData } from 'chart.js';
import React, { Component, createRef, RefObject } from 'react';


export interface JobResult {
    type: string;
    drives: string[];
    attributes: any[];
    startTime: number;
    endTime: number;
    labelInfo?: {
        title: string;
        type: LabelType;
        color: number;
        pointCloud: {
            position: number[];
            size: number[];
        },
        video: {
            position: number[],
            size: number[];
        };
    };
}

export interface JobStatus {
    jobName: string;
    jobType: string;
    jobProgress: number;
    jobResults: JobResult[];
}

export interface StatusProperties {
    loadJobStatus: () => JobStatus[];
}

export interface StatusState {
    jobs: JobStatus[];
}

export class JobStatusChart extends Component<StatusProperties, StatusState> {

    private canvasRef: RefObject<HTMLCanvasElement>;

    constructor(props: StatusProperties) {
        super(props);
        this.canvasRef = createRef();
        this.state = {
            jobs: this.props.loadJobStatus()
        };

        this.createChart();

    }

    componentDidMount(): void {
        this.createChart();
    }

    private createChart(): void {
        const barColor = styles.magentarColor || '#E20074';
        const textColor = styles.textColor || '#FEFEFE';
        if (this.canvasRef.current) {
            const canvas = this.canvasRef.current;
            const chartData: ChartData = {
                labels: this.state.jobs.map((_, idx) => `Job_${idx}`),
                datasets: [{
                    label: 'Job progress',
                    backgroundColor: barColor,
                    data: this.state.jobs.map(job => job.jobProgress)
                }]
            };
            const chart = new Chart(canvas, {
                type: 'horizontalBar',
                data: chartData,
                options: {
                    legend: {
                        display: false,
                        labels: {
                            fontColor: textColor
                        }
                    },

                    scales: {
                        xAxes: [{
                            ticks: {
                                fontColor: textColor
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                fontColor: textColor
                            }
                        }]
                    }
                }
            });
            chart.update();
        }
    }

    render(): JSX.Element {
        return (
            <div className='job_status_container'>
                <canvas
                    id='job_status_chart'
                    ref={this.canvasRef}>
                </canvas>
            </div>
        );
    }
}