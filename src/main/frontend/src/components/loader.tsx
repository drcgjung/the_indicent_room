import React, { Component } from 'react';
import { Fade } from 'react-bootstrap';
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

export class Loader extends Component {

    render(): JSX.Element {
        return (
            <div className="loader_container">
                <div className="loader"></div>
            </div>
        );
    }
}


export class ThreeLoader extends Component<{ show: boolean; height?: number; }, { show: boolean; height: number; }> {
    constructor(props: any) {
        super(props);
        this.state = {
            show: this.props.show,
            height: this.props.height || 500
        };
    }
    render(): JSX.Element {
        return (
            <div id='loading_screen' style={{ height: this.state.height }}>
                <Fade in={this.state.show}>
                    <div id='loader'></div>
                </Fade>
            </div>
        );
    }
}
