export enum LabelType {
    OBJECT = 'object',
    EVENT = 'event',
}