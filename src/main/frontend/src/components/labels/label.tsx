import { LabelType } from './labelType';
import { RenderLabelMessage } from '../../model/labeling.model';
import { SETTINGS, toHexColor } from '../../model/settings';
import { ComponentService } from '../../services/componentService';
import { ThreeJsLane } from '../lanes/threejslane';
import React, { Component } from 'react';
import { FaObjectUngroup, FaTag } from 'react-icons/fa';
import { HiLightBulb } from 'react-icons/hi';

export interface LabelProperties {
    title: string;
    type: LabelType;
    drive: string;
    vehicleType: string;
    startTime: number;
    endTime: number;
    color?: number;
    pointCloud: {
        position: number[];
        size: number[];
    },
    video: {
        position: number[],
        size: number[];
    };
}

export interface LabelState {
    clicked: boolean;
}

export class Label extends Component<LabelProperties, LabelState> {

    private onClick: () => void;

    static BUSID = 'Label';

    private msgBus: ComponentService;

    constructor(props: LabelProperties) {
        super(props);
        this.msgBus = ComponentService.getInstance();
        this.state = {
            clicked: false
        };

        this.onClick = () => {
            const msg = this.state.clicked ? {
                from: `${Label.BUSID}:${this.props.type}`,
                to: ThreeJsLane.BUSID,
                data: undefined
            } : {
                    from: `${Label.BUSID}:${this.props.type}`,
                    to: ThreeJsLane.BUSID,
                    data: {
                        title: this.props.title,
                        color: this.props.color || toHexColor(SETTINGS.colors.label),
                        pointCloud: {
                            position: this.props.pointCloud.position,
                            size: this.props.pointCloud.size
                        },
                        video: {
                            position: this.props.video.position,
                            size: this.props.video.size
                        }
                    } as RenderLabelMessage
                };

            this.msgBus.sendMessage(msg);

            this.setState({
                clicked: !this.state.clicked
            });
        };
    }

    render(): JSX.Element {
        return (
            <div className='job_label' onClick={this.onClick}>
                <span>
                    {this.props.type === LabelType.OBJECT ? <FaObjectUngroup /> : <FaTag />} {this.props.title} {`(${this.props.startTime} - ${this.props.endTime})`} {this.state.clicked && <HiLightBulb />}
                </span>
            </div>
        );
    }
}