import { App } from './App';
import { Header } from './Header';
import * as React from 'react';
import * as ReactDOM from 'react-dom';



ReactDOM.render(
    <div className="render_container">
        <Header />
        <App
            userName="Developer"
            lang="TypeScript"
            title="The Incident Room">
        </App>
    </div>,
    document.getElementById('App')
);
