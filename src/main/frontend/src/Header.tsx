import favicon from './assets/images/favicon.ico';
import logo192 from './assets/images/logo192.png';
import './assets/scss/styles.scss';
import * as React from 'react';
import { Component } from 'react';
import { Helmet } from 'react-helmet';



export class Header extends Component {

    render(): JSX.Element {
        return (
            <Helmet>
                <link rel="icon" href={`${favicon}`} />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <meta name="theme-color" content="#000000" />
                <meta name="description" content="Big Data Signal Processing Standard Frontend" />
                <link rel="apple-touch-icon" href={`${logo192}`} />
                <link rel="manifest" href="manifest.json" />
                <title>BDSP</title>
            </Helmet>
        );
    }
}
