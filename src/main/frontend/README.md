# The Incident Room Frontend Guide

The incident room web frontend is a react based web client. The current development of the components etc. is done with **typescript** and **webpack** as the bundler. The typescript transpilation is defined by **./tsconfig.json**. Webpack gets its configuration by **webpack.config.js** but also from other configs like webpack.config.*.js. This depends on each environment.

## Initial setup
The setup description targets only the web frontend part. That means the installation of the backend is allready done and working yet.

To setup your local environemt for this web project please install following:
* Node JS 14.x (LTS) or later (https://nodejs.org/en/)

After you have done the installation successfully open a command terminal and navigate to the location of this README.md (the same location of the package.json file) and execute:
```shell
npm install
```

This resolves all dependencies for this web frontend. After this you should see the directory **'node_modules'** in this location.


## Dev builds

Dev builds are done while active development. That means the webpack dev server (webpack dev server is configure in webpack.config.js) get its context and content from the automatically created **'dist'** directory within this project. Currently on http://localhost:9000 the dev server is accessable. All url and environment specifig configuration are done with so called DOTENV environment variables. For react they have to prefixed with 'REACT_APP'. Therefore a **'.env'** file must exists within this project. Keep in mind this a hot module replaced build. That means if the dev server is running each time you change the code this get instantly installed into the dev server.

To start the web pack dev server inclusive dev build process just open a command shell navigate to this location of this README.md and execute

```shell
npm run serve
```

As you can see the bootstrapping etc. is done with NPM environment. Please look into the package.json **scripts** section to get an overview of possible commands for cleaning, testing and building etc.

## Web builds

Web builds are installations of the frontend bunlde into the SparkUI backend. That means the production read bundle is installed in the web resources directory which is installed as jetty servlet container. The current installation directory is **../resources/web**. For this build the separated webpack config file **webpack.config.web.js** is used for. The web build is accessable on http://localhost:4040/bdsp/. Keep in mind this not a hot module replaced build. That means for a new version you have to repeat the build with the command below manually.

To create a new web build just open a command terminal and execute
```shell
npm run build:web
```