const path = require('path');
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { WebpackManifestPlugin }  = require('webpack-manifest-plugin');
const Dotenv = require('dotenv-webpack');

const distDirPath = path.resolve(__dirname, '../resources/web');

module.exports = {
    entry: './src/index.tsx',
    target: 'web',
    mode: 'production',
    output: {
        path: distDirPath,
        filename: 'bundle.js',
        publicPath: '',
        devtoolModuleFilenameTemplate: '[absolute-resource-path]'
    },
    devtool: 'cheap-source-map',
    resolve: {
        extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
    },
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/i,
                loader: 'babel-loader',
            },
            {
                test: /\.(sass|scss|css)$/i,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                        }
                    },
                    {
                        loader: 'resolve-url-loader'
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true
                        }
                    }
                ],
            },
            {
                test: /\.(png|jpg|svg|ico)$/i,
                type: 'asset/resource',
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'src', 'index.html'),
        }),
        new MiniCssExtractPlugin({
            filename: './src/assets/styles.css'
        }),
        new webpack.HotModuleReplacementPlugin(),
        new WebpackManifestPlugin(),
        new Dotenv(),
    ],
}
