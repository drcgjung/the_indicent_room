FROM gitpod/workspace-full
USER gitpod

RUN brew install sbt
RUN echo 'export PATH="/home/linuxbrew/.linuxbrew/bin:$PATH"' >> /home/gitpod/.profile
RUN echo 'export PATH="/home/linuxbrew/.linuxbrew/opt/python@3.9/libexec/bin:$PATH"' >> /home/gitpod/.profile
RUN sudo mkdir -p /Library/Java/JavaVirtualMachines
RUN sudo ln -sfn /home/linuxbrew/.linuxbrew/opt/openjdk/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk.jdk
RUN echo 'export PATH="/home/linuxbrew/.linuxbrew/opt/openjdk/bin:$PATH"' >> /home/gitpod/.profile
RUN sudo mv /home/linuxbrew/.linuxbrew/Cellar/sbt/1.4.6.reinstall /home/linuxbrew/.linuxbrew/Cellar/sbt/1.4.6
RUN sudo ln -sfn /home/linuxbrew/.linuxbrew/Cellar/sbt/1.4.6/bin/sbt /home/linuxbrew/.linuxbrew/bin/sbt
RUN sudo ln -sfn /home/linuxbrew/.linuxbrew/opt/openjdk /home/linuxbrew/.linuxbrew/opt/openjdk@11
RUN sbt --version 
RUN brew install --ignore-dependencies scala@2.12 
RUN echo 'export PATH="/home/linuxbrew/.linuxbrew/opt/scala@2.12/bin:$PATH"' >> /home/gitpod/.profile
RUN brew install --ignore-dependencies apache-spark
RUN echo 'export SPARK_HOME="/home/linuxbrew/.linuxbrew/opt/apache-spark"' >> /home/gitpod/.profile

