// The simplest possible sbt build file is just one line:
val scalaMajor = "2.12"
scalaVersion := s"${scalaMajor}.12"

// That is, to create a valid sbt build, all you've got to do is define the
// version of Scala you'd like your project to use.

// ============================================================================

// Lines like the above defining `scalaVersion` are called "settings". Settings
// are key/value pairs. In the case of `scalaVersion`, the key is "scalaVersion"
// and the value is "2.12.10"

// It's possible to define many kinds of settings, such as:

name := "the-incident-room"
organization := "com.tsi.bdsp"
version := "1.0"

// Note, it's not required for you to define these three settings. These are
// mostly only necessary if you intend to publish your library's binaries on a
// place like Sonatype or Bintray.
lazy val sparkGroup = "org.apache.spark"
lazy val sparkVersion = "3.0.1"

lazy val scalatraGroup = "org.scalatra"
lazy val scalatraVersion = "2.6.5"

// Want to use a published library in your project?
// You can define other libraries as dependencies in your build like this:
libraryDependencies += "org.json4s" %% "json4s-native" % "3.6.10"
libraryDependencies += sparkGroup %% "spark-core" % sparkVersion
libraryDependencies += sparkGroup %% "spark-streaming" % sparkVersion
libraryDependencies += sparkGroup %% "spark-sql" % sparkVersion
libraryDependencies += sparkGroup %% "spark-catalyst" % sparkVersion

// scalatra and web
libraryDependencies += "javax.servlet" % "javax.servlet-api" % "3.1.0"
libraryDependencies += scalatraGroup %% "scalatra" % scalatraVersion
libraryDependencies += scalatraGroup %% "scalatra-swagger" % scalatraVersion
libraryDependencies += scalatraGroup %% "scalatra-json" % scalatraVersion

// Test dependencies
libraryDependencies += "junit" % "junit" % "4.13.1" % Test


// rename
assemblyJarName in assembly := s"com.tsi.bdsp.the-incident-room-jar-with-dependencies.jar"

// workaround of https://github.com/sbt/sbt-assembly/issues/295
fullClasspath in assembly := {
  val cp: Classpath = (fullClasspath in assembly).value
  val deps: Vector[File] = update.map(f => f.select(configurationFilter("provided"))).value
  cp.filterNot(f => deps.contains(f.data))
}

// META-INF discarding
assemblyMergeStrategy in assembly := {
    case x if Assembly.isConfigFile(x) =>
      MergeStrategy.concat
    case PathList(ps @ _*) if Assembly.isReadme(ps.last) || Assembly.isLicenseFile(ps.last) =>
      MergeStrategy.rename
    case PathList("META-INF", xs @ _*) =>
      (xs map {_.toLowerCase}) match {
        case ("manifest.mf" :: Nil) | ("index.list" :: Nil) | ("dependencies" :: Nil) =>
          MergeStrategy.discard
        case ps @ (x :: xs) if ps.last.endsWith(".sf") || ps.last.endsWith(".dsa") =>
          MergeStrategy.discard
        case "plexus" :: xs =>
          MergeStrategy.discard
        case "services" :: xs =>
          MergeStrategy.filterDistinctLines
        case ("spring.schemas" :: Nil) | ("spring.handlers" :: Nil) =>
          MergeStrategy.filterDistinctLines
        case _ => MergeStrategy.first
      }
    case _ => MergeStrategy.first
}

