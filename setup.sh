if [ ! -d "resources/2011_09_26" ]; then
    curl -o 2011_09_26_calib.zip https://s3.eu-central-1.amazonaws.com/avg-kitti/raw_data/2011_09_26_calib.zip
    unzip 2011_09_26_calib.zip -d resources
    rm -f 2011_09_26_calib.zip
fi

if [ ! -d "resources/2011_09_26/2011_09_26_drive_0001_extract" ]; then
    curl -o 2011_09_26_drive_0001_extract.zip https://s3.eu-central-1.amazonaws.com/avg-kitti/raw_data/2011_09_26_drive_0001/2011_09_26_drive_0001_extract.zip
    unzip 2011_09_26_drive_0001_extract.zip -d resources
    rm -f 2011_09_26_drive_0001_extract.zip
    curl -o 2011_09_26_drive_0001_sync.zip https://s3.eu-central-1.amazonaws.com/avg-kitti/raw_data/2011_09_26_drive_0001/2011_09_26_drive_0001_sync.zip
    unzip 2011_09_26_drive_0001_sync.zip -d resources
    rm -f 2011_09_26_drive_0001_sync.zip
    curl -o 2011_09_26_drive_0001_tracklets.zip https://s3.eu-central-1.amazonaws.com/avg-kitti/raw_data/2011_09_26_drive_0001/2011_09_26_drive_0001_tracklets.zip
    unzip 2011_09_26_drive_0001_tracklets.zip -d resources
    rm -f 2011_09_26_drive_0001_tracklets.zip
fi

if [ ! -d "resources/2011_09_26/2011_09_26_drive_0002_extract" ]; then
    curl -o 2011_09_26_drive_0079_extract.zip https://s3.eu-central-1.amazonaws.com/avg-kitti/raw_data/2011_09_26_drive_0079/2011_09_26_drive_0079_extract.zip
    unzip 2011_09_26_drive_0079_extract.zip -d resources
    rm -f 2011_09_26_drive_0079_extract.zip
    curl -o 2011_09_26_drive_0079_sync.zip https://s3.eu-central-1.amazonaws.com/avg-kitti/raw_data/2011_09_26_drive_0079/2011_09_26_drive_0079_sync.zip
    unzip 2011_09_26_drive_0079_sync.zip -d resources
    rm -f 2011_09_26_drive_0079_sync.zip
    curl -o 2011_09_26_drive_0079_tracklets.zip https://s3.eu-central-1.amazonaws.com/avg-kitti/raw_data/2011_09_26_drive_0079/2011_09_26_drive_0079_tracklets.zip
    unzip 2011_09_26_drive_0079_tracklets.zip -d resources
    rm -f 2011_09_26_drive_0079_tracklets.zip
fi

