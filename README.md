[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/drcgjung/the_indicent_room)

# the_indicent_room

Student Project for Big Data Signal Processing

## Setting up your IDE (Integrated Development Environment)

### Using Cloud IDE (Gitpod)

Use [this link](https://gitpod.io/#https://gitlab.com/drcgjung/the_indicent_room) to automatically start a Gitpod workspace.

### Using Local IDE (Visual Studio Code and git/bash)

The following instructions are for Windows (10) on a 64Bit machine. We assume in the following that %USERPROFILE%=c:\users\chris

- Install/Unzip [git](https://github.com/git-for-windows/git/releases/download/v2.30.0.windows.2/PortableGit-2.30.0.2-64-bit.7z.exe), for example into %USERPROFILE%\Programme\PortableGit

- Install/Unzip [jdk](https://github.com/AdoptOpenJDK/openjdk15-binaries/releases/download/jdk-15.0.1%2B9/OpenJDK15U-jdk_x64_windows_hotspot_15.0.1_9.zip), for example into $USERPROFILE%\Programme\jdk-15.0.1+9

- Install/Unzip [sbt](https://github.com/sbt/sbt/releases/download/v1.4.5/sbt-1.4.5.zip), for example into %USERPROFILE%\Programme\sbt

- Install/Unzip [python3](https://www.python.org/ftp/python/3.9.1/python-3.9.1-amd64.exe), for example into %USERPROFILE%\Programme\python3

- Install/Unzip [spark 3.0.1 for hadoop 2.7](https://apache.mirror.digionline.de/spark/spark-3.0.1/spark-3.0.1-bin-hadoop2.7.tgz), for example into %USERPROFILE%\Programme\spark-3.0.1-bin-hadoop2.7

- Configure your shell-based build environment (assuming that you installed the packages - and the upcoming hadoop - to the above folders)
```
export HADOOP_HOME=~/Programme/winutils/hadoop-2.7.1
export JAVA_HOME=~/Programme/jdk-15.0.1+9
export PYTHON_HOME=~/Programme/python3
export SBT_HOME=~/Programme/sbt
export SPARK_HOME=~/Programme/spark-3.0.1-bin-hadoop2.7
PATH=$JAVA_HOME/bin:$SBT_HOME/bin:$SPARK_HOME/bin:$PYTHON_HOME/:$PYTHON_HOME/Scripts:$PATH
```

- Open/Execute git-bash.exe from %USERPROFILE%\Programme\PortableGit\git-bash.exe

- Install windows binaries for hadoop
```
$ cd ~/Programme
$ git clone https://github.com/steveloughran/winutils.git
```

- Check out this project (by eventually logging into gitlab)
```
$ cd ~
$ mkdir Projekte
$ cd Projekte
$ git clone git@gitlab.com:drcgjung/the_indicent_room.git
```

- Build & run sample scala application with sbt
```
$ sbt run 
Copying runtime jar.
[info] [launcher] getting org.scala-sbt sbt 1.4.6  (this may take some time)...
...
[info] [launcher] getting Scala 2.12.12 (for sbt)...
...
[info] welcome to sbt 1.4.6 (AdoptOpenJDK Java 15.0.1)
[info] loading settings for project the_indicent_room-build-build from metals.sbt ...
[info] loading project definition from C:\Projekte\the_indicent_room\project\project
[info] loading settings for project the_indicent_room-build from metals.sbt ...
[info] loading project definition from C:\Projekte\the_indicent_room\project
[success] Generated .bloop\the_indicent_room-build.json
[success] Total time: 11 s, completed 16.01.2021, 11:17:16
[info] loading settings for project the_indicent_room from build.sbt ...
[info] set current project to the-incident-room (in build file:/C:/Projekte/the_indicent_room/)
[warn] There may be incompatibilities among your library dependencies; run 'evicted' to see detailed eviction warnings.
[info] compiling 1 Scala source to C:\Projekte\the_indicent_room\target\scala-2.12\classes ...
[info] Non-compiled module 'compiler-bridge_2.12' for Scala 2.12.12. Compiling...
[info]   Compilation completed in 16.306s.
[info] done compiling
[info] running Main
WARNING: An illegal reflective access operation has occurred
...
Using Spark's default log4j profile: org/apache/spark/log4j-defaults.properties
21/01/16 11:18:06 INFO SparkContext: Running Spark version 3.0.1
...
Pi is roughly 3.136817842044605
21/01/16 11:18:10 INFO SparkUI: Stopped Spark web UI at http://host.docker.internal:4040
...
21/01/16 11:18:10 INFO SparkContext: Successfully stopped SparkContext
[success] Total time: 51 s, completed 16.01.2021, 11:18:10
```

- Install python requirements and run the sample python application
```
$ cd the_incident_room
$ pip install -r requirements.txt
Collecting pyspark==3.0.1
  Downloading pyspark-3.0.1.tar.gz (204.2 MB)
...
Building wheels for collected packages: pyspark
...
Successfully built pyspark
Installing collected packages: pytz, python-dateutil, py4j, numpy, pyspark, pandas, findspark
Successfully installed findspark-1.4.2 numpy-1.19.5 pandas-1.2.0 py4j-0.10.9 pyspark-3.0.1 python-dateutil-2.8.1 pytz-2020.5
$ python src/main/python/Main.py
WARNING: An illegal reflective access operation has occurred
...
Using Spark's default log4j profile: org/apache/spark/log4j-defaults.properties
...
Pi is roughly 3.142560
ERFOLGREICH: Der Prozess mit PID 12448 (untergeordnetem Prozess von PID 16368) wurde beendet.
ERFOLGREICH: Der Prozess mit PID 16368 (untergeordnetem Prozess von PID 6448) wurde beendet.
ERFOLGREICH: Der Prozess mit PID 6448 (untergeordnetem Prozess von PID 9340) wurde beendet.
```
- Install [Visual Studio Code](https://code.visualstudio.com/sha/download?build=stable&os=win32-x64-archive)
- Register git in Visual Studio Code by adding a JSON property to %USERPROFILE%\AppData\Roaming\Code\User\settings.json (for example if %USERPROFILE%=c:\users\chris and you extracted git into Programme\PortableGit within your home folder)
```
{
    ...
    "git.path": "C:\\Users\\chris\\Programme\\PortableGit\\bin\\git.exe"
}
```
- Clone [this repository](git@gitlab.com:drcgjung/the_indicent_room.git) by copying the URL into the input box when opening the "Clone Repository" dialogue

![Visual Studio Code Clone Repository Dialogue](src/site/resources/vscode_clone_repo.png)
- Install the [python extension](vscode:extension/ms-python.python)

```
{
    ...
    "python.defaultInterpreterPath": "c:\\users\\chris\\Programme\\python3\\python.exe"
}
```
- Install pylint
- Install metals